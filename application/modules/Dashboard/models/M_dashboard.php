<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}


   public function getChartBulan($tahun='')
   {
   	($tahun =='')?$tahun = date('Y'):$tahun;

   	$query = "SELECT DATE_FORMAT(a.tgl,'%m') AS bln , (SUM(a.tunjangan)/1000) AS tnj FROM t_tukin a   WHERE DATE_FORMAT(a.tgl,'%Y') = '".$tahun."'  GROUP BY  DATE_FORMAT(a.tgl,'%m-%Y') ";

   	return $this->db->query($query);
   }


    public function getChartKelas($tahun='')
   {
   	($tahun =='')?$tahun = date('Y'):$tahun;

   	$query = "SELECT a.kelas_jabatan , (SUM(a.tunjangan)/1000) AS tnj FROM t_tukin a   WHERE DATE_FORMAT(a.tgl,'%Y') = '".$tahun."'  GROUP BY  a.kelas_jabatan";

   	return $this->db->query($query);
   }


    public function getChartSatker($tahun='')
   {
   	($tahun =='')?$tahun = date('Y'):$tahun;

   	$query = "SELECT b.nama_satker , (SUM(a.tunjangan)/1000) AS tnj FROM t_tukin a   LEFT JOIN m_satker b ON b.kd_satker = a.kd_satker WHERE DATE_FORMAT(a.tgl,'%Y') = '".$tahun."' GROUP BY  a.kd_satker";

   	return $this->db->query($query);
   }




}

?>