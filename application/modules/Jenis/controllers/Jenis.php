<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	

	//KELAS_JABATAN
	public function index()
	{
		$data['konten']="home";
     
		$this->_template($data);
	}

	function load_jenis()
	{
			
		$list = $this->setting->get_jenis();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->kode."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->rekening."</span>";
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void();" title="Edit" onclick="edit('.$dataDB->id_jenis.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void();" title="Hapus" onclick="deleted('.$dataDB->id_jenis.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_jenis("m_jenis"),
						"recordsFiltered" =>$this->setting->count_filtered_jenis('m_jenis'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_jenis()
	{
//            print_r($_POST);exit;
		$data=$this->setting->add_jenis();
		echo json_encode($data);
	}

	function edit_jenis($id)
	{
		$data=$this->setting->get_data_jenis($id);
		echo json_encode($data);
	}		

	function update_jenis()
	{
		$id=$this->input->post("id_jenis");
		$data=$this->setting->update_jenis($id);
		echo json_encode($data);
	}

	function delete_jenis($id)
	{
		$data=$this->setting->delete_jenis($id);
		echo json_encode($data);
	}

}

