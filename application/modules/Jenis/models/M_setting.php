<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}
	//Penanda Tangan
	function get_jenis()
	{
		
		$query=$this->_get_datatables_jenis();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_jenis()
	{
	$query="SELECT * FROM m_jenis where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_surtu='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			kode LIKE '%".$searchkey."%' or
			nama LIKE '%".$searchkey."%' or
			rekening LIKE '%".$searchkey."%' 
			) ";
		}

		$column = array('','kode','nama','rekening');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_jenis($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_jenis($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_jenis();
		return $this->db->query($query)->num_rows();
	}

	function add_jenis()
	{
		
                    $data = array(
                        "kode" => $this->input->post("kode"),
                        "nama" => $this->input->post("nama"),
                        "rekening" => $this->input->post("rekening"),
                    );
                return $this->db->insert("m_jenis",$data);
	}

	function get_data_jenis($id) //id_file
	{
		$this->db->from('m_jenis');
		$this->db->where('id_jenis',$id);
		$query = $this->db->get();
		return $query->row();
	}
        
        

	public function update_jenis($id)
	{
	
                    $data=array(
                    "kode" => $this->input->post("kode"),
                    "nama" => $this->input->post("nama"),
                    "rekening" => $this->input->post("rekening"),
                    );
                $this->db->where("id_jenis",$id);
		return $this->db->update("m_jenis",$data);
	}

	public function delete_jenis($id)
	{
		$this->db->where("id_jenis",$id);
		$data=$this->db->delete("m_jenis");
	}
        
        function get_ref($table){
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
                    
        }
}

?>