<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_laporan','laporan');
		$this->load->model('setting/m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	


	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}


	//PERHITUNGAN_TUKIN
	public function lap_tukin_persatker()
	{
		$data['konten']="lap_tukin_persatker";
		$data['satker']= $this->setting->get_listsatker();
		$this->_template($data);
	}

	public function lap_tukin_perkelas()
	{
		$data['konten']="lap_tukin_perkelas";
		$data['kelas']= $this->setting->get_listkelas();
		$this->_template($data);
	}

	public function rekap_tukin_perkelas()
	{
		$data['konten']="rekap_tukin_perkelas";
		$this->_template($data);
	}

	public function rekap_tukin_persatker()
	{
		$data['konten']="rekap_tukin_persatker";
		$this->_template($data);
	}

	function rest_tukin1($tahun='',$satker='')
	{
		$data = $this->laporan->get_table_tukin_per_satker($satker,$tahun);
		$data = array(
			'data' => $data->result()
			);
		echo json_encode($data);
	}

	function rest_tukin2($tahun='',$kelas='')
	{
		$data = $this->laporan->get_table_tukin_per_kelas($kelas,$tahun);
		$data = array(
			'data' => $data->result()
			);
		echo json_encode($data);
	}

	function rest_tukin3($tahun='')
	{
		$data = $this->laporan->get_rekap_tukin_per_kelas($tahun);
		$data = array(
			'data' => $data->result()
			);
		echo json_encode($data);
	}

	function rest_tukin4($tahun='')
	{
		$data = $this->laporan->get_rekap_tukin_per_satker($tahun);
		$data = array(
			'data' => $data->result()
			);
		echo json_encode($data);
	}

}

