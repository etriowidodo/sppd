<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_laporan extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}

    function LapSatker () {
		$query=$this->_get_datatables_open();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}


	function get_table_tukin_per_satker($satker,$tgl)
	{

	$query  = 	"SELECT DATE_FORMAT(a.tgl,'%m-%Y') AS tanggal ,b.nama,a.nip,a.jabatan,a.kelas_jabatan,a.tunjangan,a.potongan,c.nama_satker,a.kd_satker
 			FROM t_tukin a 
 			LEFT JOIN m_karyawan b ON a.nip = b.nip 
 			LEFT JOIN m_satker c ON a.kd_satker = c.kd_satker 
 			HAVING a.kd_satker = ".$satker." AND tanggal = '".$tgl."'";
	 		//HAVING a.kd_satker = $satker AND tanggal = $tgl
	 		return $this->db->query($query);
	}

	function get_table_tukin_per_kelas($kelas,$tgl)
	{

	$query  = 	"SELECT DATE_FORMAT(a.tgl,'%m-%Y') AS tanggal ,b.nama,a.nip,a.jabatan,a.kelas_jabatan,a.tunjangan		 ,a.potongan,c.nama_satker,a.kd_satker
 			FROM t_tukin a 
 			LEFT JOIN m_karyawan b ON a.nip = b.nip 
 			LEFT JOIN m_satker c ON a.kd_satker = c.kd_satker 
 			HAVING a.kelas_jabatan = ".$kelas." AND tanggal = '".$tgl."'";
	 		//HAVING a.kd_satker = $satker AND tanggal = $newt_textbox_get_num_lines(textbox)
	 		return $this->db->query($query);
	}


	function get_rekap_tukin_per_kelas($tgl)
	{
		$query = "SELECT
					kelas_jabatan, 
					COUNT(kelas_jabatan) AS count_kelas, 
					SUM(tunjangan) AS tnj , 
					pajak AS pjk 
					FROM t_tukin 
					WHERE DATE_FORMAT(tgl,'%m-%Y')='".$tgl."' 
					GROUP BY kelas_jabatan";
		return $this->db->query($query);
	}

	function get_rekap_tukin_per_satker($tgl)
	{
		$query = "SELECT
					a.kd_satker,
					b.nama_satker,
					COUNT(a.kelas_jabatan) AS count_kelas, 
					SUM(a.tunjangan) AS tnj , 
					SUM(a.pajak) AS pjk FROM t_tukin a 
					LEFT JOIN m_satker b ON a.kd_satker = b.kd_satker
					WHERE DATE_FORMAT(a.tgl,'%m-%Y')='".$tgl."' GROUP BY a.kd_satker";
		return $this->db->query($query);
	}

}

?>