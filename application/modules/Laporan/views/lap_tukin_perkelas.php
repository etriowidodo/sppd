  
	
  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Laporan</a></li>
      <li class="active"><span>Tukin Per Kelas Jabatan</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix ">
			<header class="main-box-header clearfix">
				<h2 class="sadow05  ">Laporan Rinci Tunjangan Kinerja Per Kelas Jabatan</h2>
				<h5 class="sadow05 " id="container-date" style="display: none">Bulan <span id='bulan'></span> Tahun <span id='tahun'></span> </h5>
			</header>
			<div class="main-box-body clearfix ">
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Tahun</label>
					<div class="col-lg-3">
							<input class="form-control" value="" type="text" name="tahun" id="yearpicker" />
						</fieldset>
					</div>
				</div>
				<br/>
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Kelas Jabatan</label>
					<div class="col-lg-3">
							<select class="form-control" name="kelas" id="kelas">
								<option value=""> -- Semua -- </option>
								<?php foreach ($kelas as $row): ?>
									<option value="<?=$row['kelas_jabatan']?>"><?=$row['kelas_jabatan']?></option>
								<?php endforeach; ?>
							</select>
					</div>
					<div class="col-lg-1">
							<button id='proses' type="button" class="btn btn-info"> 
							<span class="glyphicon glyphicon-search"></span>  Proses
							</button>
					</div>
					<div class="col-lg-2">
							<button type="button" class="btn  btn-danger"> 
							<span class="glyphicon glyphicon-file"></span>  Pdf
							</button>
							<button type="button" class="btn btn-success"> 
							<span class="glyphicon glyphicon-th"></span>  Excel
							</button>
					</div>
				</div>
				<br/>				
				<table id='table' class="tabel black table-striped table-bordered table-hover dataTable">
					<thead>			
						<tr>
							<th  width='15px'  style="text-align: center;">No</th>
							<th >Nama Pegawai</th>
							<th >NIP</th>
							<th> Jabatan</th>
							<th> Kelas Jabatan</th>
							<th> Tukin</th>
							<th style="text-align: center;" > Potongan (%) </th>
							<th> Potongan</th>
							<th> Jumlah Dibayarkan</th>
							</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
		


<style>
.table_processing { display:none;}
.top{
	float:right;
}
a.dt-button.red {
	color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
#table th { text-align: center }
#table td:nth-child(1) { text-align : center }
#table td:nth-child(6) { text-align : right }
#table td:nth-child(7) { text-align : right }
#table td:nth-child(8) { text-align : right }
#table td:nth-child(9) { text-align : right }
</style>
 <link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
  	
<script type="text/javascript">
	var save_method; //for save method string

	$(document).ready(function() {	
		$("#yearpicker").datepicker( {
			format: "mm-yyyy",
    		viewMode: "months", 
    		minViewMode: "months",
    		autoclose: true,
		});
		// $("#yearpicker").datepicker( {
		// 	onSelect: function(date) {
  //           alert(date);
  //       },
		// });

		$('#yearpicker').on('change',function(){
			var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
			];

			var spliting = $(this).val().split('-');
			var month    = parseInt(spliting[0]);
			$('#container-date').show();
			$('#bulan').html(monthNames[month]);
			$('#tahun').html(spliting[1]);
			
		});


		
		var table = $('#table').DataTable( {
                     destroy: true,
                     "bLengthChange": false,
                     "bFilter": false,                    
                     "autoWidth": true,
                     "ajax": {
                         "url": '<?php echo base_url()?>'+'laporan/rest_tukin2/01-1990/0',
                         dataSrc: 'data',
                     },
                     "columns": [
                     	 { "data"	 : function(row, data, index,display)	{ return display.row+1; } },		
                         { "data"    : "nama" },
                         { "data"    : "nip" },
                         { "data"    : "jabatan" },
                         { "data"    : "kelas_jabatan" },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					
                         					 return toRp(row.tunjangan);
                         				} 
                         },
                         { "data"    : function(row, data, index,display)	{ return row.potongan+" %" }  },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					 var jumlah = (row.potongan/100)*row.tunjangan;
                         					 return toRp(jumlah);
                         				}  
                         },
                          { "data"    : function(row, data, index,display)	
                         				{
                         					 var jumlah = (row.potongan/100)*row.tunjangan;
                         					 var total  = row.tunjangan - jumlah ;
                         					 return toRp(total);
                         				}  
                         },
                     	 

                     ],

                     "columnDefs": [
                             { "width": "2%", "targets": 0 },
                             { "width": "20%", "targets": 1 },
                             { "width": "15%", "targets": 2 },
                             { "width": "5%", "targets": 3 },
                             { "width": "3%", "targets": 4 },
                             { "width": "20%", "targets": 5 },
                             { "width": "5%", "targets": 6 },
                             { "width": "15%", "targets": 7 },
                             { "width": "20%", "targets": 8 },
                           ],
                     select: true
                 } );

		$('#proses').on('click',function(){

			var tahun 	= $('#yearpicker').val();
			var kelas 	= $('#kelas option:selected').val();
			table.ajax.url( '<?php echo base_url()?>'+'laporan/rest_tukin2/'+tahun+'/'+kelas ).load();

		});

		

			
		
		



		
	});
	
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)+',00'}

	
  </script>		
  