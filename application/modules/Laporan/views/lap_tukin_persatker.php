  
	
  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Laporan</a></li>
      <li class="active"><span>Tukin Per Satker</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix ">
			<header class="main-box-header clearfix">
				<h2 class="sadow05  ">Laporan Rinci Tunjangan Kinerja Per Satker</h2>
				<h5 class="sadow05 " id="container-date" style="display: none">Bulan <span id='bulan'></span> Tahun <span id='tahun'></span> </h5>
			</header>
			<div class="main-box-body clearfix ">
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Tahun</label>
					<div class="col-lg-3">
							<input class="form-control" value="" type="text" name="tahun" id="yearpicker" />
						</fieldset>
					</div>
				</div>
				<br/>
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Satker</label>
					<div class="col-lg-3">
							<select class="form-control" name="satker" id="satker">
								<option value=""> -- Semua -- </option>
								<?php foreach ($satker as $row): ?>
									<option value="<?=$row['kd_satker']?>"><?=$row['nama_satker']?></option>
								<?php endforeach; ?>
							</select>
					</div>
					<div class="col-lg-1">
							<button id='proses' type="button" class="btn btn-info"> 
							<span class="glyphicon glyphicon-search"></span>  Proses
							</button>
					</div>
					<div class="col-lg-2">
							<button id="pdf" type="button" class="btn  btn-danger"> 
							<span class="glyphicon glyphicon-file"></span>  Pdf
							</button>
							<button type="button" class="btn btn-success"> 
							<span class="glyphicon glyphicon-th"></span>  Excel
							</button>
					</div>
				</div>
				<br/>				
				<table id='table' class="tabel black table-striped table-bordered table-hover dataTable">
					<thead>			
						<tr>
							<th  width='15px'  style="text-align: center;">No</th>
							<th >Nama Pegawai</th>
							<th >NIP</th>
							<th> Jabatan</th>
							<th> Kelas Jabatan</th>
							<th> Tukin</th>
							<th style="text-align: center;" > Potongan (%) </th>
							<th> Potongan</th>
							<th> Jumlah Dibayarkan</th>
							</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
		


<style>
.table_processing { display:none;}
.top{
	float:right;
}
a.dt-button.red {
	color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
#table th { text-align: center }
#table td:nth-child(1) { text-align : center }
#table td:nth-child(5) { text-align : center }
#table td:nth-child(6) { text-align : right }
#table td:nth-child(7) { text-align : right }
#table td:nth-child(8) { text-align : right }
#table td:nth-child(9) { text-align : right }
</style>
 <link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
  <script src="<?php echo base_url()?>plug/js/pdfmake.min.js"></script>	
  <script src="<?php echo base_url()?>plug/js/vfs_fonts.js"></script>	
<script type="text/javascript">
	var save_method; //for save method string

	$(document).ready(function() {	
		$("#yearpicker").datepicker( {
			format: "mm-yyyy",
    		viewMode: "months", 
    		minViewMode: "months",
    		autoclose: true,
		});
		// $("#yearpicker").datepicker( {
		// 	onSelect: function(date) {
  //           alert(date);
  //       },
		// });


		

		$('#yearpicker').on('change',function(){
			var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
			];

			var spliting = $(this).val().split('-');
			var month    = parseInt(spliting[0]);
			$('#container-date').show();
			$('#bulan').html(monthNames[month]);
			$('#tahun').html(spliting[1]);
			
		});


		
		var table = $('#table').DataTable( {
                     destroy: true,
                     "bLengthChange": false,
                     "bFilter": false,                    
                     "autoWidth": true,
                     "ajax": {
                         "url": '<?php echo base_url()?>'+'laporan/rest_tukin1/01-1990/0',
                         dataSrc: 'data',
                     },
                     "columns": [
                     	 { "data"	 : function(row, data, index,display)	{ return display.row+1; } },		
                         { "data"    : "nama" },
                         { "data"    : "nip" },
                         { "data"    : "jabatan" },
                         { "data"    : "kelas_jabatan" },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					
                         					 return toRp(row.tunjangan);
                         				} 
                         },
                         { "data"    : function(row, data, index,display)	{ return row.potongan+" %" }  },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					 var jumlah = (row.potongan/100)*row.tunjangan;
                         					 return toRp(jumlah);
                         				}  
                         },
                          { "data"    : function(row, data, index,display)	
                         				{
                         					 var jumlah = (row.potongan/100)*row.tunjangan;
                         					 var total  = row.tunjangan - jumlah ;
                         					 return toRp(total);
                         				}  
                         },
                     	 

                     ],

                     "columnDefs": [
                             { "width": "2%", "targets": 0 },
                             { "width": "20%", "targets": 1 },
                             { "width": "15%", "targets": 2 },
                             { "width": "5%", "targets": 3 },
                             { "width": "3%", "targets": 4 },
                             { "width": "20%", "targets": 5 },
                             { "width": "5%", "targets": 6 },
                             { "width": "15%", "targets": 7 },
                             { "width": "20%", "targets": 8 },
                           ],
                     select: true
                 } );

		$('#proses').on('click',function(){
			

			var tahun 	= $('#yearpicker').val();
			
			var satker 	= $('#satker option:selected').val();
			table.ajax.url( '<?php echo base_url()?>'+'laporan/rest_tukin1/'+tahun+'/'+satker ).load();

		});

		

			
		$('#pdf').on('click',function(){
			var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
			];
			var tahun 		= $('#yearpicker').val();
			var spliting = tahun.split('-');
			var month    = monthNames[parseInt(spliting[0]-1)];
			var satker 		= $('#satker option:selected').val();
			var nmsatker 	= $('#satker option:selected').text();
							function returnData(selected) {
							   return $.ajax({
							      url : 'http://localhost/basarnas-tukin/laporan/rest_tukin1/'+tahun+'/'+satker,
							      data: {
							              issession: 1,
							              selector: selector
							          },
							      // dataType: "json",
							          async: false,
							          error: function() {
							              alert("Error occured")
							          }
							    });

							}

							var selector = !0;
							var ajaxObj = returnData(selector);
							var data    = JSON.parse(ajaxObj.responseText);

							function buildTableBody(data, columns) {
							    var body = [];
							    var i = 0;
							    data.forEach(function(row) {
							        var dataRow = [];

							        // columns.forEach(function(column) {
							        //     dataRow.push({'text':row[column].toString(),'aligement':'tester '});
							        // })
							        var potongan = (row["potongan"]/100)*row["tunjangan"];
							        var total   = row["tunjangan"]-potongan;
							        dataRow.push({'text':++i,'aligement':'' ,style: 'content'});
							        dataRow.push({'text': row["nama"].toString(),'aligement':'',style: 'content'});
							        dataRow.push({'text': row["nip"].toString(),'aligement':'',style: 'content'});
							        dataRow.push({'text': "PNS",'aligement':'',style: 'content'});
							        dataRow.push({'text': row["jabatan"].toString(),'aligement':'',style: 'content'});       
							        dataRow.push({'text': row["kelas_jabatan"].toString(),'aligement':'center',style: 'content'});
							         dataRow.push({'text': toRp(row["tunjangan"]),'aligement':'right',style: 'content'});
							        dataRow.push({'text': row["potongan"].toString(),'aligement':'',style: 'content'});
							        dataRow.push({'text': toRp(potongan),'aligement':'right',style: 'content'});
							        dataRow.push({'text': "",'aligement':'',style: 'content'});
							        dataRow.push({'text': "",'aligement':'',style: 'content'});
							        dataRow.push({'text': toRp(total).toString(),'aligement':'right',style: 'content'});
							        dataRow.push({'text': "",'aligement':'',style: 'content'});
							        body.push(dataRow);
							    });

							    return body;
							}

							var hasil = buildTableBody(data.data,['kd_satker','nama_satker','count_kelas','tnj','pjk']);
							var insert = {"body":[
							                        [ 
							                            {text: 'NO', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'NAMA PEGAWAI', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'NIP', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'STATUS \n KEPEG \n (PNS/CPNS)', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'JABATAN (TMT)', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'KLS\nJAB\nATAN', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'TUNJANGAN \n KINERJA', style: 'tableHeader', rowSpan: 3, alignment: 'center'}, 
							                            {text: 'POTONGAN', style: 'tableHeader3', colSpan: 4, alignment: 'center'}, 
							                            {}, 
							                            {},
							                            {},
							                            {text: 'JUMLAH \n DIBAYARKAN', style: 'tableHeader', rowSpan: 3, alignment: 'center'},
							                            {text: 'KET', style: 'tableHeader', rowSpan: 3, alignment: 'center'}
							                        ],
							                        [
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {text: 'FAKTOR PENGURANG', style: 'tableHeader3', colSpan: 3, alignment: 'center'}, 
							                          {}, 
							                          {},
							                          {text:'JUMLAH POTONGAN',  alignment: 'center', style: 'tableHeader2',  rowSpan: 2,}
							                        ],
							                        [                    
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {},
							                          {text:'Poto\nngan\n(%)'},
							                          {text:'Nilai %'},
							                          {text:'NILAI\nRUPIAH'},
							                          {}
							                        ],
							                        [
							                          {text: '1', style: 'subheader', alignment: 'center'}, 
							                          {text: '2', style: 'subheader', alignment: 'center'}, 
							                          {text: '3', style: 'subheader', alignment: 'center'}, 
							                          {text: '4', style: 'subheader', alignment: 'center'}, 
							                          {text: '5', style: 'subheader', alignment: 'center'}, 
							                          {text: '6', style: 'subheader', alignment: 'center'}, 
							                          {text: '7', style: 'subheader', alignment: 'center'}, 
							                          {text: '8', style: 'subheader', alignment: 'center'}, 
							                          {text: '9', style: 'subheader', alignment: 'center'}, 
							                          {text: '10', style: 'subheader', alignment: 'center'}, 
							                          {text: '13=10+12', style: 'subheader', alignment: 'center'}, 
							                          {text: '14=7+10', style: 'subheader', alignment: 'center'}, 
							                          {text: '11', style: 'subheader', alignment: 'center'}, 
							                        ],

							                      ]
							              };

							for(var loop =0 ; loop<= (hasil.length-1) ;loop++)
							{ 
							  insert.body.push(hasil[loop]);
							}

							// console.log(hasil);
							 function tableSample(data, columns) {
							    return {
							        table: {
							            headerRows: 4,
							          dontBreakRows: true,
							          keepWithHeaderRows: 1,
							          widths: [30,80,120,50,80,30,80,25,80,80,80,80,30],
							          body: insert.body
							        }
							    };
							}
							 	var docDefinition = {
							    //pageMargins: [ 10, 70, 10, 20 ],
							    pageSize: 'LEGAL',
							    pageOrientation: 'landscape',
							    content: [
							      {
										alignment: 'left',
										columns: [
											{
												text: 'RINCIAN PEMBAYARAN TUNJANGAN KINERJA PEGAWAI'
											}
										],
							                        
									},
							      {
										alignment: 'left',
										columns: [
											{
												text: 'SATUAN KERJA'
											},
											{
												text: ': '+nmsatker
											},
							                                {
							                                    
							                                },
							                                {
							                                    
							                                },
							                                {
							                                    
							                                }
										],
							                        
									},
							       {
										alignment: 'left',
										columns: [
											{
												text: 'BULAN / TAHUN '
											},
											{
												text: ': '+month+' '+spliting[1]
											},
							                                {
							                                    
							                                },
							                                {
							                                    
							                                },
							                                {
							                                }
										],
							                        
									},
							      '\n',
							      tableSample() 
							    ],
							    styles: {
							      header1: {
							        fontSize: 14,
							        bold: true,
							        margin: [0, 0, 0, 10]
							      },
							      header2: {
							        fontSize: 18,
							        bold: true,
							        margin: [0, 0, 0, 10]
							      },
							      subheader: {
							        fontSize: 8,
							        bold: true,
							        margin: [0, 10, 0, 5]
							      },
							      title: {
							        fontSize: 11,
							        bold: true,
							       
							      },
							      content_right: {
							        fontSize: 11,
							        alignment: 'right'
							      },
							      content: {
							        fontSize: 11,
							        alignment: 'center'
							      },
							      content_left: {
							        fontSize: 11,
							        alignment: 'center'
							      },
							      tableExample: {
							        margin: [0, 0, 0, 0]
							      },
							      tableHeader: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 40, 0, 0],
							        color: 'black'
							      },
							      tableHeader2: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 20, 0, 0],
							        color: 'black'
							      },
							      tableHeader3: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 5, 0, 0],
							        color: 'black'
							      }
							    },
							    defaultStyle: {
							      // alignment: 'justify'
							    }

							    };

							 pdfMake.createPdf(docDefinition).open();

		});
		
	});
	
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)}

	
  </script>		
  