  
	
  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Laporan</a></li>
      <li class="active"><span>Rekap Tunjangan Kinerja Per Kelas Jabatan</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix ">
			<header class="main-box-header clearfix">
				<h2 class="sadow05  ">Rekapitulasi Daftar Pembayaran Tunjangan Kinerja Per Kelas Jabatan</h2>
				<h5 class="sadow05 " id="container-date" style="display: none">Bulan <span id='bulan'></span> Tahun <span id='tahun'></span> </h5>
			</header>
			<div class="main-box-body clearfix ">
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Tahun</label>
					<div class="col-lg-3">
							<input class="form-control" value="" type="text" name="tahun" id="yearpicker" />	
					</div>
					<div class="col-lg-1">
						<button id='proses' type="button" class="btn btn-info"> 
						<span class="glyphicon glyphicon-search"></span>  Proses
						</button>
					</div>
					<div class="col-lg-2">
							<button  id='pdf' type="button" class="btn  btn-danger"> 
							<span class="glyphicon glyphicon-file"></span>  Pdf
							</button>
							<button type="button" class="btn btn-success"> 
							<span class="glyphicon glyphicon-th"></span>  Excel
							</button>
					</div>
				</div>
				<br/>				
				<table id='table' class="tabel black table-striped table-bordered table-hover dataTable">
					<thead>			
						<tr>
							<th>No</th>
							<th >Kelas Jabatan</th>
							<th >Jumlah Penerima</th>
							<th> Jumlah Tukin / Kelas Jabatan</th>
							<th> <ol><li>Jumlah Tunjangan</li><li>Pajak</li><li>Jumlah</li></ol> </th>
							<th ><ol><li>Potongan Pajak</li><li>Jumlah Netto</li></ol></th>
							</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
		


<style>
.table_processing { display:none;}
.top{
	float:right;
}
a.dt-button.red {
	color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
#table th { text-align: center }
#table th:nth-child(5) { text-align: left }
#table td:nth-child(1) { text-align : center }
#table td:nth-child(2) { text-align : center}
#table td:nth-child(5) { text-align : right }
#table td:nth-child(6) { text-align : right}

</style>
 <link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
  	<script src="<?php echo base_url()?>plug/js/pdfmake.min.js"></script>	
  <script src="<?php echo base_url()?>plug/js/vfs_fonts.js"></script>
<script type="text/javascript">
	var save_method; //for save method string

	$(document).ready(function() {	
		$("#yearpicker").datepicker( {
			format: "mm-yyyy",
    		viewMode: "months", 
    		minViewMode: "months",
    		autoclose: true,
		});
		// $("#yearpicker").datepicker( {
		// 	onSelect: function(date) {
  //           alert(date);
  //       },
		// });

		$('#yearpicker').on('change',function(){
			var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
			];

			var spliting = $(this).val().split('-');
			var month    = parseInt(spliting[0]);
			$('#container-date').show();
			$('#bulan').html(monthNames[month]);
			$('#tahun').html(spliting[1]);
			
		});


		
		var table = $('#table').DataTable( {
                     destroy: true,
                     "bLengthChange": false,
                     "bFilter": false,                    
                     "autoWidth": true,
                     "ajax": {
                         "url": '<?php echo base_url()?>'+'laporan/rest_tukin3/01-1990',
                         dataSrc: 'data',
                     },
                     "columns": [
                     	 { "data"	 : function(row, data, index,display)	{ return display.row+1; } },		
                         { "data"    : "kelas_jabatan" },
                         { "data"    : "count_kelas" },
                         { "data"    : function(row, data, index,display)	
                         				{ 
                         					return toRp(row.tnj);
                         				}
                         },

                         { "data"    : function(row, data, index,display)	
                         				{
                         					
                         					var pajak = ((row.pjk/100)*row.tnj);
                         					var total = parseInt(row.tnj)+parseInt(pajak);
                         					 return '<ul><li>'+toRp(row.tnj)+'</li><li>'+toRp(pajak)+'</li><li>'+toRp(total)+'</li></ul>';
                         				} 
                         },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					var pajak = ((row.pjk/100)*row.tnj);
                         					var total = parseInt(row.tnj)+parseInt(pajak);
                         					return '<ul><li>'+toRp(pajak)+'</li><li>'+toRp(row.tnj)+'</li></ul>'; 
                         				},
                         }
                     	 

                     ],

                     "columnDefs": [
                             { "width": "2%", "targets": 0 },
                             { "width": "20%", "targets": 1 },
                             // { "width": "15%", "targets": 2 },
                             // { "width": "5%", "targets": 3 },
                             // { "width": "3%", "targets": 4 },
                             // { "width": "20%", "targets": 5 },
                             // { "width": "5%", "targets": 6 },
                             // { "width": "15%", "targets": 7 },
                             // { "width": "20%", "targets": 8 },
                           ],
                     select: true
                 } );

		$('#proses').on('click',function(){
			var tahun 	= $('#yearpicker').val();
			table.ajax.url( '<?php echo base_url()?>'+'laporan/rest_tukin3/'+tahun).load();

		});

		

			
		$('#pdf').on('click',function(){
				var tahun 	= $('#yearpicker').val();
				var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
					];

					var spliting = tahun.split('-');
					var month    = parseInt(spliting[0])-1;
					// $('#container-date').show();
					// $('#bulan').html(monthNames[month]);
					// $('#tahun').html(spliting[1]);

				 function returnData(selected) {
							           return $.ajax({
							              url :  '<?php echo base_url()?>'+'laporan/rest_tukin3/'+tahun,
							              data: {
							                      issession: 1,
							                      selector: selector
							                  },
							              // dataType: "json",
							                  async: false,
							                  error: function() {
							                      alert("Error occured")
							                  }
							            });

							        }

							        var selector = !0;
							        var ajaxObj = returnData(selector);
							        var data    = JSON.parse(ajaxObj.responseText);
							        var insert = {"body":[ 
							                                [
							                                  {text: 'NO', style: 'tableHeader', alignment: 'center'}, 
							                                  {text: 'Uraian \n Kelas \n Jabatan', style: 'tableHeader', alignment: 'center'}, 
							                                  {text: 'Jumlah \n Penerima ', style: 'tableHeader', alignment: 'center'}, 
							                                  {text: 'Tunjangan Kinerja per Kelas \n Jabatan   ', style: 'tableHeader', alignment: 'center'}, 
							                                  {text: '1. Jumlah Tunjangan\n2. Pajak\n 3.Jumlah', style: 'tableHeaderLeft'}, 
							                                  {text: '1. Potongan Pajak \n 2. Jumlah Netton', style: 'tableHeaderLeft'}                    
							                                ],
							                                //Data Yang Harus Di Isi Kelak Menggunakan Perulangan Query atau Rest API
							                                [
							                                  {text: '1', style: 'content', alignment: 'center'}, 
							                                  {text: '2', style: 'content', alignment: 'center'}, 
							                                  {text: '3', style: 'content', alignment: 'center'}, 
							                                  {text: '4', style: 'content', alignment: 'center'}, 
							                                  {text: '5\n6\n7', style: 'content', alignment: 'center'}, 
							                                  {text: '8\n9', style: 'content', alignment: 'center'},
							                                ]
							                            ]

							                     }

							        function buildTableBody(data, columns) {
							            var body = [];
							            var i = 0;
							            data.forEach(function(row) {
							                var dataRow = [];

							                // columns.forEach(function(column) {
							                //     dataRow.push({'text':row[column].toString(),'aligement':'tester '});
							                // })
							               	var pajak 	= ((parseInt(row["pjk"])/100)*parseInt(row["tnj"]));
							                var total   = parseInt(row["tnj"])+parseInt(pajak);
							                dataRow.push({'text':++i,'aligement':'tester '});
							                dataRow.push({'text': row["kelas_jabatan"].toString(),style: 'content'});
							                dataRow.push({'text': row["count_kelas"].toString(),style: 'content',});
							                dataRow.push({'text': toRp(row["tnj"].toString()),style: 'content_right'});
							                dataRow.push(
								                	{
								                		'style':'content_right',
								                		'ul': [
															toRp(row["tnj"].toString()),
															toRp(pajak),
															toRp(total)
														 ]
													}
							                	);       
							                dataRow.push(
							                		{	'style':'content_right',
							                			'ul': [
															toRp(pajak),
															toRp(row["tnj"]),
														 ]
													}

							                	);
							                body.push(dataRow);
							            });

							            return body;
							        }
							  var hasil = buildTableBody(data.data,['kd_satker','nama_satker','count_kelas','tnj','pjk']);
							        for(var loop =0 ; loop<= (hasil.length-1) ;loop++)
							          { 
							            insert.body.push(hasil[loop]);
							          }
							      


							         function tableSample(data, columns) {
							              return {
							                  table: {
							                      headerRows: 4,
							                    dontBreakRows: true,
							                    keepWithHeaderRows: 1,
							                    widths: [30,50,50,120,120,100],
							                    body: insert.body,

							                  }
							              };
							          }


							 	var docDefinition = {
							    //pageMargins: [ 10, 70, 10, 20 ],
							    pageSize: 'A4',
							    // pageOrientation: 'landscape',
							    content: [
							      {
										alignment: 'center',
										columns: [
											{
												text: 'REKAPITULASI DAFTAR PEMBAYARAN TUNJANGAN KINERJA PEGAWAI'
											}
										],
							                        
									},
							     {
							      alignment: 'center',
							      columns: [
							        {
							          text: 'BULAN '+monthNames[month]+' Tahun '+spliting[1]+'\n \n'
							        }
							      ],
							                        
							    },
							      {
										alignment: 'left',
										columns: [
											{
												text: 'Badan SAR Nasional '
											},
											
										],
							                        
									},

							     '\n',
							      tableSample() 
							    ],
							    styles: {
							      header1: {
							        fontSize: 14,
							        bold: true,
							        margin: [0, 0, 0, 10]
							      },
							      header2: {
							        fontSize: 18,
							        bold: true,
							        margin: [0, 0, 0, 10]
							      },
							      subheader: {
							        fontSize: 8,
							        bold: true,
							        margin: [0, 10, 0, 5]
							      },
							      title: {
							        fontSize: 11,
							        bold: true,
							       
							      },
							      content_right: {
							        fontSize: 11,
							        alignment: 'right'
							      },
							      content: {
							        fontSize: 11,
							        alignment: 'center'
							      },
							      content_left: {
							        fontSize: 11,
							        alignment: 'center'
							      },
							      tableExample: {
							        margin: [0, 0, 0, 0]
							      },
							      tableHeaderLeft: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 0, 0, 0],
							        alignment: 'left',
							        color: 'black',
							        fillColor: '#dddcda'
							      },
							      tableHeader: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 0, 0, 0],
							        color: 'black',
							        fillColor: '#dddcda'
							      },
							      tableHeaderRight: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 0, 0, 0],
							        alignment: 'right',
							        color: 'black',
							        fillColor: '#dddcda'
							      },
							      tableHeader2: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 20, 0, 0],
							        color: 'black'
							      },
							      tableHeader3: {
							        bold: true,
							        fontSize: 12,
							        margin: [0, 5, 0, 0],
							        color: 'black'
							      }
							    },
							    defaultStyle: {
							      // alignment: 'justify'
							    }

							    };

							 pdfMake.createPdf(docDefinition).open();
		});
		



		
	});
	
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return e(d)}

	
  </script>		
  