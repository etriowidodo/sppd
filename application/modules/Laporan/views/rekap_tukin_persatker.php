  
	
  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Laporan</a></li>
      <li class="active"><span>Rekap Tunjangan Kinerja Per Satker</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix ">
			<header class="main-box-header clearfix">
				<h2 class="sadow05  ">Rekapitulasi Daftar Pembayaran Tunjangan Kinerja Per Satker</h2>
				<h5 class="sadow05 " id="container-date" style="display: none">Bulan <span id='bulan'></span> Tahun <span id='tahun'></span> </h5>
			</header>
			<div class="main-box-body clearfix ">
				<div class="form-group">
					<label for="input6" class="col-lg-2 control-label black">Tahun</label>
					<div class="col-lg-3">
							<input class="form-control" value="" type="text" name="tahun" id="yearpicker" />	
					</div>
					<div class="col-lg-1">
						<button id='proses' type="button" class="btn btn-info"> 
						<span class="glyphicon glyphicon-search"></span>  Proses
						</button>
					</div>
					<div class="col-lg-2">
							<button type="button" class="btn  btn-danger"> 
							<span class="glyphicon glyphicon-file"></span>  Pdf
							</button>
							<button type="button" class="btn btn-success"> 
							<span class="glyphicon glyphicon-th"></span>  Excel
							</button>
					</div>
				</div>
				<br/>				
				<table id='table' class="tabel black table-striped table-bordered table-hover dataTable">
					<thead>			
						<tr>
							<th>No</th>
							<th >Satker</th>
							<th >Jumlah Penerima</th>
							<th> Jumlah Tukin / Kelas Jabatan</th>
							<th> <ol><li>Jumlah Tunjangan</li><li>Pajak</li><li>Jumlah</li></ol> </th>
							<th ><ol><li>Potongan Pajak</li><li>Jumlah Netto</li></ol></th>
							</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
		


<style>
.table_processing { display:none;}
.top{
	float:right;
}
a.dt-button.red {
	color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
#table th { text-align: center }
#table th:nth-child(5) { text-align: left }
#table td:nth-child(1) { text-align : center }
#table td:nth-child(2) { text-align : center}
#table td:nth-child(5) { text-align : right }
#table td:nth-child(6) { text-align : right}

</style>
 <link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
  	
<script type="text/javascript">
	var save_method; //for save method string

	$(document).ready(function() {	

		$("#yearpicker").datepicker( {
			format: "mm-yyyy",
    		viewMode: "months", 
    		minViewMode: "months",
    		autoclose: true,
		});
		// $("#yearpicker").datepicker( {
		// 	onSelect: function(date) {
  //           alert(date);
  //       },
		// });


		function returnData(selector) {
			   return $.ajax({
			      url : 'http://localhost/basarnas-tukin/laporan/rest_tukin4/05-2017',
			      data: {
			              issession: 1,
			              selector: selector
			          },
			      // dataType: "json",
			          async: false,
			          error: function() {
			              alert("Error occured")
			          }
			    });

			}

			var selector = !0;
			// get return ajax object
			var ajaxObj = returnData(selector);

			var data    = JSON.parse(ajaxObj.responseText);

			console.log(data);
		$('#yearpicker').on('change',function(){
			var monthNames = ["Januari", "Pebruari", "Maret", "April", "Mei", "Juni",
			  "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"
			];

			var spliting = $(this).val().split('-');
			var month    = parseInt(spliting[0]);
			$('#container-date').show();
			$('#bulan').html(monthNames[month]);
			$('#tahun').html(spliting[1]);
			
		});


		
		var table = $('#table').DataTable( {
                     destroy: true,
                     "bLengthChange": false,
                     "bFilter": false,                    
                     "autoWidth": true,
                     "ajax": {
                         "url": '<?php echo base_url()?>'+'laporan/rest_tukin4/01-1990',
                         dataSrc: 'data',
                     },
                     "columns": [
                     	 { "data"	 : function(row, data, index,display)	{ return display.row+1; } },		
                         { "data"    : "nama_satker" },
                         { "data"    : "count_kelas" },
                         { "data"    : function(row, data, index,display)	
                         				{ 
                         					return toRp(row.tnj);
                         				}
                         },

                         { "data"    : function(row, data, index,display)	
                         				{
                         					var total = parseInt(row.tnj)+parseInt(row.pjk);
                         					 return '<ul><li>'+toRp(row.tnj)+'</li><li>'+toRp(row.pjk)+'</li><li>'+toRp(total)+'</li></ul>';
                         				} 
                         },
                         { "data"    : function(row, data, index,display)	
                         				{
                         					return '<ul><li>'+toRp(row.pjk)+'</li><li>'+toRp(row.tnj)+'</li></ul>'; 
                         				},
                         }
                     	 

                     ],

                     "columnDefs": [
                             { "width": "2%", "targets": 0 },
                             { "width": "20%", "targets": 1 },
                             // { "width": "15%", "targets": 2 },
                             // { "width": "5%", "targets": 3 },
                             // { "width": "3%", "targets": 4 },
                             // { "width": "20%", "targets": 5 },
                             // { "width": "5%", "targets": 6 },
                             // { "width": "15%", "targets": 7 },
                             // { "width": "20%", "targets": 8 },
                           ],
                     select: true
                 } );

		$('#proses').on('click',function(){
			var tahun 	= $('#yearpicker').val();
			table.ajax.url( '<?php echo base_url()?>'+'laporan/rest_tukin4/'+tahun).load();
		});

		

			
		
		



		
	});
	
function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)+',00'}

	
  </script>		
  