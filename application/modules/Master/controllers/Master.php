<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_master','master');
		$this->load->model('setting/m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}

	//DATA_REKENING
	public function data_rekening()
	{
		$data['konten']="data_rekening";
		$data['satker']	= $this->setting->get_listsatker();
		$this->_template($data);
	}

	function load_DataRekening()
	{
		$list 	= $this->master->get_DataRekening();
		
		$data 	= array();
		$no 	= $_POST['start'];
		$no 	= $no+1;
		foreach ($list as $dataDB) {
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
			$row[] = "<span class='size'>".$dataDB->no_rek."</span>";
			$row[] = "<span class='size'>".$dataDB->bank."</span>";
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Entry" onclick="entry(`'.$dataDB->nip.'`)">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-plus fa-stack-1x fa-inverse"></i>
			</span></a>
			|
			<a class="table-link" href="javascript:void()" title="Edit" onclick="edit('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link" href="javascript:void()" title="Delete" onclick="deleted('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->master->count_DataRekening(),
						"recordsFiltered" =>$this->master->count_filtered_DataRekening(),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function entry_DataRekening($id=null)
	{
		$data=$this->master->get_DataRekeningById($id);
		echo json_encode($data);
	}	

	function add_DataRekening()
	{
		$data=$this->master->add_DataRekening();
		echo json_encode($data);
	}

	function edit_DataRekening($id)
	{
		$data=$this->master->get_MRekeningById($id);
		echo json_encode($data);
	}

	function SimpanEditDataRekening()
	{
		$id=$this->input->post("id");
		$data=$this->master->SimpanEditDataRekening($id);
		echo json_encode($data);
	}
	
	function delete_DataRekening($id)
	{
		$data=$this->master->delete_DataRekening($id);
		echo json_encode($data);
	}
	//DATA_REKENING

	//DATA_KARYAWAN
	public function data_karyawan()
	{	
		
		$data['konten'] = "data_karyawan";
		$data['satker']	= $this->setting->get_listsatker();
		$this->_template($data);
	}

	function load_data_karyawan()
	{
		$list 	= $this->master->get_data_karyawan();
		
		$data 	= array();
		$no 	= $_POST['start'];
		$no 	= $no+1;
		foreach ($list as $dataDB) {
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->jabatan."</span>";
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->master->count_file(),
						"recordsFiltered" =>$this->master->count_filtered(),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}
}

