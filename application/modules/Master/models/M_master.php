<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_master extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}

    //DATA_REKENING
	function get_DataRekening() {
		$query=$this->_get_datatables_DataRekening();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}

	private function _get_datatables_DataRekening()
	{
		$query="SELECT a.nip, a.nama, b.kd_satker, b.nama_satker, c.id, c.no_rek, c.bank from m_karyawan a
				LEFT JOIN m_satker b ON a.kd_satker = b.kd_satker
				LEFT JOIN m_rekening c ON a.nip = c.nip WHERE 1=1";
	
		if($this->uri->segment(3))
		{
			$id=$this->uri->segment(3);
			// $query.="AND a.level='".$id."' ";
		}
		
		if($_POST['columns'][6]['search']['value'] != ''){
            
                $searchkey6 = $_POST['columns'][6]['search']['value'];
         
                
                if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" AND ( a.`kd_satker` = '".trim($searchkey6)."' ) ";
                else if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" WHERE ( a.`kd_satker` = '".trim($searchkey6)."' ) ";
        }
		
		$column = array('','nip','nama','kd_satker','no_rek','bank','nip');
		$i=0;
		foreach ($column as $item) {
			$column[$i] = $item;
		}
		
		if(isset($_POST['order'])) {
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order)) {
			$order = $order;
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		//echo $query;
		return $query;
	
	}

	public function count_DataRekening() {
		$this->db->from("m_karyawan");
		return $this->db->count_all_results();
	}
	
	function count_filtered_DataRekening() {
		$query=$this->_get_datatables_DataRekening();
		return $this->db->query($query)->num_rows();
	}

	function get_DataRekeningById($id) //id_file
	{
		$this->db->where("nip",$id);
		return $this->db->get("m_karyawan")->row();
	}

	public function add_DataRekening()
	{
		$data=array(
		"nip"=>$this->input->post("nip"),
		"nama"=>$this->input->post("nama"),
		"kd_satker"=>$this->input->post("kd_satker"),
		"no_rek"=>$this->input->post("no_rek"),
		"bank"=>$this->input->post("bank"),
	);
		return $this->db->insert("m_rekening",$data);
	}

	function get_MRekeningById($id) //id_file
	{
		$this->db->where("id",$id);
		return $this->db->get("m_rekening")->row();
	}

	public function SimpanEditDataRekening($id)
	{
		$data=array(
		"nip"=>$this->input->post("nip"),
		"nama"=>$this->input->post("nama"),
		"kd_satker"=>$this->input->post("kd_satker"),
		"no_rek"=>$this->input->post("no_rek"),
		"bank"=>$this->input->post("bank"),
	);
		$this->db->where("id",$id);
		return $this->db->update("m_rekening",$data);
	}
	
	public function delete_DataRekening($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->delete("m_rekening");
	}
	//DATA_REKENING

	//DATA_KARYAWAN
	function get_data_karyawan() {
		$query=$this->_get_datatables_data_karyawan();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}

	private function _get_datatables_data_karyawan()
	{
		$query="SELECT * FROM m_karyawan AS mk
				LEFT JOIN m_satker AS ms ON mk.`kd_satker` = ms.`kd_satker` WHERE 1=1 ";
	
		if($this->uri->segment(3))
		{
			$id=$this->uri->segment(3);
			// $query.="AND a.level='".$id."' ";
		}
		
		if($_POST['columns'][5]['search']['value'] != ''){
            
                $searchkey6 = $_POST['columns'][5]['search']['value'];
         
                
                if($_POST['columns'][5]['search']['value'] != '')
                    $query.=" AND ( mk.`kd_satker` = '".trim($searchkey6)."' ) ";
                else if($_POST['columns'][5]['search']['value'] != '')
                    $query.=" WHERE ( mk.`kd_satker` = '".trim($searchkey6)."' ) ";
        }
		
		$column = array('','nip','nama','jabatan','kelas_jabatan','kd_satker','nip');
		$i=0;
		foreach ($column as $item) {
			$column[$i] = $item;
		}
		
		if(isset($_POST['order'])) {
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order)) {
			$order = $order;
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		//echo $query;
		return $query;
	
	}

	public function count_file() {
		$this->db->from("m_karyawan");
		return $this->db->count_all_results();
	}
	
	function count_filtered() {
		$query=$this->_get_datatables_data_karyawan();
		return $this->db->query($query)->num_rows();
	}
	//DATA_KARYAWAN

}

?>