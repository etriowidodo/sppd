  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Master</a></li>
      <li class="active"><span>Data Rekening</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
  <div class="col-lg-12">
    <div class="main-box clearfix ">
      <header class="main-box-header clearfix">
        <h2 class="sadow05 black b">Data Rekening</h2>
      </header>
      <div class="main-box-body clearfix ">
        <div class="form-group">
          <label for="input6" class="col-lg-2 control-label black">Nama Satker</label>
          <div class="col-lg-3">
              <select class="form-control" name="satker" id="satker">
                <option value=""> -- Pilih Satker -- </option>
					<?php foreach ($satker as $row): ?>
					<option value="<?=$row['kd_satker']?>"><?=$row['nama_satker']?></option>
					<?php endforeach; ?>
              </select>
          </div>
        </div>
  		<br><br><br>
        <table id='table' class="table table-striped table-hover">
          <thead>     
            <tr>
              	<th class='thead' axis="date">No</th>
      			<th class='thead' axis="date">NIP</th>
      			<th class='thead' axis="date">Nama Pegawai</th>
      			<th class='thead' axis="date">Satker</th>
      			<th class='thead' axis="date">Nomor Rekening</th>
      			<th class='thead' axis="date">Bank</th>
      			<th class='thead' axis="date">Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link rel="stylesheet" href="<?php echo base_url();?>/plug/offline/bootstrap-datepicker3.min.css">
<script type='text/javascript' src="<?php echo base_url();?>/plug/offline/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>     


<style>
.table_processing { display:none;}
.top{
  float:right;
}
a.dt-button.red {
  color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
</style>
<script type="text/javascript">
  var save_method; //for save method string
  var table;
  $(document).ready(function() {  
    table = $('#table').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bSortable": true,
      "bFilter":true,
      "sDom": '<"top"B>rt<"bottom"ilp><"clear">',
      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('master/load_DataRekening/'.$this->uri->segment(3).'')?>",
        "type": "POST"
      },
      //Set column definition initialisation properties.
     "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
      ],
    });
    
    $('[name="satker"]').on('change',function(){
      table.column(6).search(this.value).draw();
    });
  });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function simpan()
    {
      var id=$('[name="id"]').val();
      var link='<?php echo base_url("master/add_DataRekening"); ?>'; 

      $('#form').ajaxForm({
      url:link,
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        //if success close modal and reload ajax table
        $('#modal_form').modal('hide');
        reload_table();
      },
        error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      } 
      });     
  };
  
  function simpanedit()
    {
      var id=$('[name="id"]').val();
      var link='<?php echo base_url("master/SimpanEditDataRekening"); ?>'; 

      $('#form_edit').ajaxForm({
      url:link,
      data: $('#form_edit').serialize(),
      dataType: "JSON",
      success: function(data)
      {
        //if success close modal and reload ajax table
        $('#modal_edit').modal('hide');
        reload_table();
      },
        error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
      } 
      });     
  };
    function entry(id)
    {
     var save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('master/entry_DataRekening/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
        	$('[name="nip"]').val(data.nip);
            $('[name="nama"]').val(data.nama);
        	$('[name="kd_satker"]').val(data.kd_satker);
            $('[name="no_rek"]').val(data.no_rek);
            $('[name="bank"]').val(data.bank);
                    
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Entry Data Rekening</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

	function edit(id)
    {
     var save_method = 'update';
      $('#form_edit')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo base_url('master/edit_DataRekening/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
			$('[name="id"]').val(data.id);
        	$('[name="nip"]').val(data.nip);
            $('[name="nama"]').val(data.nama);
        	$('[name="kd_satker"]').val(data.kd_satker);
            $('[name="no_rek"]').val(data.no_rek);
            $('[name="bank"]').val(data.bank);
                    
            $('#modal_edit').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Data Rekening</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }
	
	function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>master/delete_DataRekening/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
  </script>   

  
  <!-- Bootstrap modal -->
  <div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="md-content">
	        <div class="modal-header">
	        <button data-dismiss="modal" class="md-close close">&times;</button>
	        <h4 class="modal-title"><b>Entry Data Rekening</b></h4>
	        </div>
        <div class="modal-body">
  			<form  action="javascript:simpan()" id="form" class="form-horizontal" method="post">
  				<input type="hidden" value="" name="id"/>
				<div class="form-group">
			  		<label class="black col-lg-3 control-label">NIP</label>
				   	<div class="col-lg-4">
				    	<input required readonly type="text" class="form-control"  value='' name="nip">
				   	</div>
			  	</div>

			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Nama</label>
				   	<div class="col-lg-6">
				    	<input required readonly type="text" class="form-control"  value='' name="nama">
				   	</div>
			  	</div>

				<div class="form-group">
			    	<label for="input7" class="col-lg-3 control-label black">Satker</label>
				    <div class="col-lg-6">
				      	<select class="form-control" disabled name="kd_satker" id="kd_satker">
			                <option value=""> -- Pilih Satker -- </option>
								<?php foreach ($satker as $row): ?>
								<option value="<?=$row['kd_satker']?>"><?=$row['nama_satker']?></option>
								<?php endforeach; ?>
			            </select>
				    </div>
			  	</div>
				
				<input type="hidden" value='' name="kd_satker"/>

			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Nomer Rekening</label>
				   	<div class="col-lg-4">
				    	<input required type="text" class="form-control"  value='' name="no_rek">
				   	</div>
			  	</div>

			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Bank</label>
				   	<div class="col-lg-4">
				    	<input required type="text" class="form-control"  value='' name="bank">
				   	</div>
			  	</div>

				<div class="modal-footer">
				    <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpan()">Save</button>
				    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
				</div>			       
			</form>
        </div>
        </div>   
    </div>
</div>
</div>


<!-- Bootstrap modal -->
  <div class="modal fade" id="modal_edit" role="dialog">
    <div class="modal-dialog">
        <div class="md-content">
	        <div class="modal-header">
	        <button data-dismiss="modal" class="md-close close">&times;</button>
	        <h4 class="modal-title"><b>Edit Data Rekening</b></h4>
	        </div>
        <div class="modal-body">
  			<form  action="javascript:simpanedit()" id="form_edit" class="form-horizontal" method="post">
  				<input type="hidden" name="id"/>
				<div class="form-group">
			  		<label class="black col-lg-3 control-label">NIP</label>
				   	<div class="col-lg-4">
				    	<input required readonly type="text" class="form-control"  value='' name="nip">
				   	</div>
			  	</div>

			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Nama</label>
				   	<div class="col-lg-6">
				    	<input required readonly type="text" class="form-control"  value='' name="nama">
				   	</div>
			  	</div>

				<div class="form-group">
			    	<label for="input7" class="col-lg-3 control-label black">Satker</label>
				    <div class="col-lg-6">
				      	<select class="form-control" disabled name="kd_satker" id="kd_satker">
			                <option value=""> -- Pilih Satker -- </option>
								<?php foreach ($satker as $row): ?>
								<option value="<?=$row['kd_satker']?>"><?=$row['nama_satker']?></option>
								<?php endforeach; ?>
			            </select>
				    </div>
			  	</div>
				
				<input type="hidden" value='' name="kd_satker"/>
				
			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Nomer Rekening</label>
				   	<div class="col-lg-4">
				    	<input required type="text" class="form-control"  value='' name="no_rek">
				   	</div>
			  	</div>

			  	<div class="form-group">
			  		<label class="black col-lg-3 control-label">Bank</label>
				   	<div class="col-lg-4">
				    	<input required type="text" class="form-control"  value='' name="bank">
				   	</div>
			  	</div>

				<div class="modal-footer">
				    <button type="submit" id="btnSave" class="btn btn-primary pull-right" onclick="javascript:simpanedit()">Save</button>
				    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
				</div>			       
			</form>
        </div>
        </div>   
    </div>
</div>
</div>
