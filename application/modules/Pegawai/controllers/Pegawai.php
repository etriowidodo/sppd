<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	

	//KELAS_JABATAN
	public function index()
	{
		$data['konten']="home";
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_pegawai()
	{
			
		$list = $this->setting->get_pegawai();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->tempatlahir."</span>";
                        $row[] = "<span class='size'>".$dataDB->tgl_lahir."</span>";                         
                        $row[] = "<span class='size'>".$dataDB->pangkat."</span>";
                        $row[] = "<span class='size'>".$dataDB->gol."</span>";
                        $row[] = "<span class='size'>".$dataDB->jabatan."</span>";
                        $row[] = "<span class='size'>".$dataDB->stat."</span>";
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void();" title="Edit" onclick="edit('.$dataDB->id_pegawai.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void();" title="Hapus" onclick="deleted('.$dataDB->id_pegawai.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_pegawai("m_pegawai"),
						"recordsFiltered" =>$this->setting->count_filtered_pegawai('m_pegawai'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_pegawai()
	{
		$data=$this->setting->add_pegawai();
		echo json_encode($data);
	}

	function edit_pegawai($id)
	{
		$data=$this->setting->get_data_pegawai($id);
		echo json_encode($data);
	}		

	function update_pegawai()
	{
		$id=$this->input->post("id_pegawai");
		$data=$this->setting->update_pegawai($id);
		echo json_encode($data);
	}

	function delete_pegawai($id)
	{
		$data=$this->setting->delete_pegawai($id);
		echo json_encode($data);
	}

}

