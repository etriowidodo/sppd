<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}
	//Penanda Tangan
	function get_pegawai()
	{
		
		$query=$this->_get_datatables_pegawai();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_pegawai()
	{
	$query="SELECT *,if(status_pns>0,'PNS','Non Pns') AS stat,DATE_FORMAT(lahir, '%d-%m-%Y') As tgl_lahir FROM m_pegawai HAVING 1=1 ";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_surtu='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			nip LIKE '%".$searchkey."%' or
			nama LIKE '%".$searchkey."%' or
			tempatlahir LIKE '%".$searchkey."%' or
                        tgl_lahir LIKE '%".$searchkey."%' or  
                        pangkat LIKE '%".$searchkey."%' or
                        gol LIKE '%".$searchkey."%' or
                        jabatan LIKE '%".$searchkey."%' or
                        stat LIKE '%".$searchkey."%'
			) ";
		}

		$column = array('','nip','nama','tempatlahir','pangkat','gol','jabatan','status_pns');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_pegawai($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_pegawai($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_pegawai();
		return $this->db->query($query)->num_rows();
	}

	function add_pegawai()
	{
		
                    $data = array(
                        "nip"           => $this->input->post("nip"),
                        "nama"          => $this->input->post("nama"),
                        "tempatlahir"   => $this->input->post("tempatlahir"),
                        "lahir"         => date("Y-m-d", strtotime($this->input->post("lahir"))),
                        "pangkat"       => $this->input->post("pangkat"),
                        "gol"           => $this->input->post("gol"),
                        "jabatan "      => $this->input->post("jabatan"),
                        "status_pns"    => $this->input->post("status_pns")
                       
                    );
                return $this->db->insert("m_pegawai",$data);
	}

	function get_data_pegawai($id) //id_file
	{
		$this->db->from('m_pegawai');
		$this->db->where('id_pegawai',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_pegawai($id)
	{
	
                   $data = array(
                        "nip"           => $this->input->post("nip"),
                        "nama"          => $this->input->post("nama"),
                        "tempatlahir"   => $this->input->post("tempatlahir"),
                        "lahir"         => date("Y-m-d", strtotime($this->input->post("lahir"))),
                        "pangkat"       => $this->input->post("pangkat"),
                        "gol"           => $this->input->post("gol"),
                        "jabatan "      => $this->input->post("jabatan"),
                        "status_pns"    => $this->input->post("status_pns")
                       
                    );
                $this->db->where("id_pegawai",$id);
		return $this->db->update("m_pegawai",$data);
	}

	public function delete_pegawai($id)
	{
		$this->db->where("id_pegawai",$id);
		$data=$this->db->delete("m_pegawai");
	}
}

?>