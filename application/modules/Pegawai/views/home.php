 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Master</a></li>
      <li class="active"><span>Pegawai</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix" style="min-height: 820px;">
		    <header class="main-box-header clearfix">
		      <h2 class="sadow05 black b">Pegawai
		        <a href="javascript:add()" title="Add Data" class="pull-right">
		            <span class="fa-stack"><i class="fa fa-plus-circle fa-lg"></i></span> 
		        </a> 
		      </h2>
		    </header>
      		<div class="main-box-body clearfix ">
				<table id="table" class="table table-striped table-hover">
					<thead>			
						<tr>
							<th class='thead' axis="string" width='15px'>No</th>
							<th class='thead' axis="date">Nip</th>
							<th class='thead' axis="date">Nama</th>
							<th class='thead' axis="date">Tempat Lahir</th>
                                                        <th class='thead' axis="date">Tanggal Lahir</th>
                                                        <th class='thead' axis="date">Pangkat</th>
                                                        <th class='thead' axis="date">Golongan</th>
                                                        <th class='thead' axis="date">Jabatan</th>
                                                        <th class='thead' axis="date">Status</th>
							<th class='thead' axis="date">Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="md-content">
        	<div class="modal-header">
        		<button data-dismiss="modal" class="md-close close">&times;</button>
        		<h4 class="modal-title"><b>Entry Pegawai</b></h4>
        	</div>
        <div class="modal-body">
			<form action="javascript:simpan()" id="form" class="form-horizontal" method="post">
			    <input type="hidden" value="" name="id_pegawai"/>
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">NIP</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="nip">
			       	</div>
			    </div>
			    
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">Nama</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="nama">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Tanggal Lahir</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control datepicker"  value='' name="lahir">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Tempat Lahir</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="tempatlahir">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Pangkat</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="pangkat">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Golongan</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="gol">
			       	</div>
			    </div>
                             <div class="form-group">
			      	<label class="black col-lg-2 control-label">Jabatan</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="jabatan">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Status</label>
			       	<div class="col-lg-6">
			        	<label><input type="radio" value="1" name="status_pns">PNS</label>
                                        <label><input type="radio" value="0"name="status_pns">Non PNS</label>
			       	</div>
			    </div>

				<div class="modal-footer">
				    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:simpan()">Simpan</button>
				</div>     
			</form>
        </div>
        </div>  
    </div>
</div>

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>			

<script type="text/javascript">
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
        $('.datepicker').datepicker({
            format: "dd-mm-yyyy",
        });
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('pegawai/load_pegawai/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    var save_method;
 
 	function simpan()
	{
	  var id=$('[name="id_pegawai"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("pegawai/add_pegawai"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("pegawai/update_pegawai"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};

	function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('pegawai/edit_pegawai/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            
            $('[name="id_pegawai"]').val(data.id_pegawai); 
            $('[name="nip"]').val(data.nip);
            $('[name="nama"]').val(data.nama);
            var tgl_lahir = new Date(data.lahir);
            var lahir = formatDate(tgl_lahir);
            $('[name="lahir"]').val(lahir);
            $('[name="tempatlahir"]').val(data.tempatlahir);
            $('[name="pangkat"]').val(data.pangkat);
            $('[name="gol"]').val(data.gol);
            $('[name="jabatan"]').val(data.jabatan);
            $('[name="status_pns"]').each(function(i,x){
                if(data.status_pns==x.value){
                $(this).attr("checked","checked");
                }
            })
//            $('[name="status_pns"]').attr("checked", "checked"); 
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Pegawai</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>pegawai/delete_pegawai/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
 </script>

 <script type="text/javascript">
 	var save_method="";
	function add()
	  {
	  save_method="add";
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Pegawai</b>'); // Set Title to Bootstrap modal title
	  }
          function formatDate(date) {
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    return date.getDate() + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
                  }
 </script>

 <script type="text/javascript">
	$('#datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('input#number').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;

		  // format number
		  $(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		  });
		});
</script>

<script type="text/javascript">
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>
	

	