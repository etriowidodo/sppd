<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penandatangan extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	

	//KELAS_JABATAN
	public function index()
	{
		$data['konten']="home";
                $data['pegawai'] = $this->setting->get_ref('m_pegawai');
//                echo '<pre>';
//                print_r($data);exit;
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_penandatangan()
	{
			
		$list = $this->setting->get_penandatangan();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".nl2br($dataDB->rincian)."</span>";
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void();" title="Edit" onclick="edit('.$dataDB->id_penandatangan.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void();" title="Hapus" onclick="deleted('.$dataDB->id_penandatangan.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_penandatangan("m_penandatangan"),
						"recordsFiltered" =>$this->setting->count_filtered_penandatangan('m_penandatangan'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_penandatangan()
	{
		$data=$this->setting->add_penandatangan();
		echo json_encode($data);
	}

	function edit_penandatangan($id)
	{
		$data=$this->setting->get_data_penandatangan($id);
		echo json_encode($data);
	}		

	function update_penandatangan()
	{
		$id=$this->input->post("id_penandatangan");
		$data=$this->setting->update_penandatangan($id);
		echo json_encode($data);
	}

	function delete_penandatangan($id)
	{
		$data=$this->setting->delete_penandatangan($id);
		echo json_encode($data);
	}

}

