<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}
	//Penanda Tangan
	function get_penandatangan()
	{
		
		$query=$this->_get_datatables_penandatangan();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_penandatangan()
	{
	$query="SELECT * FROM m_penandatangan where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_surtu='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			nip LIKE '%".$searchkey."%' or
			nama LIKE '%".$searchkey."%' or
			rincian LIKE '%".$searchkey."%' 
			) ";
		}

		$column = array('','nip','nama','rincian');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_penandatangan($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_penandatangan($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_penandatangan();
		return $this->db->query($query)->num_rows();
	}

	function add_penandatangan()
	{
		
                    $data = array(
                        "nip" => $this->input->post("nip"),
                        "nama" => $this->input->post("nama"),
                        "rincian" => $this->input->post("rincian"),
                        "status" => $this->input->post("status"),
                    );
                return $this->db->insert("m_penandatangan",$data);
	}

	function get_data_penandatangan($id) //id_file
	{
		$this->db->from('m_penandatangan');
		$this->db->where('id_penandatangan',$id);
		$query = $this->db->get();
		return $query->row();
	}
        
        

	public function update_penandatangan($id)
	{
	
                    $data=array(
                    "nip" => $this->input->post("nip"),
                    "nama" => $this->input->post("nama"),
                    "rincian" =>   $this->input->post("rincian"),
                    "status" => $this->input->post("status"),
                    );
                $this->db->where("id_penandatangan",$id);
		return $this->db->update("m_penandatangan",$data);
	}

	public function delete_penandatangan($id)
	{
		$this->db->where("id_penandatangan",$id);
		$data=$this->db->delete("m_penandatangan");
	}
        
        function get_ref($table){
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
                    
        }
}

?>