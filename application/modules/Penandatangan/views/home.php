 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Master</a></li>
      <li class="active"><span>Penanda Tangan</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix" style="min-height: 820px;">
		    <header class="main-box-header clearfix">
		      <h2 class="sadow05 black b">Penanda Tangan
		        <a href="javascript:add()" title="Add Data" class="pull-right">
		            <span class="fa-stack"><i class="fa fa-plus-circle fa-lg"></i></span> 
		        </a> 
		      </h2>
		    </header>
      		<div class="main-box-body clearfix ">
				<table id="table" class="table table-striped table-hover">
					<thead>			
						<tr>
							<th class='thead' axis="string" width='15px'>No</th>
							<th class='thead' axis="date">Nip</th>
							<th class='thead' axis="date">Nama</th>
							<th class='thead' axis="date">Rincian</th>
							<th class='thead' axis="date">Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="md-content">
        	<div class="modal-header">
        		<button data-dismiss="modal" class="md-close close">&times;</button>
        		<h4 class="modal-title"><b>Entry Penanda Tangan</b></h4>
        	</div>
        <div class="modal-body">
			<form action="javascript:simpan()" id="form" class="form-horizontal" method="post">
			    <input type="hidden" value="" name="id_penandatangan"/>
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">NIP</label>
			       	<div class="col-lg-6">
                                    <select  id="nip" name="nip">
                                        <option value="">--Pilih--</option>
                                        <?php foreach($pegawai AS $key=>$val){?>
                                        <option data-status="<?php echo $val->status_pns ?>" data-nama="<?php echo $val->nama ?>" value="<?php echo $val->nip ?>"><?php echo $val->nip?>-<?php echo $val->nama?></option>
                                        <?php }?>
                                    </select>
			        	<input type="hidden" class="form-control"  value='' name="status">
			       	</div>
			    </div>
			    
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">Nama</label>
			       	<div class="col-lg-6">
                                    <input readonly type="text" class="form-control"  value='' name="nama">
			       	</div>
			    </div>

			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">Rincian</label>
			       	<div class="col-lg-6">
                                    <textarea class="form-control" name="rincian" ></textarea>
			       	</div>
			    </div>

				<div class="modal-footer">
				    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:simpan()">Simpan</button>
				</div>     
			</form>
        </div>
        </div>  
    </div>
</div>

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>			

<script type="text/javascript">
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
        $("#nip" ).select2({ width: '100%' });
        $('#nip').on('change',function(){
           var nama     = $(this).find(":selected").data("nama");
           var status   = $(this).find(":selected").data("status");
           $("[name='nama']").val(nama);
           $("[name='status']").val(status);
        })
        table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('penandatangan/load_penandatangan/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    var save_method;
 
 	function simpan()
	{
	  var id=$('[name="id_penandatangan"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("penandatangan/add_penandatangan"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("penandatangan/update_penandatangan"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};

	function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('penandatangan/edit_penandatangan/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            
            $('[name="id_penandatangan"]').val(data.id_penandatangan); 
            $("#nip").select2("val", data.nip);
            $('[name="status"]').val(data.status);
            $('[name="nama"]').val(data.nama);
            $('[name="rincian"]').val(data.rincian);
                    
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Penandatangan</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>penandatangan/delete_penandatangan/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
 </script>

 <script type="text/javascript">
 	var save_method="";
	function add()
	  {
	  save_method="add";
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Penanda Tangan</b>'); // Set Title to Bootstrap modal title
	  }
 </script>

 <script type="text/javascript">
	$('#datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        

    $('input#number').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;

		  // format number
		  $(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		  });
		});
</script>

<script type="text/javascript">
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
</script>
	

	