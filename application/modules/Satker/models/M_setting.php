<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}
	//Penanda Tangan
	function get_satker()
	{
		
		$query=$this->_get_datatables_satker();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_satker()
	{
	$query="SELECT * FROM m_satker where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_surtu='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			kode_satker LIKE '%".$searchkey."%' or
			nama LIKE '%".$searchkey."%' or
			nama_atasan LIKE '%".$searchkey."%' 
			) ";
		}

		$column = array('','kode_satker','nama','nama_atasan');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_satker($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_satker($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_satker();
		return $this->db->query($query)->num_rows();
	}

	function add_satker()
	{
		
                    $data = array(
                        "kode_satker"       => $this->input->post("kode_satker"),
                        "nama"              => $this->input->post("nama"),
                        "nama_atasan"       => $this->input->post("nama_atasan"),
                        "jabatan_atasan"    => $this->input->post("jabatan_atasan"),
                        "pangkat_atasan"    => $this->input->post("pangkat_atasan"),
                        "nip_atasan"        => $this->input->post("nip_atasan"),
                    );
                return $this->db->insert("m_satker",$data);
	}

	function get_data_satker($id) //id_file
	{
		$this->db->from('m_satker');
		$this->db->where('id_satker',$id);
		$query = $this->db->get();
		return $query->row();
	}
        
        

	public function update_satker($id)
	{
	
                    $data=array(
                        "kode_satker"       => $this->input->post("kode_satker"),
                        "nama"              => $this->input->post("nama"),
                        "nama_atasan"       => $this->input->post("nama_atasan"),
                        "jabatan_atasan"    => $this->input->post("jabatan_atasan"),
                        "pangkat_atasan"    => $this->input->post("pangkat_atasan"),
                        "nip_atasan"        => $this->input->post("nip_atasan"),
                    );
                $this->db->where("id_satker",$id);
		return $this->db->update("m_satker",$data);
	}

	public function delete_satker($id)
	{
		$this->db->where("id_satker",$id);
		$data=$this->db->delete("m_satker");
	}
        
        function get_ref($table){
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
                    
        }
}

?>