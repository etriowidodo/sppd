<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sppd extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	

	//KELAS_JABATAN
	public function index()
	{
		$data['konten']         = "home";
                $data['pegawai']        = $this->setting->get_ref("m_pegawai");
                $data['penandatangan']  = $this->setting->get_ref("m_penandatangan");
                $data['surtu']          = $this->setting->get_ref_surtu();
                $data['jenis']          = $this->setting->get_ref('m_jenis'); 
//                print_r($data['surtu']);exit;
		$this->_template($data);
	}

	function load_sppd()
	{
			
		$list = $this->setting->get_sppd();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->no_sppd."</a></span>";
			$row[] = "<span class='size'>".$dataDB->tgl_sppd."</span>";
			$row[] = "<span class='size'>".$dataDB->pejabat_perintah."</span>";
	
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void(0);" title="Edit" onclick="edit('.$dataDB->id_sppd.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void(0);" title="Hapus" onclick="deleted('.$dataDB->id_sppd.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>
                        |
                        <a class="table-link success" href="javascript:void(0);" title="Cetak" onclick="cetak('.$dataDB->id_sppd.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-print fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_sppd("sppd"),
						"recordsFiltered" =>$this->setting->count_filtered_sppd('sppd'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_sppd()
	{
		$data=$this->setting->add_sppd();
		echo json_encode($data);
	}

	function edit_sppd($id)
	{
           
		$data=$this->setting->get_data_sppd($id);
		echo json_encode($data);
	}		
        
	function update_sppd()
	{
		$id=$this->input->post("id_sppd");
		$data=$this->setting->update_sppd($id);
		echo json_encode($data);
	}

	function delete_sppd($id)
	{
		$data=$this->setting->delete_sppd($id);
		echo json_encode($data);
	}
        
        function get_print_sppd($id)
	{
           
		$data=$this->setting->get_print_sppd($id);
		echo json_encode($data);
	}
       

}

