<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}

    

	//KELAS_JABATAN
	function get_sppd()
	{
		
		$query=$this->_get_datatables_sppd();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_sppd()
	{
	$query="SELECT * FROM sppd where 1=1 ";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_sppd='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			no_sppd LIKE '%".$searchkey."%' or
			ket_lain LIKE '%".$searchkey."%' or
			jenis_perjalanan LIKE '%".$searchkey."%' or
			pejabat_perintah LIKE '%".$searchkey."%'
			) ";
		}

		$column = array('','no_sppd','tgl_sppd','pejabat_perintah');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_sppd($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_sppd($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_sppd();
		return $this->db->query($query)->num_rows();
	}

	function add_sppd()
	{
		
		$data=array(
		"id_surtu"          => $this->input->post('surtu'),
                "no_sppd"           => $this->input->post('no_sppd'),
                "tgl_sppd"          => date("Y-m-d", strtotime($this->input->post('tgl_sppd'))),
                "jenis_perjalanan"  => $this->input->post('jenis'),
                "pejabat_perintah"  => $this->input->post('pejabat_perintah'),
                "ket_lain"          => $this->input->post('ket_lain'),
                
                );
		return $this->db->insert("sppd",$data);
	}

	function get_data_sppd($id) //id_file
	{
		$this->db->from('sppd');
		$this->db->where('id_sppd',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_sppd($id)
	{
		
		$data=array(
		//"id_surtu"          => $this->input->post('surtu'),
                "no_sppd"           => $this->input->post('no_sppd'),
                "tgl_sppd"          => date("Y-m-d", strtotime($this->input->post('tgl_sppd'))),
                "jenis_perjalanan"  => $this->input->post('jenis'),
                "pejabat_perintah"  => $this->input->post('pejabat_perintah'),
                "ket_lain"          => $this->input->post('ket_lain'),
                );
		$this->db->where("id_sppd",$id);
		return $this->db->update("sppd",$data);
	}

	public function delete_sppd($id)
	{
		$this->db->where("id_sppd",$id);
		$data=$this->db->delete("sppd");
	}
        
        function get_ref($table){
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
                    
        }
        
        function get_print_sppd($id){
            $sql = "select * from sppd a left join surtu b on a.id_surtu = b.id_surtu where id_sppd = '".$id."'";
            return $this->db->query($sql)->result();
        }
        
        function get_ref_surtu(){
             $sql = "SELECT * FROM surtu WHERE id_surtu NOT IN (SELECT id_surtu FROM sppd) ";
             return $this->db->query($sql)->result();
        }
}

?>