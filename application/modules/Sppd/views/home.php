 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Transaksi</a></li>
      <li class="active"><span>Surat Perintah Perjalanan Dinas ( SPPD) </span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix" style="min-height: 820px;">
		    <header class="main-box-header clearfix">
		      <h2 class="sadow05 black b">Surat Perintah Perjalanan Dinas ( SPPD) 
		        <a href="javascript:add()" title="Add Data" class="pull-right">
		            <span class="fa-stack"><i class="fa fa-plus-circle fa-lg"></i></span> 
		        </a> 
		      </h2>
		    </header>
      		<div class="main-box-body clearfix ">
				<table id="table" class="table table-striped table-hover">
					<thead>			
						<tr>
							<th class='thead' axis="string" width='15px'>No</th>
							<th class='thead' axis="date">Nomor SPPD</th>
							<th class='thead' axis="date">Tanggal SPPD</th>
							<th class='thead' axis="date">Perintah</th>
							<th class='thead' axis="date">Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="md-content">
        	<div class="modal-header">
        		<button data-dismiss="modal" class="md-close close">&times;</button>
        		<h4 class="modal-title"><b>Entry Surat Perintah Perjalanan Dinas ( SPPD) </b></h4>
        	</div>
        <div class="modal-body">
			<form action="javascript:simpan()" id="form" class="form-horizontal" method="post">
			    <input type="hidden" value="" name="id_sppd"/>
                            <div id="group-id-surtu" class="form-group" style="display:none ">
			      	<label class="black col-lg-2 control-label">Surat Tugas</label>
			       	<div class="col-lg-6">
			        	<select   id="surtu" name="surtu">
                                        <option value="">--Pilih--</option>
                                        <?php  foreach ($surtu AS $key => $val) { ?>
                                        <option value="<?php echo $val->id_surtu ?>"><?php echo $val->nomor ?>-<?php echo date('d-m-Y', strtotime($val->tgl_surat)) ?></option>
                                        <?php } ?>
                                    </select>
			       	</div>
			    </div>
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">No SPPD</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="no_sppd">
			       	</div>
                                <label class="black col-lg-1 control-label">Tgl Surat</label>
			       	<div class="col-lg-2">
			        	<input type="text" class="form-control datepicker"  value='' name="tgl_sppd">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Jenis Perjalanan</label>
			       	<div class="col-lg-6">
			        	<select  id="jenis" name="jenis">
                                        <option value="">--Pilih--</option>
                                        <?php  foreach ($jenis AS $key => $val) { ?>
                                        <option value="<?php echo $val->id_jenis ?>"><?php echo $val->kode ?>-<?php echo $val->rekening ?>-<?php echo $val->nama ?></option>
                                        <?php } ?>
                                    </select>
			       	</div>
			    </div>
                            <div class="form-group">
                                <label class="black col-lg-2 control-label">Pejabat Yang Memberi Perintah</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control "  value='' name="pejabat_perintah">
			       	</div>
			    </div>
                           <div class="form-group">
                                <label class="black col-lg-2 control-label">Keterangan Lain</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control "  value='' name="ket_lain">
			       	</div>
			    </div>
				<div class="modal-footer">
				    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:simpan()">Simpan</button>
				</div>     
			</form>
        </div>
        </div>  
    </div>
</div>

<input type="hidden" id="pemda" />
<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>			
<script src="<?php echo base_url()?>plug/js/pdfmake.min.js"></script>	
<script src="<?php echo base_url()?>plug/js/vfs_fonts.js"></script>
<script type="text/javascript">
    
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
        $("#surtu" ).select2({ width: '100%' });
        $("#jenis" ).select2({ width: '100%' });
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('sppd/load_sppd/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    
       
    });
    
    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    var save_method;
 
 	function simpan()
	{
	  var id=$('[name="id"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("sppd/add_sppd"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("sppd/update_sppd/"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};

	function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('sppd/edit_sppd/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
//            console.log(data);
            $('[name="id_sppd"]').val(data.id_sppd);
            $('#surtu').select2("val", data.id_surtu);
            $('#jenis').select2("val", data.jenis_perjalanan);
            $('[name="no_sppd"]').val(data.no_sppd); 
            var tgl_sppd = new Date(data.tgl_sppd);
            var tg_sppd = formatDate(tgl_sppd);
            $('[name="tgl_sppd"]').val(tg_sppd);
            $('[name="pejabat_perintah"]').val(data.pejabat_perintah);
            $('[name="ket_lain"]').val(data.ket_lain);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Surat Perintah Perjalanan Dinas ( SPPD) </b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>sppd/delete_sppd/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
 </script>

 <script type="text/javascript">
     
 	var save_method="";
	function add()
	  {
	  save_method="add";
          $('#group-id-surtu').show();
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Surat Perintah Perjalanan Dinas ( SPPD) </b>'); // Set Title to Bootstrap modal title
	  }
 </script>

 <script type="text/javascript">
        $('#modal_form').on('hidden.bs.modal',  function () {
            $('#group-id-surtu').hide();
        })
	$('#datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('input#number').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;

		  // format number
		  $(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		  });
		});
</script>

<script type="text/javascript">
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  
  
function terbilang(a){
	var bilangan = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas'];

	// 1 - 11
	if(a < 12){
		var kalimat = bilangan[a];
	}
	// 12 - 19
	else if(a < 20){
		var kalimat = bilangan[a-10]+' Belas';
	}
	// 20 - 99
	else if(a < 100){
		var utama = a/10;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%10;
		var kalimat = bilangan[depan]+' Puluh '+bilangan[belakang];
	}
	// 100 - 199
	else if(a < 200){
		var kalimat = 'Seratus '+ terbilang(a - 100);
	}
	// 200 - 999
	else if(a < 1000){
		var utama = a/100;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%100;
		var kalimat = bilangan[depan] + ' Ratus '+ terbilang(belakang);
	}
	// 1,000 - 1,999
	else if(a < 2000){
		var kalimat = 'Seribu '+ terbilang(a - 1000);
	}
	// 2,000 - 9,999
	else if(a < 10000){
		var utama = a/1000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000;
		var kalimat = bilangan[depan] + ' Ribu '+ terbilang(belakang);
	}
	// 10,000 - 99,999
	else if(a < 100000){
		var utama = a/100;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000;
		var kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
	}
	// 100,000 - 999,999
	else if(a < 1000000){
		var utama = a/1000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000;
		var kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
	}
	// 1,000,000 - 	99,999,999
	else if(a < 100000000){
		var utama = a/1000000;
		var depan = parseInt(String(utama).substr(0,4));
		var belakang = a%1000000;
		var kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
	}
	else if(a < 1000000000){
		var utama = a/1000000;
		var depan = parseInt(String(utama).substr(0,4));
		var belakang = a%1000000;
		var kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
	}
	else if(a < 10000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 100000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 1000000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 10000000000000){
		var utama = a/10000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%10000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}
	else if(a < 100000000000000){
		var utama = a/1000000000000;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}

	else if(a < 1000000000000000){
		var utama = a/1000000000000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}

  else if(a < 10000000000000000){
		var utama = a/1000000000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000000000000000;
		var kalimat = terbilang(depan) + ' Kuadriliun '+ terbilang(belakang);
	}

	var pisah = kalimat.split(' ');
	var full = [];
	for(var i=0;i<pisah.length;i++){
	 if(pisah[i] != ""){full.push(pisah[i]);}
	}
	return full.join(' ');
}

$('[name="waktu"]').on('keyup blur change',function(){
    if(this.value.length>0){
        var angka    = (this.value.length<=1)?"0"+this.value:this.value;
        var huruf    = terbilang(this.value);
        var gabungan  = "("+angka+") "+huruf+" Hari Kerja";
        $('[name="detil_waktu"]').val(gabungan);
       }else{
           $('[name="detil_waktu"]').val("");
       }
});

function formatDate(date) {
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    return ('0'+date.getDate()).slice(-2) + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
                  }
function formatDateIndo(date) {
                    var month = new Array();
                    month[0] = "Januari";
                    month[1] = "Pebruari";
                    month[2] = "Maret";
                    month[3] = "April";
                    month[4] = "Mei";
                    month[5] = "Juni";
                    month[6] = "Juli";
                    month[7] = "Agustus";
                    month[8] = "September";
                    month[9] = "Oktober";
                    month[10] = "Nopember";
                    month[11] = "Desember";

                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    return ('0'+date.getDate()).slice(-2) + " " + month[date.getMonth()] + " " + date.getFullYear();
                  }
function cetak(id){
            function getSurtu(selected) {
                return $.ajax({
                    url: "<?php echo site_url('sppd/get_print_sppd/')?>/" + id,
                    data: {
                        issession: 1,
                        selector: selector
                    },
                    // dataType: "json",
                    async: false,
                    error: function () {
                        alert("Error occured");
                    }
                });
            }
            var selector = !0;
            var ajaxObj = getSurtu(selector);
            var result = JSON.parse(ajaxObj.responseText);
            
        
           var pemda = $('#pemda').val();
           var tembusan=[];
           var datatembusan = result[0].tembusan.split(',')
           $.each(datatembusan,function(i,x){
           tembusan.push({text:x,style:'tembusan'});
           });
           var tgl_surat = new Date(result[0].tgl_sppd);
           var tg_surat = formatDateIndo(tgl_surat);
           var logo = pemda;
//            console.log(result[0].skpd.indexOf("("));return false;
           var skpd =result[0].skpd.split('(');
          
           var skpd = skpd[0].toUpperCase();
           
           var row3 =' -';
           if(result[0].status==1){
                row3 = result[0].jabatan_tugas+", "+result[0].pangkat_tugas+" "+result[0].gol_tugas;
            }
           console.log(result);
            var tgl_surtu = new Date(result[0].tgl_surat);
            var tg_surtu = formatDateIndo(tgl_surtu);
//           console.log(tembusan);
//           return false;
           var dd = {
            pageSize: 'Legal',
            pageMargins: [30, 30, 10, 10],
                content: [
                    {
                        style: 'tableExample',
                        table: {
                                widths: [50,400],
                          body: [
                            [
                              {
                                image:logo,width: 50,height: 50
                              },
                              {
                                text: [
                                  {text: 'PEMERINTAH KABUPATEN RAJA AMPAT \n', style:'header1',alignment: 'center'},
                                  {text: skpd+'\n', style:'header1',alignment: 'center'},
                                ],
                              }

                            ]
                          ]
                        },
                        layout: 'noBorders',

                      },
                      {canvas: [{ type: 'line', x1: 40, y1: 5, x2: 580-2*40, y2: 5, lineWidth: 3 }]},
                      {
                        alignment: 'center',

                            columns: [
                                    {
                                        text: 'SURAT PERINTAH PERJALANAN DINAS',
                                        style : 'header2',
                                        decoration: 'underline',
                                    }
                            ],

                       },
                       {
                            alignment: 'center',
                            
                            columns: [
                                    {
                                        text: 'Nomor : '+result[0].no_sppd,
                                        style : 'nomor',
                                    }
                            ],

                        },
                        {
                            style: 'head',
                            table: {
                                    widths: [10, 220, 280],
                                    body: [
                                            [
                                                {text:'1.'}, 
                                                {text:'Pejabatan yang memberi Perintah'}, 
                                                {text:result[0].pejabat_perintah,style:'bold'}
                                            ],
                                            [
                                                {text:'2.'}, 
                                                {text:'Nama / NIP pegawai yang diperintahkan'}, 
                                                {text:result[0].nama_tugas}
                                            ],
                                            [
                                                {text:'3.'}, 
                                                {text:'Jabatan, pangkat dan golongan dari\npegawai yang diperintahkan'}, 
                                                {text:row3}
                                            ],
                                            [
                                                {text:'4.'}, 
                                                {text:'Maksud dari Perjalanan Dinas'}, 
                                                {
                                                    table:{
                                                        widths: [120,5,125],
                                                        body:[
                                                            [
                                                                {text:"Melaksanakan Surat Perintah "+result[0].pejabat_perintah,colSpan:3},
                                                                {},
                                                                {}
                                                            ],
                                                            [
                                                                {text:"Nomor "},
                                                                {text:":"},
                                                                {text:result[0].nomor},
                                                            ],
                                                            [
                                                                {text:"Tanggal "},
                                                                {text:":"},
                                                                {text:tg_surtu},
                                                            ]
                                                        ]
                                                    },
                                                    layout: 'noBorders',
                                                }
                                            ],
                                            [
                                                {text:'5.'}, 
                                                {text:'Alat angkutan yang digunakan'}, 
                                                {text:result[0].alat_transport}
                                            ],
                                            [
                                                {text:'6.'}, 
                                                {
                                                    type: 'lower-alpha',
                                                    ol: [
                                                            'Tempat berangkat',
                                                            'Tujuan',
                                                    ]
                                                }, 
                                                {
                                                    type: 'lower-alpha',
                                                    ol: [
                                                            result[0].tujuan,
                                                            result[0].ke,
                                                        ]
                                                },
                                            ],
                                            [
                                                {text:'7.'}, 
                                                {
                                                    type: 'lower-alpha',
                                                    ol: [
                                                            'Lamanya Perjalanan Dinas',
                                                            'Tanggal Berangkat',
                                                            'Tanggal Kembali',
                                                    ]
                                                }, 
                                                {
                                                    type: 'lower-alpha',
                                                    ol: [
                                                            result[0].detil_waktu,
                                                            "Kesempatan Pertama",
                                                            "Selesai Urusan Dinas",
                                                        ]
                                                },
                                            ],
                                            [
                                                {text:'8.'}, 
                                                {text:'Pembebanan Anggaran\na. Instansi'}, 
                                                {text: result[0].beban+"\n"+result[0].skpd}, 
                                            ],
                                            [
                                                {text:'9.'}, 
                                                {text:'Keterangan Lain - Lain'}, 
                                                {text: result[0].ket_lain}, 
                                            ],
                                    ],
                                    
                            },
                            
                        },
                        {
                            style: 'ttm',
                            table: {
                                    widths: [300, 75,5,120],
                                    body: [
                                            [{text:'',border: [false, false, false, false]}, {text:'Dikeluarkan',border: [false, false, false, false]} ,{text:':',border: [false, false, false, false]},{text:result[0].dikeluarkan,border: [false, false, false, false]}],
                                            [{text:'',border: [false, false, false, false]}, {text:'Pada Tanggal',border: [false, false, false, true]},{text:':',border: [false, false, false, true]},{text:tg_surat,border: [false, false, false, true]}],
                                            
                                    ],
                                    
                            },
//                            layout: 'noBorders'
                        },
                        {
                            style: 'ttd',
                            table: {
                                    widths: [300, 200],
                                    body: [
                                            [{text:''},{text:result[0].rincian_penandatangan.replace(/<br[^>]*>/g, "")+'\n\n\n\n',alignment: 'center' ,style:'headttm' }],
                                            [{text:''},{text:result[0].nama_penandatangan,alignment: 'center' ,style:'headttm' }],
                                            [{text:''},{text:"NIP. "+result[0].nip_penandatangan,alignment: 'center' ,style:'headttm' }],
                                          ],
                                    
                            },
                            layout: 'noBorders'
                        },
                        {
                            style: 'ttd',
                            table: {
                                    widths: [300, 200],
                                    body: [
                                            [{text:'Tembusan disampaikan Kepada Yth',style:'tembusan'},{text:""}],
                                            [{ol:tembusan},{qr:result[0].no_sppd,fit: 50,alignment: 'center' ,style:'headttm',foreground: 'red', background: 'yellow' }],
                                          ],
                                    
                            },
                            layout: 'noBorders',
                            pageBreak: 'after'
                        },
                        {
                            
                            table: {
                                    widths: [260, 260],
                                    body: [
                                               
                                                [
                                                    {text:''},
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'I.'},
                                                                                {text:'Berangkat dari'},
                                                                                {text:':'},
                                                                                {text:result[0].tujuan}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Ke'},
                                                                                {text:':'},
                                                                                {text:result[0].ke}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:tg_surat}, 
                                                                            ],
                                                                            [
                                                                                {text:result[0].rincian_penandatangan.replace(/<br[^>]*>/g, "")+'\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:result[0].nama_penandatangan,alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"NIP. "+result[0].nip_penandatangan,alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    }
                                                ],
                                                [
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'II.'},
                                                                                {text:'Tiba di'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:""+'\n\n\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    },
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'III.'},
                                                                                {text:'Berangkat dari'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Ke'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''+'\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:'',alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    }
                                                ],
                                                [
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'IV.'},
                                                                                {text:'Tiba di'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:""+'\n\n\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    },
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'V.'},
                                                                                {text:'Berangkat dari'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Ke'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''+'\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:'',alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"",alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    }
                                                ],
                                                [
                                                    {
                            
                                                        table: {
                                                                widths: [5,100,5,100],
                                                                body: [
                                                                            [
                                                                                {text:'VI.'},
                                                                                {text:'Tiba di'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:'Pada tanggal'},
                                                                                {text:':'},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''},
                                                                                {text:''}, 
                                                                            ],
                                                                            [
                                                                                {text:result[0].rincian_penandatangan.replace(/<br[^>]*>/g, "")+'\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:result[0].nama_penandatangan,alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"NIP. "+result[0].nip_penandatangan,alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    },
                                                    {
                            
                                                        table: {
                                                                widths: [10,100,0,150],
                                                                body: [
                                                                            [
                                                                                {text:'VII.'},
                                                                                {text:' Telah diperiksa dengan keterangan bahwa, Perjalanan',colSpan:3,style:'smallttd'},
                                                                                {},
                                                                                {} 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:' tersebut atas perintahnya dan semata-mata untuk',colSpan:3,style:'smallttd'},
                                                                                {},
                                                                                {} 
                                                                            ],
                                                                            [
                                                                                {text:''},
                                                                                {text:' kepentingan jabatan dalam waktu sesingkat-singkatnya',colSpan:3,style:'smallttd'},
                                                                                {},
                                                                                {} 
                                                                            ],
                                                                            [
                                                                                {text:result[0].rincian_penandatangan.replace(/<br[^>]*>/g, "")+'\n\n\n\n',alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:result[0].nama_penandatangan,alignment: 'center'  ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ],
                                                                            [
                                                                                {text:"NIP. "+result[0].nip_penandatangan,alignment: 'center' ,colSpan:4,style:'smallttd'},
                                                                                {},
                                                                                {},
                                                                                {}, 
                                                                            ]
                                                                       ]
                                                            },
                                                            layout: 'noBorders',
                                                    }
                                                ],
                                                [
                                                    {text:'VIII. Catatan Lain-lain :' ,colSpan:2,style:'box'},
                                                    {}
                                                ],
                                                [
                                                    
                                                    {text:'IX P E R H A T I A N' ,border: [true, false, true, false],colSpan:2,style:'boxii',decoration: 'underline'},
                                                    {}
                                                ],
                                                [
                                                    
                                                    {text:'Pejabat yang berwenang menerbitkan SPPD, Pegawai yang melakukan Perjalanan Dinas\nPara Pejabat yang mengesahkan berangkat/ tiba, serta bendaharawan bertanggung jawab\nberdasarkan Peraturan - Peraturan Keuangan Negara, apabila Negara menderita Rugi akibat\nkesalahan, kelalaian dan kealpaannya.' ,border: [true, false, true, true],colSpan:2,style:'smallttd2'},
                                                    {}
                                                ],
                                                [
                                                    
                                                    {text:'Catatan :' ,border: [false, false, false, false],colSpan:2,style:'smallttd2'},
                                                    {}
                                                ],
                                                [
                                                    
                                                    {text:'Segera menyerahkan kembali SPPD ini kepada Pemegang Kas masing-masing Instansi se Kabupaten\nRaja Ampat selambat-lambatnya 3 (tiga) hari setelah kembali ke tempat kedudukan dengan\nmelampirkan tiket pergi - pulang' ,border: [false, false, false, false],colSpan:2,style:'smallttd3'},
                                                    {}
                                                ],
                                                [
                                                    {text:'',border: [false, false, false, false]},
                                                    {qr:result[0].no_sppd,fit: 50,alignment: 'center' ,style:'headttm',foreground: 'red', background: 'yellow' ,border: [false, false, false, false]}
                                                ]
                                                
                                           ]
                                }
                        }
                     
                ],
                styles:{
                    box:{
                         margin: [0, 0, 0, 60]
                    },
                    boxii:{
                         margin: [0, 0, 0, 0]
                          
                    },
                    tableExample: 
                    {
                        margin: [40, 0, 0, 0]
                    },
                    center:
                    {   
                        alignment: 'center',
                        margin: [0, 8, 0, 0]
                    },
                    bold:{
                        bold:true
                    },
                    smallttd3: {
                                fontSize: 10,
                                italics: true,
                                margin: [5, 0, 0, 0]
                              },
                    smallttd2: {
                                fontSize: 10,
                                italics: true,
                              },
                     smallttd: {
                                fontSize: 10,
                              },
                    header1: {
                                fontSize: 18,
                                bold: true,
                                alignment: 'center',
                                margin: [0, 10, 0, 5]
                              },
                    header2: {
                                fontSize: 18,
                                bold: true,
                                alignment: 'center',
                                margin: [0, 10, 0, 0]
                              },
                    nomor: {
                                fontSize:11,
                                margin: [0, 0, 0, 5]
                              },
                    head: {
                                margin: [0, 0, 0, 0]
                              },
                    isi: {
                                bold: true,
                                margin: [0, 0, 0, 10]
                              },
                    list: {
                                bold: true,
                                margin: [0, 0, 0, 10]
                              },
                     ttm: {
                                bold: true,
                                margin: [0, 10, 0, 10]
                              },
                    headttm: {
                                bold: true,
                                margin: [0, 0, 0, 0]
                              },
                    tembusan: {
                                fontSize: 11,
                                italics: true,
                                bold: true,
                                margin: [0, 0, 0, 0]
                              },

                }
	
            }
            
            pdfMake.createPdf(dd).open();
        }
       
       function toDataUrl(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }
      
            
             toDataUrl("<?php echo base_url().'assets/img/pemda.png'?>",function(base){
                 $('#pemda').val(base);
            });
  
</script>
	

	