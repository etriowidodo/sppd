<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surtu extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	

	//KELAS_JABATAN
	public function index()
	{
		$data['konten']="home";
                $data['pegawai'] = $this->setting->get_ref("m_pegawai");
                $data['penandatangan'] = $this->setting->get_ref("m_penandatangan");
                $data['skpd'] = $this->setting->get_ref("m_satker");
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_surtu()
	{
			
		$list = $this->setting->get_surtu();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nomor."</a></span>";
			$row[] = "<span class='size'>".$dataDB->tgl_surat."</span>";
			$row[] = "<span class='size'>".$dataDB->tujuan."</span>";
			$row[] = "<span class='size'>".$dataDB->dasar."</span>";


	
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void(0);" title="Edit" onclick="edit('.$dataDB->id_surtu.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void(0);" title="Hapus" onclick="deleted('.$dataDB->id_surtu.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>
                        |
                        <a class="table-link success" href="javascript:void(0);" title="Cetak" onclick="cetak('.$dataDB->id_surtu.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-print fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_surtu("surtu"),
						"recordsFiltered" =>$this->setting->count_filtered_surtu('surtu'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_surtu()
	{
		$data=$this->setting->add_surtu();
		echo json_encode($data);
	}

	function edit_surtu($id)
	{
		$data=$this->setting->get_data_surtu($id);
		echo json_encode($data);
	}		

	function update_surtu()
	{
		$id=$this->input->post("id_surtu");
		$data=$this->setting->update_surtu($id);
		echo json_encode($data);
	}

	function delete_surtu($id)
	{
		$data=$this->setting->delete_surtu($id);
		echo json_encode($data);
	}
        
       

}

