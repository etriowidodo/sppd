<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}

    

	//KELAS_JABATAN
	function get_surtu()
	{
		
		$query=$this->_get_datatables_surtu();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_surtu()
	{
	$query="SELECT * FROM surtu where 1=1 ";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id_surtu='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			nomor LIKE '%".$searchkey."%' or
			tgl_surat LIKE '%".$searchkey."%' or
			dasar LIKE '%".$searchkey."%' or
			nip_tugas LIKE '%".$searchkey."%'
			) ";
		}

		$column = array('','nomor','tgl_surat','tujuan','dasar','keperluan');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_surtu($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_surtu($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_surtu();
		return $this->db->query($query)->num_rows();
	}

	function add_surtu()
	{
		
		$data=array(
		"nomor"         => $this->input->post('nomor'),
                "tgl_surat"     => date("Y-m-d", strtotime($this->input->post('tgl_surat'))),
                "dasar"         => $this->input->post('dasar'),
                "id_skpd"       => $this->input->post('id_skpd'),
                "skpd"          => $this->input->post('skpd'),
                "nip_tugas"     => $this->input->post('nip_tugas'),
                "nama_tugas"    => $this->input->post('nip_tugas'),
                "pangkat_tugas" => $this->input->post('pangkat_tugas'),
                "gol_tugas"     => $this->input->post('gol_tugas'),
                "jabatan_tugas" => $this->input->post('jabatan_tugas'),
                "status_tugas" => $this->input->post('status_tugas'),
                "tujuan"        => $this->input->post('tujuan'),
                "ke"            => $this->input->post('ke'),
                "keperluan"     => $this->input->post('keperluan'),
                "alat_transport"=> $this->input->post('alat_transport'),
                "waktu"         => $this->input->post('waktu'),
                "detil_waktu"   => $this->input->post('detil_waktu'),
                "beban"         => $this->input->post('beban'),
                "nip_penandatangan"      => $this->input->post('nip_penandatangan'),
                "nama_penandatangan"     => $this->input->post('nama_penandatangan'),
                "status_penandatangan"     => $this->input->post('status_penandatangan'),
                "rincian_penandatangan"  => $this->input->post('rincian_penandatangan'),
                "dikeluarkan"   => $this->input->post('dikeluarkan'),
                "pengikut"      => $this->input->post('pengikut'),
                "tembusan"      => $this->input->post('tembusan'),
                "kop"           => $this->input->post('kop'),
                );
		return $this->db->insert("surtu",$data);
	}

	function get_data_surtu($id) //id_file
	{
		$this->db->from('surtu');
		$this->db->where('id_surtu',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_surtu($id)
	{
		
		$data=array(
		"nomor"         => $this->input->post('nomor'),
                "tgl_surat"     => date("Y-m-d", strtotime($this->input->post('tgl_surat'))),
                "dasar"         => $this->input->post('dasar'),
                "id_skpd"       => $this->input->post('id_skpd'),
                "skpd"          => $this->input->post('skpd'),
                "nip_tugas"     => $this->input->post('nip_tugas'),
                "nama_tugas"    => $this->input->post('nama_tugas'),
                "pangkat_tugas" => $this->input->post('pangkat_tugas'),
                "gol_tugas"     => $this->input->post('gol_tugas'),
                "jabatan_tugas" => $this->input->post('jabatan_tugas'),
                "status_tugas" => $this->input->post('status_tugas'),
                "tujuan"        => $this->input->post('tujuan'),
                "ke"            => $this->input->post('ke'),
                "keperluan"     => $this->input->post('keperluan'),
                "alat_transport"=> $this->input->post('alat_transport'),
                "waktu"         => $this->input->post('waktu'),
                "detil_waktu"   => $this->input->post('detil_waktu'),
                "beban"         => $this->input->post('beban'),
                "nip_penandatangan"      => $this->input->post('nip_penandatangan'),
                "nama_penandatangan"     => $this->input->post('nama_penandatangan'),
                "status_penandatangan"     => $this->input->post('status_penandatangan'),
                "rincian_penandatangan"  => $this->input->post('rincian_penandatangan'),
                "dikeluarkan"   => $this->input->post('dikeluarkan'),
                "pengikut"      => $this->input->post('pengikut'),
                "tembusan"      => $this->input->post('tembusan'),
                "kop"           => $this->input->post('kop'),
                );
		$this->db->where("id_surtu",$id);
		return $this->db->update("surtu",$data);
	}

	public function delete_surtu($id)
	{
		$this->db->where("id_surtu",$id);
		$data=$this->db->delete("surtu");
	}
        
        function get_ref($table){
            $this->db->from($table);
            $query = $this->db->get();
            return $query->result();
                    
        }
}

?>