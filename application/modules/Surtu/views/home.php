 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Transaksi</a></li>
      <li class="active"><span>Surat Tugas</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix" style="min-height: 820px;">
		    <header class="main-box-header clearfix">
		      <h2 class="sadow05 black b">Surat Tugas
		        <a href="javascript:add()" title="Add Data" class="pull-right">
		            <span class="fa-stack"><i class="fa fa-plus-circle fa-lg"></i></span> 
		        </a> 
		      </h2>
		    </header>
      		<div class="main-box-body clearfix ">
				<table id="table" class="table table-striped table-hover">
					<thead>			
						<tr>
							<th class='thead' axis="string" width='15px'>No</th>
							<th class='thead' axis="date">Nomor</th>
							<th class='thead' axis="date">Tanggal Surat</th>
							<th class='thead' axis="date">Tujuan</th>
							<th class='thead' axis="date">Dasar</th>
							<th class='thead' axis="date">Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="md-content">
        	<div class="modal-header">
        		<button data-dismiss="modal" class="md-close close">&times;</button>
        		<h4 class="modal-title"><b>Entry Surat Tugas</b></h4>
        	</div>
        <div class="modal-body">
			<form action="javascript:simpan()" id="form" class="form-horizontal" method="post">
			    <input type="hidden" value="" name="id_surtu"/>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Dasar</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control"  value='' name="dasar">
			       	</div>
                                <label class="black col-lg-2 control-label">Kop Bupati</label>
                                <div class="col-lg-1">
                                    <input type="checkbox" class="form-control"  value='1' name="kop">
			       	</div>
			    </div>
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">No Surat</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="nomor">
			       	</div>
                                <label class="black col-lg-1 control-label">Tgl Surat</label>
			       	<div class="col-lg-2">
			        	<input type="text" class="form-control datepicker"  value='' name="tgl_surat">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">SKPD</label>
			       	<div class="col-lg-6">
                                        <select  id="id_skpd" name="id_skpd">
                                        <option value="">--Pilih--</option>
                                        <?php foreach ($skpd AS $key => $val) { ?>
                                            <option data-nama="<?php echo $val->nama ?>"    value="<?php echo $val->id_satker ?>"><?php echo $val->kode_satker ?>-<?php echo $val->nama ?></option>
                                        <?php } ?>
                                    </select>
			        	<input type="hidden" class="form-control "  value='' name="skpd">
			       	</div>
			    </div>
			    <div class="form-group">
			      	<label class="black col-lg-2 control-label">Kepada</label>
			       	<div class="col-lg-8">
                                    <select  id="nip_tugas" name="nip_tugas">
                                        <option value="">--Pilih--</option>
                                        <?php foreach ($pegawai AS $key => $val) { ?>
                                            <option data-status="<?php echo $val->status_pns ?>" data-nama="<?php echo $val->nama ?>" data-pangkat="<?php echo $val->pangkat ?>"  data-gol="<?php echo $val->gol ?>" data-jabatan="<?php echo $val->jabatan ?>" value="<?php echo $val->nip ?>"><?php echo $val->nip ?>-<?php echo $val->nama ?>-<?php echo $val->gol ?>-<?php echo $val->pangkat ?>-<?php echo $val->jabatan ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" value="" name="nama_tugas"/>
                                    <input type="hidden" value="" name="pangkat_tugas"/>
                                    <input type="hidden" value="" name="gol_tugas"/>
                                    <input type="hidden" value="" name="jabatan_tugas"/>
                                    <input type="hidden" value="" name="status_tugas"/>
                                    
			       	</div>  
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Dari</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="tujuan">
			       	</div>
                                <label class="black col-lg-1 control-label">Ke</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="ke">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Keperluan</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control"  value='' name="keperluan">
			       	</div>
                                <label class="black col-lg-1 control-label">Alat Transportasi</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control "  value='' name="alat_transport">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Lama Perjalanan</label>
			       	<div class="col-lg-2">
                                    <input type="number" min="1" class="form-control "  value='' name="waktu">
			       	</div>
                                <div class="col-lg-5">
                                    <input type="text"  class="form-control "  value='' name="detil_waktu">
			       	</div>
			    </div>
                            <div class="form-group">
                                <label class="black col-lg-2 control-label">Beban Anggaran</label>
			       	<div class="col-lg-6">
			        	<input type="text" class="form-control "  value='' name="beban">
			       	</div>
			    </div>
                             <div class="form-group">
                                <label class="black col-lg-2 control-label">Penanda Tangan</label>
			       	<div class="col-lg-8">
			        	<select  id="nip_penandatangan" name="nip_penandatangan">
                                        <option value="">--Pilih--</option>
                                        <?php foreach ($penandatangan AS $key => $val) { ?>
                                            <option data-status="<?php echo $val->status ?>" data-nama="<?php echo $val->nama ?>" data-rincian="<?php echo nl2br($val->rincian) ?>"  value="<?php echo $val->nip ?>"><?php echo $val->nip ?>-<?php echo $val->nama ?>-<?php echo $val->rincian ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" value="" name="nama_penandatangan"/>
                                    <input type="hidden" value="" name="rincian_penandatangan"/>
                                    <input type="hidden" value="" name="status_penandatangan"/>
			       	</div>
			    </div>
                             <div class="form-group">
			      	<label class="black col-lg-2 control-label">Dikeluarkan Di</label>
			       	<div class="col-lg-4">
			        	<input type="text" class="form-control "  value='' name="dikeluarkan">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Pengikut</label>
			       	<div class="col-lg-10">
			        	<input data-role="tagsinput" type="text" class="form-control"  value="" name="pengikut">
			       	</div>
			    </div>
                            <div class="form-group">
			      	<label class="black col-lg-2 control-label">Tembusan</label>
			       	<div class="col-lg-10">
			        	<input data-role="tagsinput" type="text" class="form-control"  value='' name="tembusan">
			       	</div>
			    </div>
                           
				<div class="modal-footer">
				    <button type="submit" class="btn btn-primary pull-right" onclick="javascript:simpan()">Simpan</button>
				</div>     
			</form>
        </div>
        </div>  
    </div>
</div>
<input type="hidden" id="bupati" />
<input type="hidden" id="pemda" />
<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>			
<script src="<?php echo base_url()?>plug/js/pdfmake.min.js"></script>	
<script src="<?php echo base_url()?>plug/js/vfs_fonts.js"></script>
<script type="text/javascript">
    
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
        $("#nip_tugas" ).select2({ width: '100%' });
        $('#nip_tugas').on('change',function(){
           var nama         = $(this).find(":selected").data("nama");
           var pangkat      = $(this).find(":selected").data("pangkat");
           var gol          = $(this).find(":selected").data("gol");
           var jabatan      = $(this).find(":selected").data("jabatan");
           var status      = $(this).find(":selected").data("status");
           $("[name='nama_tugas']").val(nama);
           $("[name='pangkat_tugas']").val(pangkat);
           $("[name='gol_tugas']").val(gol);
           $("[name='jabatan_tugas']").val(jabatan);
           $("[name='status_tugas']").val(status);
        });
        $("#nip_penandatangan" ).select2({ width: '100%' });
        $('#nip_penandatangan').on('change',function(){
           var nama_penandatangan         = $(this).find(":selected").data("nama");
           var rincian_penandatangan      = $(this).find(":selected").data("rincian");
           var status_penandatangan      = $(this).find(":selected").data("status");
           $("[name='nama_penandatangan']").val(nama_penandatangan);
           $("[name='rincian_penandatangan']").val(rincian_penandatangan);
           $("[name='status_penandatangan']").val(status_penandatangan);
        });
        $("#id_skpd" ).select2({ width: '100%' });
        $('#id_skpd').on('change',function(){
           var nama_skpd         = $(this).find(":selected").data("nama");
           $("[name='skpd']").val(nama_skpd);
        });
        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('surtu/load_surtu/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    
       
    });
    
    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    var save_method;
 
 	function simpan()
	{
	  var id=$('[name="id"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("surtu/add_surtu"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("surtu/update_surtu/"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};

	function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('surtu/edit_surtu/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            var tunjangan = data.tunjangan > 0 ? numberWithCommas(data.tunjangan) : "";
            $('[name="id_surtu"]').val(data.id_surtu); 
            $('[name="dasar"]').val(data.dasar);
            $('[name="nomor"]').val(data.nomor);
            var tgl_surat = new Date(data.tgl_surat);
            var tg_surat = formatDate(tgl_surat);
            $('[name="tgl_surat"]').val(tg_surat);
            $("#id_skpd").select2("val", data.id_skpd);
            $('[name="skpd"]').val(data.skpd);
            $("#nip_tugas").select2("val", data.nip_tugas);
            $('[name="gol_tugas"]').val(data.gol_tugas);
            $('[name="pangkat_tugas"]').val(data.pangkat_tugas);
            $('[name="nama_tugas"]').val(data.nama_tugas);
            $('[name="jabatan_tugas"]').val(data.jabatan_tugas);
            $('[name="status_tugas"]').val(data.status_tugas);
            $('[name="tujuan"]').val(data.tujuan);
            $('[name="ke"]').val(data.ke);
            $('[name="keperluan"]').val(data.keperluan);    
            $('[name="alat_transport"]').val(data.alat_transport);  
            $('[name="waktu"]').val(data.waktu);
            $('[name="detil_waktu"]').val(data.detil_waktu);  
            $('[name="beban"]').val(data.beban);
            $('#nip_penandatangan').select2("val", data.nip_penandatangan);
            $('[name="nama_penandatangan"]').val(data.nama_penandatangan);
            $('[name="rincian_penandatangan"]').val(data.rincian_penandatangan);
            $('[name="status_penandatangan"]').val(data.status_penandatangan);
            $('[name="dikeluarkan"]').val(data.dikeluarkan);
            $('[name="pengikut"]').tagsinput('add',data.pengikut);
            $('[name="tembusan"]').tagsinput('add',data.tembusan);
            if(data.kop==1)
            {
                    $('[name="kop"]').attr("checked","checked");
            }else{
                    $('[name="kop"]').removeAttr("checked");
            }
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Surat Tugas</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>surtu/delete_surtu/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
 </script>

 <script type="text/javascript">
 	var save_method="";
	function add()
	  {
	  save_method="add";
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Surat Tugas</b>'); // Set Title to Bootstrap modal title
	  }
 </script>

 <script type="text/javascript">
	$('#datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('input#number').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;

		  // format number
		  $(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		  });
		});
</script>

<script type="text/javascript">
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  
  
function terbilang(a){
	var bilangan = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan','Sepuluh','Sebelas'];

	// 1 - 11
	if(a < 12){
		var kalimat = bilangan[a];
	}
	// 12 - 19
	else if(a < 20){
		var kalimat = bilangan[a-10]+' Belas';
	}
	// 20 - 99
	else if(a < 100){
		var utama = a/10;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%10;
		var kalimat = bilangan[depan]+' Puluh '+bilangan[belakang];
	}
	// 100 - 199
	else if(a < 200){
		var kalimat = 'Seratus '+ terbilang(a - 100);
	}
	// 200 - 999
	else if(a < 1000){
		var utama = a/100;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%100;
		var kalimat = bilangan[depan] + ' Ratus '+ terbilang(belakang);
	}
	// 1,000 - 1,999
	else if(a < 2000){
		var kalimat = 'Seribu '+ terbilang(a - 1000);
	}
	// 2,000 - 9,999
	else if(a < 10000){
		var utama = a/1000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000;
		var kalimat = bilangan[depan] + ' Ribu '+ terbilang(belakang);
	}
	// 10,000 - 99,999
	else if(a < 100000){
		var utama = a/100;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000;
		var kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
	}
	// 100,000 - 999,999
	else if(a < 1000000){
		var utama = a/1000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000;
		var kalimat = terbilang(depan) + ' Ribu '+ terbilang(belakang);
	}
	// 1,000,000 - 	99,999,999
	else if(a < 100000000){
		var utama = a/1000000;
		var depan = parseInt(String(utama).substr(0,4));
		var belakang = a%1000000;
		var kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
	}
	else if(a < 1000000000){
		var utama = a/1000000;
		var depan = parseInt(String(utama).substr(0,4));
		var belakang = a%1000000;
		var kalimat = terbilang(depan) + ' Juta '+ terbilang(belakang);
	}
	else if(a < 10000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 100000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 1000000000000){
		var utama = a/1000000000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000000000;
		var kalimat = terbilang(depan) + ' Milyar '+ terbilang(belakang);
	}
	else if(a < 10000000000000){
		var utama = a/10000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%10000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}
	else if(a < 100000000000000){
		var utama = a/1000000000000;
		var depan = parseInt(String(utama).substr(0,2));
		var belakang = a%1000000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}

	else if(a < 1000000000000000){
		var utama = a/1000000000000;
		var depan = parseInt(String(utama).substr(0,3));
		var belakang = a%1000000000000;
		var kalimat = terbilang(depan) + ' Triliun '+ terbilang(belakang);
	}

  else if(a < 10000000000000000){
		var utama = a/1000000000000000;
		var depan = parseInt(String(utama).substr(0,1));
		var belakang = a%1000000000000000;
		var kalimat = terbilang(depan) + ' Kuadriliun '+ terbilang(belakang);
	}

	var pisah = kalimat.split(' ');
	var full = [];
	for(var i=0;i<pisah.length;i++){
	 if(pisah[i] != ""){full.push(pisah[i]);}
	}
	return full.join(' ');
}

$('[name="waktu"]').on('keyup blur change',function(){
    if(this.value.length>0){
        var angka    = (this.value.length<=1)?"0"+this.value:this.value;
        var huruf    = terbilang(this.value);
        var gabungan  = "("+angka+") "+huruf+" Hari Kerja";
        $('[name="detil_waktu"]').val(gabungan);
       }else{
           $('[name="detil_waktu"]').val("");
       }
});

function formatDate(date) {
                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    return ('0'+date.getDate()).slice(-2) + "-" + ('0'+(date.getMonth()+1)).slice(-2) + "-" + date.getFullYear();
                  }
function formatDateIndo(date) {
                    var month = new Array();
                    month[0] = "Januari";
                    month[1] = "Pebruari";
                    month[2] = "Maret";
                    month[3] = "April";
                    month[4] = "Mei";
                    month[5] = "Juni";
                    month[6] = "Juli";
                    month[7] = "Agustus";
                    month[8] = "September";
                    month[9] = "Oktober";
                    month[10] = "Nopember";
                    month[11] = "Desember";

                    var hours = date.getHours();
                    var minutes = date.getMinutes();
                    var ampm = hours >= 12 ? 'pm' : 'am';
                    hours = hours % 12;
                    hours = hours ? hours : 12; // the hour '0' should be '12'
                    minutes = minutes < 10 ? '0'+minutes : minutes;
                    var strTime = hours + ':' + minutes + ' ' + ampm;
                    return ('0'+date.getDate()).slice(-2) + " " + month[date.getMonth()] + " " + date.getFullYear();
                  }
function cetak(id){
            function getSurtu(selected) {
                return $.ajax({
                    url: "<?php echo site_url('surtu/edit_surtu/')?>/" + id,
                    data: {
                        issession: 1,
                        selector: selector
                    },
                    // dataType: "json",
                    async: false,
                    error: function () {
                        alert("Error occured");
                    }
                });
            }
            var selector = !0;
            var ajaxObj = getSurtu(selector);
            var result = JSON.parse(ajaxObj.responseText);
            
           var bupati = $('#bupati').val();
           var pemda = $('#pemda').val();
           
           var tembusan=[];
           var datatembusan = result.tembusan.split(',')
           $.each(datatembusan,function(i,x){
           tembusan.push({text:x,style:'tembusan'});
           });
           var tgl_surat = new Date(result.tgl_surat);
           var tg_surat = formatDateIndo(tgl_surat);
           var logo = "";
           if(result.kop==1){
                logo = bupati;
            }else{
                logo = pemda;
            }
//           console.log(tembusan);
//           return false;
           var dd = {
            pageSize: 'A4',
            pageMargins: [30, 30, 10, 10],
                content: [
                    
                        {
                            image: logo,
                            width : 70,
                            style: 'center'
                        },
                        {
                            alignment: 'center',
                            
                            columns: [
                                    {
                                        text: 'S U R A T    P E R I N T A H    T U G A S',
                                        style : 'header1',
                                        decoration: 'underline',
                                    }
                            ],

                        },
                        {
                            alignment: 'center',
                            
                            columns: [
                                    {
                                        text: 'Nomor : '+result.nomor,
                                        style : 'nomor',
                                    }
                            ],

                        },
                        {
                            style: 'head',
                            table: {
                                    widths: [100, 5, 400],
                                    body: [
                                            [{text:'DASAR'}, ':', {text:'DASAR'}],
                                            [{text:'KEPADA'}, ':', {text:result.nama_tugas}],
                                            [{text:''}, '', {text:result.jabatan_tugas}]
                                    ],
                                    
                            },
                            layout: 'noBorders'
                        },
                        {
                            style: 'isi',
                            table: {
                                    widths: [100, 5, 400],
                                    body: [
                                            [{text:'SUPAYA'}, ':', 
                                               {
                                                 
                                                 ol: [
                                                     {text:'Seterimanya Surat Perintah Tugas ini segera berangkat dari : '+result.tujuan+' ke '+result.ke+' dengan '+result.alat_transport+' '+result.keperluan,style:'list'},
                                                     {text:'Surat Perintah Tugas ini haruslah dilengkapi dengan Surat Perintah Perjalanan Dinas',style:'list'},
                                                     {text:'Tugas ini dilakukan atas biaya : '+result.beban,style:'list'},
                                                     {text:'Lamanya perjalanan dinas : '+result.detil_waktu,style:'list'},
                                                     {text:'Pengikut : '+result.pengikut,style:'list'},
                                                     {text:'Sekembalinya di Waisai segera melaporkan hasilnya kepada Bupati Raja Ampat ',style:'list'},
                                                     {text:'Diindahkan dan dilaksanakan dengan penuh rasa tanggung jawab ',style:'list'},
                                                     ]
                                                },
                                            ],
                                            
                                    ],
                                    
                            },
                            layout: 'noBorders'
                        },
                        {
                            style: 'ttm',
                            table: {
                                    widths: [300, 75,5,120],
                                    body: [
                                            [{text:'',border: [false, false, false, false]}, {text:'Dikeluarkan',border: [false, false, false, false]} ,{text:':',border: [false, false, false, false]},{text:result.dikeluarkan,border: [false, false, false, false]}],
                                            [{text:'',border: [false, false, false, false]}, {text:'Pada Tanggal',border: [false, false, false, true]},{text:':',border: [false, false, false, true]},{text:tg_surat,border: [false, false, false, true]}],
                                            
                                    ],
                                    
                            },
//                            layout: 'noBorders'
                        },
                        {
                            style: 'ttd',
                            table: {
                                    widths: [300, 200],
                                    body: [
                                            [{text:''},{text:result.rincian_penandatangan.replace(/<br[^>]*>/g, "")+'\n\n\n\n',alignment: 'center' ,style:'headttm' }],
                                            [{text:''},{text:result.nama_penandatangan,alignment: 'center' ,style:'headttm' }],
                                            [{text:''},{text:"NIP. "+result.nip_penandatangan,alignment: 'center' ,style:'headttm' }],
                                          ],
                                    
                            },
                            layout: 'noBorders'
                        },
                        {
                            style: 'ttd',
                            table: {
                                    widths: [300, 200],
                                    body: [
                                            [{text:'Tembusan disampaikan Kepada Yth',style:'tembusan'},{text:""}],
                                            [{ol:tembusan},{qr:result.nomor,fit: 50,alignment: 'center' ,style:'headttm',foreground: 'red', background: 'yellow' }],
                                          ],
                                    
                            },
                            layout: 'noBorders'
                        },
                ],
                styles:{
                    center:
                    {   
                        alignment: 'center'
                    },
                    header1: {
                                fontSize: 15,
                                bold: true,
                                margin: [0, 10, 0, 5]
                              },
                    nomor: {
                                bold: true,
                                margin: [0, 0, 0, 20]
                              },
                    head: {
                                bold: true,
                                margin: [0, 0, 0, 10]
                              },
                    isi: {
                                bold: true,
                                margin: [0, 0, 0, 10]
                              },
                    list: {
                                bold: true,
                                margin: [0, 0, 0, 10]
                              },
                     ttm: {
                                bold: true,
                                margin: [0, 10, 0, 10]
                              },
                    headttm: {
                                bold: true,
                                margin: [0, 0, 0, 0]
                              },
                    tembusan: {
                                fontSize: 11,
                                italics: true,
                                bold: true,
                                margin: [0, 0, 0, 0]
                              },

                }
	
            }
            
            pdfMake.createPdf(dd).open();
        }
       
       function toDataUrl(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }
           toDataUrl("<?php echo base_url().'assets/img/bupati.png'?>",function(base){
                $('#bupati').val(base);
            });
            
             toDataUrl("<?php echo base_url().'assets/img/pemda.png'?>",function(base){
                 $('#pemda').val(base);
            });
  
</script>
	

	