<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sync extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	//SATKER
	public function view_satker()
	{
		$data['konten']="view_satker";
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_satker()
	{
			
		$list = $this->setting->get_satker();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->kd_satker."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
					
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Edit" onclick="edit('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_satker("m_satker"),
						"recordsFiltered" =>$this->setting->count_filtered_satker('m_satker'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function index()
	{
		$data['konten']="sync";
		$this->_template($data);
	}

	function add_satker()
	{
		$data=$this->setting->add_satker();
		echo json_encode($data);
	}

	function edit_satker($id)
	{
		$data=$this->setting->get_data_satker($id);
		echo json_encode($data);
	}

	function update_satker()
	{
		$id=$this->input->post("id");
		$data=$this->setting->update_satker($id);
		echo json_encode($data);
	}

	function delete_satker($id)
	{
		$data=$this->setting->delete_satker($id);
		echo json_encode($data);
	}
	//SATKER

	//KELAS_JABATAN
	public function view_kelas_jabatan()
	{
		$data['konten']="view_kelas_jabatan";
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_kelas_jabatan()
	{
			
		$list = $this->setting->get_kelas_jabatan();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->kd_kelas."</a></span>";
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".number_format($dataDB->tunjangan)."</span>";
			$row[] = "<span class='size'>".$dataDB->pajak."</span>";


	
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Edit" onclick="edit('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted('.$dataDB->id.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->setting->count_kelas_jabatan("m_kelas"),
						"recordsFiltered" =>$this->setting->count_filtered_kelas_jabatan('m_kelas'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function add_kelas_jabatan()
	{
		$data=$this->setting->add_kelas_jabatan();
		echo json_encode($data);
	}

	function edit_kelas_jabatan($id)
	{
		$data=$this->setting->get_data_kelas_jabatan($id);
		echo json_encode($data);
	}		

	function update_kelas_jabatan()
	{
		$id=$this->input->post("id");
		$data=$this->setting->update_kelas_jabatan($id);
		echo json_encode($data);
	}

	function delete_kelas_jabatan($id)
	{
		$data=$this->setting->delete_kelas_jabatan($id);
		echo json_encode($data);
	}

}

