<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tukin extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->load->model('m_tukin','tukin');
		$this->load->model('setting/m_setting','setting');
		$this->m_konfig->validasi_session(array("admin"));
	}
	
	function _template($data)
	{
		$this->load->view('template/main',$data);	
	}
	
	//ENTRY_TUKIN
	public function entry_tukin()
	{
		$data['konten']="entry_tukin";
		$data['satker']	= $this->setting->get_listsatker();
		$this->_template($data);
	}
	function getNumTukin($nip)
	{
		$this->db->where("nip",$nip);
		$this->db->where("SUBSTR(tgl,1,7)",date("Y-m"));
		$data=$this->db->get("t_tukin")->num_rows();
		return $data;
	}
	
	function load_entry_tukin()
	{
		$list 	= $this->tukin->get_data_tukin();
		
		$data 	= array();
		$no 	= $_POST['start'];
		$no 	= $no+1;
		foreach ($list as $dataDB) {
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->jabatan."</span>";
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
			
			if($this->getNumTukin($dataDB->nip))
			{
				$row[]='<center>-</center>';
			}else
			{
				$row[] = '<center>
				<a class="table-link" href="javascript:void()" title="Entry Tukin" onclick="entry(`'.$dataDB->nip.'`)">
				<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-plus fa-stack-1x fa-inverse"></i>
				</span></a></center>';	
			}
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tukin->count_file(),
						"recordsFiltered" =>$this->tukin->count_filtered(),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}

	function entry_DataTukin($id=null)
	{
		$data=$this->tukin->get_DataTukin($id);
		echo json_encode($data);
	}	

	function add_tukin()
	{
		$data=$this->tukin->add_tukin();
		echo json_encode($data);
	}

	function update_tukin()
	{
		$id=$this->input->post("nip");
		$data=$this->tukin->update_tukin($id);
		echo json_encode($data);
	}

	//ENTRY_TUKIN

	//PERHITUNGAN_TUKIN
	public function perhitungan_tukin()
	{
		$data['konten']="perhitungan_tukin";
		$data['satker']	= $this->setting->get_listsatker();
		$this->_template($data);
	}

	function loadTablesPerhitunganTukin()
	{
		$list 	= $this->tukin->get_PerhitunganTukin();
		$data 	= array();
		$no 	= $_POST['start'];
		$no 	= $no+1;
		foreach ($list as $dataDB){
		$tunjangan = $dataDB->tunjangan;
		$potongan = $dataDB->potongan;
		$jumlah_potongan = ($tunjangan * $potongan)/100;
		$jumlah = $tunjangan - $jumlah_potongan; 
		
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
			$row[] = "<span class='size'>".$dataDB->tgl."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";				     
			$row[] = "<span class='size'>".$dataDB->jabatan."</span>";					 
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".number_format($tunjangan)."</span>";		
			$row[] = "<span class='size'>".$potongan."</span>";	
			$row[] = "<span class='size'>".number_format($jumlah)."</span>";	
			//$row[] = "<span class='size'>".number_format($dataDB->apbn)."</span>";
			
			/*
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Tampilkan" onclick="show_renja(\''.$dataDB->kode.'\', \''.$dataDB->tahun.'\')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-list-alt fa-stack-1x fa-inverse"></i>
			</span> </a>';
			/*
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted(\''.$dataDB->kode.'\', \''.$dataDB->thn.'\', \''.$dataDB->tahun.'\')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>'; */
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tukin->count_filePerhitunganTukin(),
						"recordsFiltered" =>$this->tukin->count_filteredPerhitunganTukin(),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);

	}
	//PERHITUNGAN_TUKIN

	//PEMBAYARAN_TUKIN
	public function pembayaran_tukin()
	{
		$data['konten']="pembayaran_tukin";
		$data['satker']	= $this->setting->get_listsatker();
		$this->_template($data);
	}

	function loadTablesPembayaranTukin()
	{
			
		$list = $this->tukin->get_PembayaranTukin();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		$tunjangan = $dataDB->tunjangan;
		$potongan = $dataDB->potongan;
		$pajak = $dataDB->pajak;
		$jumlah_potongan = $tunjangan * $potongan/100;
		$jumlah_pajak = $tunjangan * $pajak/100;
		$jumlah = $tunjangan - $jumlah_potongan - $jumlah_pajak; 
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->no_rek."</a></span>";
			$row[] = "<span class='size'>".$dataDB->bank."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".number_format($tunjangan)."</span>";
			$row[] = "<span class='size'>".$potongan."</span>";
			$row[] = "<span class='size'>".$pajak."</span>";		
			$row[] = "<span class='size'>".number_format($jumlah)."</span>";	
			
			/*
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Tampilkan" onclick="show_renja(\''.$dataDB->kode.'\', \''.$dataDB->tahun.'\')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-list-alt fa-stack-1x fa-inverse"></i>
			</span> </a>';
			/*
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted(\''.$dataDB->kode.'\', \''.$dataDB->thn.'\', \''.$dataDB->tahun.'\')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>'; */
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tukin->count_filePembayaranTukin(),
						"recordsFiltered" =>$this->tukin->count_filteredPembayaranTukin(),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);
	}
	//PEMBAYARAN_TUKIN

	//PERHITUNGAN_PAJAK
	public function perhitungan_pajak()
	{
		$data['konten']="perhitungan_pajak";
		//$data['dataProfil']=$this->entry_kpo->dataProfile($this->session->userdata("id"));
		$this->_template($data);
	}

	function load_perhitungan_pajak()
	{
			
		$list = $this->tukin->get_perhitungan_pajak();
		$data = array();
		$no = $_POST['start'];
		$no =$no+1;
		foreach ($list as $dataDB) {
		////
			$row = array();
			$row[] = "<span class='size'>".$no++."</span>";
			$row[] = "<span class='size'>".$dataDB->kd_satker."</a></span>";
			$row[] = "<span class='size'>".$dataDB->nama_satker."</span>";
			$row[] = "<span class='size'>".$dataDB->nip."</span>";
			$row[] = "<span class='size'>".$dataDB->nama."</span>";
			$row[] = "<span class='size'>".$dataDB->kelas_jabatan."</span>";
			$row[] = "<span class='size'>".$dataDB->tunjangan."</span>";
			$row[] = "<span class='size'>".$dataDB->potongan."</span>";
			$row[] = "<span class='size'>".$dataDB->pajak."</span>";
					
			//add html for action
			$row[] = '
			
			<a class="table-link" href="javascript:void()" title="Edit" onclick="edit('.$dataDB->kd_satker.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
			</span> </a>
			|
			<a class="table-link danger" href="javascript:void()" title="Hapus" onclick="deleted('.$dataDB->kd_satker.')">
			<span class="fa-stack"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
			</span> </a>';		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tukin->count_perhitungan_pajak("t_tukin"),
						"recordsFiltered" =>$this->tukin->count_filtered_perhitungan_pajak('t_tukin'),
						"data" => $data,
						);
		//output to json format
		echo json_encode($output);
	}
	//PERHITUNGAN_PAJAK

}

