<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tukin extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}


    function add_satker()
	{
		$data=array(
		"kd_satker"=>$this->input->post("kd_satker"),
		"nama_satker"=>$this->input->post("nama_satker"),
	);
		return $this->db->insert("m_satker",$data);
	}

	//ENTRI_TUKIN
	function get_data_tukin() {
		$query=$this->_get_datatables_data_tukin();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}

	private function _get_datatables_data_tukin()
	{
		$query="SELECT * FROM m_karyawan AS mk
				LEFT JOIN m_satker AS ms ON mk.`kd_satker` = ms.`kd_satker` WHERE 1=1 ";
	
		if($this->uri->segment(3))
		{
			$id=$this->uri->segment(3);
			// $query.="AND a.level='".$id."' ";
		}
		
		if($_POST['columns'][6]['search']['value'] != ''){
            
                $searchkey6 = $_POST['columns'][6]['search']['value'];
         
                
                if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" AND ( mk.`kd_satker` = '".trim($searchkey6)."' ) ";
                else if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" WHERE ( mk.`kd_satker` = '".trim($searchkey6)."' ) ";
        }
		
		$column = array('','nip','nama','jabatan','kelas_jabatan','nama_satker','nip');
		$i=0;
		foreach ($column as $item) {
			$column[$i] = $item;
		}
		
		if(isset($_POST['order'])) {
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order)) {
			$order = $order;
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		//echo $query;
		return $query;
	
	}

	public function count_file() {
		$this->db->from("m_karyawan");
		return $this->db->count_all_results();
	}
	
	function count_filtered() {
		$query=$this->_get_datatables_data_tukin();
		return $this->db->query($query)->num_rows();
	}

	function get_DataTukin($id) //id_file
	{
		$this->db->where("nip",$id);
		return $this->db->get("m_karyawan")->row();
	}
	function getTunjangan($id)
	{
		$this->db->where("kelas_jabatan",$id);
		$data=$this->db->get("m_kelas")->row();
		return isset($data->tunjangan)?($data->tunjangan):"";
	}
	function getPajak($id)
	{
		$this->db->where("kelas_jabatan",$id);
		$data=$this->db->get("m_kelas")->row();
		return isset($data->pajak)?($data->pajak):"";
	}
	function add_tukin()
	{

		$data=array(
		"kd_satker"=>$this->input->post("kd_satker"),
		"tgl"=>$this->input->post("tgl"),
		"nip"=>$this->input->post("nip"),
		"nama"=>$this->input->post("nama"),
		"jabatan"=>$this->input->post("jabatan"),
		"kelas_jabatan"=>$idkelas=$this->input->post("kelas_jabatan"),
		"potongan"=>$this->input->post("potongan"),
		"tunjangan"=>$this->getTunjangan($idkelas),
		"pajak"=>$this->getPajak($idkelas),

	);
		return $this->db->insert("t_tukin",$data);
	}

	public function update_tukin($id)
	{
		$data=array(
		"kd_satker"=>$this->input->post("kd_satker"),
		"tgl"=>$this->input->post("tgl"),
		"nip"=>$this->input->post("nip"),
		"nama"=>$this->input->post("nama"),
		"jabatan"=>$this->input->post("jabatan"),
		"kelas_jabatan"=>$this->input->post("kelas_jabatan"),
		"potongan"=>$this->input->post("potongan"),
	);
		return $this->db->insert("t_tukin",$data);
	}
	//ENTRI_TUKIN


	//PERHITUNGAN_TUKIN
	function get_PerhitunganTukin() {
		$query=$this->_get_datatables_PehitunganTukin();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}

	private function _get_datatables_PehitunganTukin()
	{
		$query="SELECT * FROM t_tukin AS tk LEFT JOIN m_satker AS ms ON tk.`kd_satker` = ms.`kd_satker`";
	
		if($this->uri->segment(3))
		{
			$id=$this->uri->segment(3);
			// $query.="AND a.level='".$id."' ";
		}

		if($_POST['columns'][3]['search']['value'] != '' || $_POST['columns'][6]['search']['value'] != ''){
                $searchkey3 = $_POST['columns'][3]['search']['value'];
                $searchkey6 = $_POST['columns'][6]['search']['value'];
                
                if($_POST['columns'][3]['search']['value'] != '')
                    $query.=" WHERE ( tgl LIKE '%".trim($searchkey3)."%' ) ";
                
                if($_POST['columns'][3]['search']['value'] != '' && $_POST['columns'][6]['search']['value'] != '')
                    $query.=" AND ( ms.`kd_satker` = '".trim($searchkey6)."' ) ";
                else if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" WHERE ( ms.`kd_satker` = '".trim($searchkey6)."' ) ";
        }
		
		
		
		$column = array('','nama_satker','tgl','nip','nama','jabatan','kelas_jabatan','tunjangan','potongan','nip');
		$i=0;
		foreach ($column as $item) {
			$column[$i] = $item;
		}
		
		if(isset($_POST['order'])) {
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order)) {
			$order = $order;
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		//echo $query;
		return $query;
	
	}

	public function count_filePerhitunganTukin() {
		$this->db->from("t_tukin");
		return $this->db->count_all_results();
	}
	
	function count_filteredPerhitunganTukin() {
		$query=$this->_get_datatables_PehitunganTukin();
		return $this->db->query($query)->num_rows();
	}
	//PERHITUNGAN_TUKIN

	//PEMBAYARAN_TUKIN
	function get_PembayaranTukin() {
		$query=$this->_get_datatables_PembayaranTukin();
		if($_POST['length'] != -1)
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		
		return $this->db->query($query)->result();
	}

	private function _get_datatables_PembayaranTukin()
	{
		$query="SELECT a.tgl, a.nip, a.nama, a.kelas_jabatan, a.tunjangan, a.potongan, a.pajak, b.kd_satker, b.nama_satker, c.no_rek, c.bank FROM t_tukin a
				LEFT JOIN m_satker b ON a.kd_satker = b.kd_satker
				LEFT JOIN m_rekening c ON a.nip = c.nip";
	
		if($this->uri->segment(3))
		{
			$id=$this->uri->segment(3);
			// $query.="AND a.level='".$id."' ";
		}

		if($_POST['columns'][3]['search']['value'] != '' || $_POST['columns'][6]['search']['value'] != ''){
                $searchkey3 = $_POST['columns'][3]['search']['value'];
                $searchkey6 = $_POST['columns'][6]['search']['value'];
                
                if($_POST['columns'][3]['search']['value'] != '')
                    $query.=" WHERE ( tgl LIKE '%".trim($searchkey3)."%' ) ";
                
                if($_POST['columns'][3]['search']['value'] != '' && $_POST['columns'][6]['search']['value'] != '')
                    $query.=" AND ( a.`kd_satker` = '".trim($searchkey6)."' ) ";
                else if($_POST['columns'][6]['search']['value'] != '')
                    $query.=" WHERE ( a.`kd_satker` = '".trim($searchkey6)."' ) ";
        }
		
		
		
		$column = array('','no_rek','bank','nip','nama','kelas_jabatan','tunjangan','potongan','pajak');
		$i=0;
		foreach ($column as $item) {
			$column[$i] = $item;
		}
		
		if(isset($_POST['order'])) {
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order)) {
			$order = $order;
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		//echo $query;
		return $query;
	
	}

	public function count_filePembayaranTukin() {
		$this->db->from("t_tukin");
		return $this->db->count_all_results();
	}
	
	function count_filteredPembayaranTukin() {
		$query=$this->_get_datatables_PembayaranTukin();
		return $this->db->query($query)->num_rows();
	}
	//PEMBAYARAN_TUKIN

	//PERHITUNGAN_PAJAK
	function get_perhitungan_pajak()
	{
		
		$query=$this->_get_datatables_perhitungan_pajak();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_perhitungan_pajak()
	{
	$query="SELECT * FROM t_tukin where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND kd_satker='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			nama_satker LIKE '%".$searchkey."%' or
			kd_satker LIKE '%".$searchkey."%'  
			) ";
		}

		$column = array('','nama_satker','kd_satker');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_perhitungan_pajak($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_perhitungan_pajak($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_perhitungan_pajak();
		return $this->db->query($query)->num_rows();
	}
	//PERHITUNGAN_PAJAK


}

?>