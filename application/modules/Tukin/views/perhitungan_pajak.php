 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Proses Tukin</a></li>
      <li class="active"><span>Perhitungan Pajak</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
	<div class="col-lg-12">
		<div class="main-box clearfix" style="min-height: 820px;">
		    <br>
		    <div class="main-box-body clearfix">
		    	<div class="panel-group accordion" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
							Perhitungan Pajak
							</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<form class="form-horizontal" role="form" id="form" action="<?php echo base_url();?>tukin/" method="post">
									<div class="form-group">
										<label class="sr-only">Nama Satker</label>
										<div class="col-lg-4">
								      	<select class="form-control" name="bid_bapeda" id="bid_bapeda">
									        <option value=""> -- Pilih Nama Satker -- </option>
									        <option value="1">A</option>
									        <option value="2">B</option>
									        <option value="3">C</option>
									        <option value="4">D</option>
								      	</select>
								    	</div>
								    	<button type="submit" class="btn btn-primary">Proses</button>
										<button type="submit" class="btn btn-primary">Simpan</button>
									</div>									
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

      		<div class="main-box-body clearfix ">
				<table id="table" class="table table-striped table-hover">
					<thead>			
						<tr>
							<th class='thead' axis="string" width='15px'>No</th>
							<th class='thead' axis="date">NIP</th>
							<th class='thead' axis="date">Nama Pegawai</th>
							<th class='thead' axis="date">Jabatan</th>
							<th class='thead' axis="date">Kelas Jabatan</th>
							<th class='thead' axis="date">Tunjangan Kinerja</th>
							<th class='thead' axis="date">Potongan</th>
							<th class='thead' axis="date">Pajak</th>
							<th class='thead' axis="date">Jumlah</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>			

<script type="text/javascript">
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('tukin/load_perhitungan_pajak/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }
 
 	function simpan()
	{
	  var id=$('[name="kd_satker"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("setting/add_satker"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("setting/update_satker/"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};
 </script>

 <script type="text/javascript">
 	var save_method="";
	function add()
	  {
	  save_method="add";
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Satker</b>'); // Set Title to Bootstrap modal title
	  }
 </script>

 <script type="text/javascript">
	$('#datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });
</script>
	
	

	