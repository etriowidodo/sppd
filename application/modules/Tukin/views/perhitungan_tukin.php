  <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Proses Tukin</a></li>
      <li class="active"><span>Perhitungan Tukin</span></li>
    </ol>
    </div>
  </div>
  <br>
<div class="row">
  <div class="col-lg-12">
    <div class="main-box clearfix ">
      <header class="main-box-header clearfix">
        <h2 class="sadow05 black b">Perhitungan Tukin</h2>
      </header>
      <div class="main-box-body clearfix ">
      	<div class="form-group">
          <label for="input6" class="col-lg-2 control-label black">Bulan</label>
          <div class="col-lg-3">
              <input class="form-control" type="text" name="tgl" id="datepicker" />
            </fieldset>
          </div>
        </div>
        <br/>
        <div class="form-group">
          <label for="input6" class="col-lg-2 control-label black">Nama Satker</label>
          <div class="col-lg-3">
              <select class="form-control" name="kd_satker" id="kd_satker">
                <option value=""> -- Pilih Satker -- </option>
					<?php foreach ($satker as $row): ?>
					<option value="<?=$row['kd_satker']?>"><?=$row['nama_satker']?></option>
					<?php endforeach; ?>
              </select>
          </div>
        </div>
        <br><br>
        <table id='table' class="table table-striped table-hover">
          <thead>     
            <tr>
              	<th class='thead' axis="date">No</th>
        		<th class='thead' axis="date">Satker</th>
        		<th class='thead' axis="date">Bulan</th>
        		<th class='thead' axis="date">NIP</th>
        		<th class='thead' axis="date">Nama</th>
        		<th class='thead' axis="date">Jabatan</th>
        		<th class='thead' axis="date">Kelas Jabatan</th>
        		<th class='thead' axis="date">Tunjangan</th>
        		<th class='thead' axis="date">Potongan (%)</th>
        		<th class='thead' axis="date">Jumlah</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link rel="stylesheet" href="<?php echo base_url();?>/plug/offline/bootstrap-datepicker3.min.css">
<script type='text/javascript' src="<?php echo base_url();?>/plug/offline/bootstrap-datepicker.min.js"></script>
<link href="<?php echo base_url();?>/plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>     


<style>
.table_processing { display:none;}
.top{
  float:right;
}
a.dt-button.red {
  color:#fff;background-color:#337ab7;border-color:#2e6da4;
}
</style>
<script type="text/javascript">
  var save_method; //for save method string
  $(document).ready(function() {  
    var oTable = $('#table').DataTable({ 
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "bSortable": true,
      "bFilter":true,
      "sDom": '<"top"B>rt<"bottom"ilp><"clear">',
      // Load data for the table's content from an Ajax source
      "ajax": {
        "url": "<?php echo site_url('tukin/loadTablesPerhitunganTukin/'.$this->uri->segment(3).'')?>",
        "type": "POST"
      },
      //Set column definition initialisation properties.
      "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        
      ],
 
   
    });
    $('[name="tgl"]').on('change',function(){
      oTable.column(3).search(this.value).draw();
    });
    
    $('[name="kd_satker"]').on('change',function(){
      oTable.column(6).search(this.value).draw();
    });
  });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

  </script>   
  
  <script type="text/javascript">
  $('#datepicker').datepicker({
    format: 'yyyy-mm-dd'
  });
  </script>