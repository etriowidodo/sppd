<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends ci_Model
{
	
	public function __construct() {
        parent::__construct();
		//$this->m_konfig->validasi_session("super");
     	}

    //SATKER
    function get_listsatker() //id_file
	{
		return $this->db->get("m_satker")->result_array();
	}

	function get_listkelas() //id_file
	{
		return $this->db->get("m_kelas")->result_array();
	}

    function get_satker()
	{
		
		$query=$this->_get_datatables_satker();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_satker()
	{
	$query="SELECT * FROM m_satker where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND id='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			nama_satker LIKE '%".$searchkey."%' or
			kd_satker LIKE '%".$searchkey."%' or 
			id LIKE '%".$searchkey."%' 
			) ";
		}

		$column = array('','nama_satker','kd_satker','id');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_satker($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}
	function count_filtered_satker($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_satker();
		return $this->db->query($query)->num_rows();
	}

    function add_satker()
	{
		$data=array(
		"kd_satker"=>$this->input->post("kd_satker"),
		"nama_satker"=>$this->input->post("nama_satker"),
	);
		return $this->db->insert("m_satker",$data);
	}

	function get_data_satker($id) //id_file
	{
		$this->db->where("id",$id);
		return $this->db->get("m_satker")->row();
	}

	public function update_satker($id)
	{
		$data=array(
		"kd_satker"=>$this->input->post("kd_satker"),
		"nama_satker"=>$this->input->post("nama_satker"),
	);
		$this->db->where("id",$id);
		return $this->db->update("m_satker",$data);
	}

	public function delete_satker($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->delete("m_satker");
	}
	//SATKER

	//KELAS_JABATAN
	function get_kelas_jabatan()
	{
		
		$query=$this->_get_datatables_kelas_jabatan();
		if($_POST['length'] != -1)
		//$this->db->limit($_POST['length'], $_POST['start']);
		$query.=" limit ".$_POST['start'].",".$_POST['length'];
		//if($keyword=$this->uri->segment(3)){ $this->db->like('TextDecoded',$keyword);};
		
		//$query = $this->db->get();
		return $this->db->query($query)->result();
	}

	private function _get_datatables_kelas_jabatan()
	{
	$query="SELECT * FROM m_kelas where 1=1";
	
	if($this->uri->segment(3))
		{
		$id=$this->uri->segment(3);
		$query.="AND kd_kelas='".$id."' ";
		}
	
		if($_POST['search']['value']){
		$searchkey=$_POST['search']['value'];
			$query.=" AND (
			kd_kelas LIKE '%".$searchkey."%' or
			kelas_jabatan LIKE '%".$searchkey."%' or
			tunjangan LIKE '%".$searchkey."%' or
			pajak LIKE '%".$searchkey."%'
			) ";
		}

		$column = array('','kd_kelas','kelas_jabatan','tunjangan','pajak','kd_kelas');
		$i=0;
		foreach ($column as $item) 
		{
		$column[$i] = $item;
		}
		
		if(isset($_POST['order']))
		{
		//	$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
			$query.=" order by ".$column[$_POST['order']['0']['column']]." ".$_POST['order']['0']['dir'] ;
		} 
		else if(isset($order))
		{
			$order = $order;
		//	$this->db->order_by(key($order), $order[key($order)]);
			$query.=" order by ".key($order)." ".$order[key($order)] ;
		}
		return $query;
	
	}

    public function count_kelas_jabatan($tabel)
	{		
		$this->db->from($tabel);
		return $this->db->count_all_results();
	}

	function count_filtered_kelas_jabatan($tabel)
	{
		$this->db->from($tabel);
		$query=$this->_get_datatables_kelas_jabatan();
		return $this->db->query($query)->num_rows();
	}

	function add_kelas_jabatan()
	{
		$tunjangan 	= preg_replace("/\D/","",$this->input->post("tunjangan"));
		$data=array(
		"kd_kelas"=>$this->input->post("kd_kelas"),
		"kelas_jabatan"=>$this->input->post("kelas_jabatan"),
		"tunjangan"=> ($tunjangan > 0 ? $tunjangan : 0),
		"pajak"=>$this->input->post("pajak"),

	);
		return $this->db->insert("m_kelas",$data);
	}

	function get_data_kelas_jabatan($id) //id_file
	{
		$this->db->from('m_kelas');
		$this->db->where('id',$id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_kelas_jabatan($id)
	{
		$tunjangan 	= preg_replace("/\D/","",$this->input->post("tunjangan"));
		$data=array(
		"kd_kelas"=>$this->input->post("kd_kelas"),
		"kelas_jabatan"=>$this->input->post("kelas_jabatan"),
		"tunjangan"=> ($tunjangan > 0 ? $tunjangan : 0),
		"pajak"=>$this->input->post("pajak"),
	);
		$this->db->where("id",$id);
		return $this->db->update("m_kelas",$data);
	}

	public function delete_kelas_jabatan($id)
	{
		$this->db->where("id",$id);
		$data=$this->db->delete("m_kelas");
	}
}

?>