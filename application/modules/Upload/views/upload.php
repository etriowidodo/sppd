

 <div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
      <li><a href="#">Master</a></li>
      <li class="active"><span>Synconisasi</span></li>
    </ol>
    </div>
  </div>
  <br>

<div class="row ">
		<div class="main-box clearfix ">
			<header class="main-box-header clearfix">
				<h2 class="sadow05  ">Upload Data</h2>
				<h5 class="sadow05 " id="container-date" style="display: none">Bulan <span id='bulan'></span> Tahun <span id='tahun'></span> </h5>
			</header>
			<div class="main-box-body clearfix ">
			<form enctype="multipart/form-data">
        <input id="file-0a" class="file" type="file" multiple data-min-file-count="1">
        <br>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Reset</button>
    </form>
	
			</div>
		</div>
</div>

<!-- Bootstrap modal -->

<!--<link href="http://localhost/ajax_crud_datatables/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
<link href="<?php echo base_url();?>plug/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>plug/css/fileinput.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>plug/css/theme.css" rel="stylesheet">
<script src="<?php echo base_url()?>plug/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url()?>plug/datatables/js/dataTables.bootstrap.js"></script>	
<script src="<?php echo base_url()?>plug/js/fileinput.js"></script>		
<script src="<?php echo base_url()?>plug/js/theme.js"></script>  
<script type="text/javascript">
   	var save_method; //for save method string
    var table;
    $(document).ready(function() {
      table = $('#table').DataTable({ 
        
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('setting/load_kelas_jabatan/'.$this->uri->segment(3).'')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ -1 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    var save_method;
 
 	function simpan()
	{
	  var id=$('[name="id"]').val();
	  if(save_method=="add")
	  {
	      var link='<?php echo base_url("setting/add_kelas_jabatan"); ?>'; 
	  }
	    else
	  {
	      var link='<?php echo base_url("setting/update_kelas_jabatan/"); ?>'; 
	  }
	  
	  
	    $('#form').ajaxForm({
	    url:link,
	    data: $('#form').serialize(),
	    dataType: "JSON",
	    success: function(data)
	    {
	      //if success close modal and reload ajax table
	      $('#modal_form').modal('hide');
	      reload_table();
	    },
	      error: function (jqXHR, textStatus, errorThrown)
	    {
	      alert('Error adding / update data');
	    } 
	    });     
	};

	function edit(id)
    {
      save_method = 'update';
      $('#form')[0].reset(); // reset form on modals
    //Ajax Load data from ajax
      $.ajax({
        url : "<?php echo site_url('setting/edit_kelas_jabatan/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        { 
            var tunjangan = data.tunjangan > 0 ? numberWithCommas(data.tunjangan) : "";
            $('[name="id"]').val(data.id); 
            $('[name="kd_kelas"]').val(data.kd_kelas);
            $('[name="kelas_jabatan"]').val(data.kelas_jabatan);
            $('[name="tunjangan"]').val(tunjangan);
            $('[name="pajak"]').val(data.pajak);
                    
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').html('<b>Edit Kelas Jabatan</b>'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function deleted(id)
    {
      if(confirm('Are you sure delete this data?'))
      {
        // ajax delete data to database
          $.ajax({
            url:"<?php echo base_url();?>setting/delete_kelas_jabatan/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               $('#modal_form').modal('hide');
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
         
      }
    }
 </script>

 <script type="text/javascript">

 $(document).ready(function(){
 	$('.click').on('click',function(){
 		$('#loading').show();
 	});

 })
 	var save_method="";
	function add()
	  {
	  save_method="add";
	   $('#form')[0].reset(); // reset form on modal
	  $('#modal_form').modal('show'); 
	  $('.modal-title').html('<b>Entry Kelas Jabatan</b>'); // Set Title to Bootstrap modal title
	  }
 </script>

 <script type="text/javascript">
	$('#datepicker').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('input#number').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40) return;

		  // format number
		  $(this).val(function(index, value) {
			return value
			.replace(/\D/g, "")
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			;
		  });
		});
</script>

<script type="text/javascript">
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  var body = $(".sync");

  $(document).on({

        ajaxStart: function() { body.addClass("loading");    },
        ajaxSuccess: function() { body.removeClass("loading"); },
        submit  : function(){ body.addClass("loading");}

});
</script>
	

	