/*
SQLyog Community v12.4.1 (64 bit)
MySQL - 5.6.21 : Database - tunjangan_kinerja
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tunjangan_kinerja` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tunjangan_kinerja`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(35) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `poto` varchar(30) DEFAULT NULL,
  `owner` varchar(30) DEFAULT NULL,
  `level` int(11) DEFAULT '3',
  `telp` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `price` double DEFAULT '2000',
  `saldo` double DEFAULT '0',
  `tgl` datetime DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `mode` int(11) DEFAULT '1',
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`username`,`password`,`poto`,`owner`,`level`,`telp`,`alamat`,`email`,`id_parent`,`price`,`saldo`,`tgl`,`ip`,`mode`) values 
(2,'root','63a9f0ea7bb98050796b649e85481845','2.jpg','Super Admin',1,'0',NULL,'b7@gmail.com',NULL,2000,0,NULL,NULL,1),
(33,'admin','21232f297a57a5a743894a0e4a801fc3','33.jpg','Administrator',11,'0',NULL,'admin@gmail.com',NULL,2000,0,'2017-04-25 19:37:14',NULL,1);

/*Table structure for table `m_karyawan` */

DROP TABLE IF EXISTS `m_karyawan`;

CREATE TABLE `m_karyawan` (
  `nip` varchar(20) NOT NULL,
  `kd_satker` int(5) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `kelas_jabatan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `m_karyawan` */

insert  into `m_karyawan`(`nip`,`kd_satker`,`nama`,`jabatan`,`kelas_jabatan`) values 
('198608182007122001',1,'Lesly Lumalessil, A.Md','Pranata Hubungan Masyarakat Pelaksana','6'),
('198608182007122002',2,'Andi','Pranata Hubungan Masyarakat Pelaksana','5'),
('198608182007122003',3,'Adi','Pranata Hubungan Masyarakat Pelaksana','3'),
('198608182007122004',2,'Yulia','Pranata Hubungan Masyarakat Pelaksana','5'),
('198608182007122005',2,'Iman','Pranata Hubungan Masyarakat Pelaksana','3'),
('198608182007122006',3,'Opik','Pranata Hubungan Masyarakat Pelaksana','4');

/*Table structure for table `m_kelas` */

DROP TABLE IF EXISTS `m_kelas`;

CREATE TABLE `m_kelas` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kd_kelas` varchar(5) DEFAULT NULL,
  `kelas_jabatan` varchar(20) DEFAULT NULL,
  `tunjangan` bigint(20) DEFAULT NULL,
  `pajak` bigint(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `m_kelas` */

insert  into `m_kelas`(`id`,`kd_kelas`,`kelas_jabatan`,`tunjangan`,`pajak`) values 
(16,'1','3',1500000,10),
(17,'2','3',1500000,10),
(18,'3','4',2000000,20),
(19,'4','5',2500000,25),
(20,'5','6',3000000,30);

/*Table structure for table `m_rekening` */

DROP TABLE IF EXISTS `m_rekening`;

CREATE TABLE `m_rekening` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `kd_satker` int(5) DEFAULT NULL,
  `no_rek` varchar(20) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `m_rekening` */

insert  into `m_rekening`(`id`,`nip`,`nama`,`kd_satker`,`no_rek`,`bank`) values 
(1,'198608182007122001','Lesly Lumalessil, A.Md',1,'71665522111','Mandiri Syariah'),
(2,'198608182007122002','Andi',2,'2131133','BNI'),
(3,'198608182007122003','Adi',3,'44232','BCA'),
(4,'198608182007122004','Yulia',2,'09811222','BJB'),
(5,'198608182007122005','Iman',2,'761522111','Mandiri');

/*Table structure for table `m_satker` */

DROP TABLE IF EXISTS `m_satker`;

CREATE TABLE `m_satker` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kd_satker` varchar(5) DEFAULT NULL,
  `nama_satker` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `m_satker` */

insert  into `m_satker`(`id`,`kd_satker`,`nama_satker`) values 
(1,'1','Kantor SAR Kelas A Ambon'),
(2,'2','Kantor SAR Kelas B Ambon'),
(3,'3','Kantor SAR Kelas C Ambon'),
(4,'4','Kantor SAR Kelas D Ambon');

/*Table structure for table `main_konfig` */

DROP TABLE IF EXISTS `main_konfig`;

CREATE TABLE `main_konfig` (
  `id_konfig` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id_konfig`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `main_konfig` */

insert  into `main_konfig`(`id_konfig`,`nama`,`value`) values 
(1,'Loggo',''),
(2,'nama aplikasi','Tunjangan Kinerja'),
(3,'Tanggal Project',''),
(4,'Klien','-'),
(5,'Product By','dr.pc'),
(6,'Jenis Login','1'),
(8,'footer','Tunjangan Kinerja'),
(7,'record log','1000');

/*Table structure for table `main_level` */

DROP TABLE IF EXISTS `main_level`;

CREATE TABLE `main_level` (
  `id_level` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL,
  `direct` text,
  `ket` text,
  PRIMARY KEY (`id_level`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `main_level` */

insert  into `main_level`(`id_level`,`nama`,`direct`,`ket`) values 
(1,'root','root','Untuk Super Admin'),
(11,'admin','dashboard','Untuk Administrator');

/*Table structure for table `main_log` */

DROP TABLE IF EXISTS `main_log`;

CREATE TABLE `main_log` (
  `id_log` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) DEFAULT NULL,
  `nama_user` varchar(56) DEFAULT NULL,
  `table_updated` varchar(25) DEFAULT NULL,
  `aksi` text,
  `tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `main_log` */

insert  into `main_log`(`id_log`,`id_admin`,`nama_user`,`table_updated`,`aksi`,`tgl`) values 
(1,2,'Bagus AA','admin','Upload photo','2017-04-25 19:37:14'),
(2,2,'Super Admin','admin','update data profile','2017-05-05 11:58:23');

/*Table structure for table `main_menu` */

DROP TABLE IF EXISTS `main_menu`;

CREATE TABLE `main_menu` (
  `id_menu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  `level` enum('1','2') DEFAULT NULL,
  `id_main` int(11) DEFAULT '0',
  `icon` varchar(25) DEFAULT NULL,
  `hak_akses` int(11) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `main_menu` */

insert  into `main_menu`(`id_menu`,`nama`,`level`,`id_main`,`icon`,`hak_akses`,`link`) values 
(1,'Konfigurasi','1',0,'fa fa-cog',1,'root/konfig'),
(2,'User Group','1',0,'fa fa-users',1,'root/manajemen'),
(3,'Data User','1',0,'fa fa-user',1,'root/data_user'),
(5,'Data Rekening','2',4,'',11,'master/data_rekening'),
(4,'Master','1',1,'fa fa-book',11,''),
(6,'Data Karyawan','2',4,'',11,'master/data_karyawan'),
(7,'Sinkronisasi','2',4,'',11,'#'),
(8,'Data Pajak','2',4,'',11,''),
(9,'Setting','1',1,'fa fa-cogs',11,''),
(10,'Pejabat Penandatangan','2',9,'',11,''),
(11,'Satker','2',9,'',11,'setting/view_satker'),
(12,'Kelas Jabatan','2',9,'',11,'setting/view_kelas_jabatan'),
(13,'Proses Tukin','1',1,'fa fa-circle-o-notch',11,''),
(14,'Entry Tukin','2',13,'',11,'tukin/entry_tukin'),
(24,'Rekap Tukin Per Kelas','2',20,'',11,'laporan/rekap_tukin_perkelas'),
(16,'Perhitungan Tukin','2',13,'',11,'tukin/perhitungan_tukin'),
(17,'Pembayaran Tukin','2',13,'',11,'tukin/pembayaran_tukin'),
(18,'Koreksi Tukin','2',13,'',11,''),
(19,'Pencetakan Tukin','2',13,'',11,''),
(20,'Laporan','1',1,'fa fa-file',11,''),
(21,'Tukin Per Satker','2',20,'',11,'laporan/lap_tukin_persatker'),
(22,'Tukin Per Kelas Jabatan','2',20,'',11,'laporan/lap_tukin_perkelas'),
(23,'Rekap Tukin Per Satker','2',20,'',11,'laporan/rekap_tukin_persatker');

/*Table structure for table `t_tukin` */

DROP TABLE IF EXISTS `t_tukin`;

CREATE TABLE `t_tukin` (
  `kd_satker` int(5) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `jabatan` varchar(50) DEFAULT NULL,
  `kelas_jabatan` varchar(20) DEFAULT NULL,
  `tunjangan` bigint(20) DEFAULT '0',
  `potongan` bigint(20) DEFAULT '0',
  `pajak` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `t_tukin` */

insert  into `t_tukin`(`kd_satker`,`tgl`,`nip`,`nama`,`jabatan`,`kelas_jabatan`,`tunjangan`,`potongan`,`pajak`) values 
(1,'2017-05-02','198608182007122001','Lesly Lumalessil, A.Md','Pranata Hubungan Masyarakat Pelaksana','6',3000000,30,30),
(2,'2017-05-01','198608182007122002','Andi','Pranata Hubungan Masyarakat Pelaksana','5',2500000,20,25),
(2,'2017-05-17','198608182007122005','Iman','Pranata Hubungan Masyarakat Pelaksana','3',1500000,15,10),
(2,'2017-05-24','198608182007122004','Yulia','Pranata Hubungan Masyarakat Pelaksana','5',2500000,25,25);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
