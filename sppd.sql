/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.7.18-log : Database - sppd
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sppd` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sppd`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(35) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `poto` varchar(30) DEFAULT NULL,
  `owner` varchar(30) DEFAULT NULL,
  `level` int(11) DEFAULT '3',
  `telp` varchar(50) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `price` double DEFAULT '2000',
  `saldo` double DEFAULT '0',
  `tgl` datetime DEFAULT NULL,
  `ip` varchar(25) DEFAULT NULL,
  `mode` int(11) DEFAULT '1',
  PRIMARY KEY (`id_admin`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id_admin`,`username`,`password`,`poto`,`owner`,`level`,`telp`,`alamat`,`email`,`id_parent`,`price`,`saldo`,`tgl`,`ip`,`mode`) values (2,'root','63a9f0ea7bb98050796b649e85481845','2.jpg','Bagus AA',1,'082319550775',NULL,'bagusaliakbar77@gmail.com',NULL,2000,0,NULL,NULL,1),(33,'admin','21232f297a57a5a743894a0e4a801fc3','33.jpg','Administrator',11,'0',NULL,'admin@gmail.com',NULL,2000,0,'2017-04-25 19:37:14',NULL,1);

/*Table structure for table `m_jenis` */

DROP TABLE IF EXISTS `m_jenis`;

CREATE TABLE `m_jenis` (
  `kode` char(2) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `rekening` int(8) DEFAULT NULL,
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `m_jenis` */

insert  into `m_jenis`(`kode`,`nama`,`rekening`,`id_jenis`) values ('01','Perjalanan Dinas Dalam Daerah Kab. Raja Ampat',5221501,1),('02','Perjalanan Dinas Dalam Daerah Wilayah Prop. Papua Barat',5221501,2),('03','Perjalanan Dinas Luar Daerah',5221502,3),('05','Perjalanan Dinas Luar Negeri1',5221503,4);

/*Table structure for table `m_jk` */

DROP TABLE IF EXISTS `m_jk`;

CREATE TABLE `m_jk` (
  `id_jk` int(11) NOT NULL AUTO_INCREMENT,
  `jk` char(1) DEFAULT NULL,
  PRIMARY KEY (`id_jk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `m_jk` */

insert  into `m_jk`(`id_jk`,`jk`) values (1,'L'),(2,'P');

/*Table structure for table `m_pegawai` */

DROP TABLE IF EXISTS `m_pegawai`;

CREATE TABLE `m_pegawai` (
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jk` tinyint(1) DEFAULT NULL,
  `tempatlahir` varchar(100) DEFAULT NULL,
  `lahir` date DEFAULT NULL,
  `satker` varchar(50) DEFAULT NULL,
  `gol` varchar(10) DEFAULT NULL,
  `tingkatan` varchar(2) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `bagian` varchar(50) DEFAULT NULL,
  `status_pns` tinyint(1) DEFAULT NULL,
  `pangkat` varchar(255) DEFAULT NULL,
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=432 DEFAULT CHARSET=utf8;

/*Data for the table `m_pegawai` */

insert  into `m_pegawai`(`nip`,`nama`,`jk`,`tempatlahir`,`lahir`,`satker`,`gol`,`tingkatan`,`jabatan`,`bagian`,`status_pns`,`pangkat`,`id_pegawai`) values ('198909252001021002','Etrio Widodo',NULL,'Jakarta','1989-09-25','1201200307','IV/a',NULL,'Sekertaris Daerah',NULL,1,'Pembina',3),('198805222009011002','Jakaria Yahya',NULL,'Bandung','1999-06-24','1201200307','IIIb',NULL,'Kepala Sub Bagian Keuangan',NULL,1,'Penata Muda Tk1',4),('0','Frans Krey',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,219),('00','Drs. Inda Arfan, M. Ec.Dev',NULL,NULL,NULL,'1201200202',NULL,NULL,NULL,NULL,NULL,NULL,220),('001','Jems Septian Dimara',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,221),('002','Susiana Ferdinad',NULL,NULL,NULL,'1201200305',NULL,NULL,NULL,NULL,NULL,NULL,222),('003','Roby Mampioper',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,223),('005','Ny. Yeni Taibe',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,224),('006','Yohan Abaire',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,225),('007','Daniel Yadera',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,226),('009','Drs. Marcus Wanma, M. Si',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,227),('010','Martha Hamadi',NULL,NULL,NULL,'1201200301',NULL,NULL,NULL,NULL,NULL,NULL,228),('010230317','Johan A.S. Mampioper',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,229),('010271294','Alfred Edison Suruan, S.STP',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,230),('011','Philipus Urbasa',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,231),('012','Yosep korwa',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,232),('013','Trayanus Rumsayor',NULL,NULL,NULL,'12019999',NULL,NULL,NULL,NULL,NULL,NULL,233),('014','Martinus Bransano',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,234),('015','Nonske Mayor',NULL,NULL,NULL,'1201200305',NULL,NULL,NULL,NULL,NULL,NULL,235),('016','Rona Imelda',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,236),('017','Pedi Rumbiak',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,237),('018','Kores Kelelufna',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,238),('019','Ny. Kloria Liunsandra',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,239),('020','H. Djalali, S.Sos. MH',NULL,NULL,NULL,'99999999',NULL,NULL,NULL,NULL,NULL,NULL,240),('021','Pujo Samedi S.Kom',NULL,NULL,NULL,'99999999',NULL,NULL,NULL,NULL,NULL,NULL,241),('022','H. Fattah Abdullah',NULL,NULL,NULL,'99999999',NULL,NULL,NULL,NULL,NULL,NULL,242),('023','Albertus S.C. Kandami',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,243),('024','Nella Mambraku',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,244),('025','Yanpiet Elday',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,245),('026','Martha Fransina Hamadi',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,246),('027','Sahdia Tukumokan',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,247),('028','Adam Inggamer',NULL,NULL,NULL,'1201200309',NULL,NULL,NULL,NULL,NULL,NULL,248),('029','Desire Sanadi',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,249),('03','Levi Rumbewas',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,250),('030','Ady Letusan',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,251),('031','Wenand Saleo',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,252),('032','Rikson Mayor',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,253),('033','Octovianus Mirino',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,254),('04','Mesak Rumbewas',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,255),('040030000','Ronny Sokoy',NULL,'','1970-01-01','12012003','',NULL,'',NULL,1,'',256),('05','Luis Yapen',NULL,'','1970-01-01','1201200304','',NULL,'',NULL,1,'',257),('050044005','Andrias Kabes',NULL,NULL,NULL,'12612601',NULL,NULL,NULL,NULL,NULL,NULL,258),('06','Romeo Omkarsba',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,259),('07','Frits Kaisepo',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,260),('08','Pdt. A. Tjoe, S.Th. M.Th',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,261),('10','Rudi Tenu',NULL,NULL,NULL,'11911902',NULL,NULL,NULL,NULL,NULL,NULL,262),('120117625','Johny Lampang, BA',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,263),('123','Ny. Saoda Arfan',NULL,NULL,NULL,'1201200202',NULL,NULL,NULL,NULL,NULL,NULL,264),('130764096','Alfaris Mambraku, SE, M.Ec.Dev',NULL,NULL,NULL,'11911903',NULL,NULL,NULL,NULL,NULL,NULL,265),('131226915','Ny. R. Sroyer',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,266),('140069693','Mintje Arfayan',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,267),('1912311994031003','H. Baba, SE MM',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,268),('195305071977101002','Drs. KARIM KADIR',NULL,NULL,NULL,'11911903',NULL,NULL,NULL,NULL,NULL,NULL,269),('195401061983031011','Marthen Umalan, SE. M.Ec.Dev',NULL,NULL,NULL,'10411301',NULL,NULL,NULL,NULL,NULL,NULL,270),('195412311986011002','Drs I Nyoman Jaya',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,271),('195412311986011022','Drs. I. Nyoman Jaya',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,272),('195509131983031001','Samuel Monim, BA',NULL,NULL,NULL,'11011001',NULL,NULL,NULL,NULL,NULL,NULL,273),('195604191979031009','Semuel Belseran, S.Sos, M.Ec.Dev',NULL,NULL,NULL,'1201200303',NULL,NULL,NULL,NULL,NULL,NULL,274),('195605111980011003','Nurdin U.Saka, S.Sos, M.Ec.Dev',NULL,NULL,NULL,'11911901',NULL,NULL,NULL,NULL,NULL,NULL,275),('195607171992012002','Ratna Bondahara, S.Sos',NULL,NULL,NULL,'11111101',NULL,NULL,NULL,NULL,NULL,NULL,276),('195705201989111001','Dr. Tommy Yong',NULL,NULL,NULL,'10210201',NULL,NULL,NULL,NULL,NULL,NULL,277),('195712121983031031','Drs. Ferdinand Dimara, M.Si',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,278),('195805131984031007','Melianus Lapong',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,279),('195809281980021003','Hanock Suruan, SE. M.Ec.Dev',NULL,NULL,NULL,'20720701',NULL,NULL,NULL,NULL,NULL,NULL,280),('195812051984031009','Pedro Sune, A.Md',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,281),('195812311985031371','Drs. Muh.Idrus, BE',NULL,NULL,NULL,'20320301',NULL,NULL,NULL,NULL,NULL,NULL,282),('195909061986031021','Saiful Hi Malik, SH',NULL,NULL,NULL,'12012007',NULL,NULL,NULL,NULL,NULL,NULL,283),('195910131982031006','Oktofianus Antaribaba, SE',NULL,NULL,NULL,'12012006',NULL,NULL,NULL,NULL,NULL,NULL,284),('196001191987031010','Drs. Yan Mambrasar',NULL,NULL,NULL,'12012005',NULL,NULL,NULL,NULL,NULL,NULL,285),('196004151981021003','Abdul Samad Wajo, S.Sos',NULL,NULL,NULL,'1201200302',NULL,NULL,NULL,NULL,NULL,NULL,286),('196004151981021005','Drs. Abdul Samad Wajo',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,287),('196009081986031015','Steven Numberi, S.Sos. M.Ec.Dev',NULL,NULL,NULL,'1201200309',NULL,NULL,NULL,NULL,NULL,NULL,288),('196102121985031001','Samgar Sosir, S.Sos, MH',NULL,NULL,NULL,'12012008',NULL,NULL,NULL,NULL,NULL,NULL,289),('196102231989031003','Ir. Johanes B. Rahawarin, MM',NULL,NULL,NULL,'10710701',NULL,NULL,NULL,NULL,NULL,NULL,290),('196110131987031008','Heri Suwasito',NULL,NULL,NULL,'12012007',NULL,NULL,NULL,NULL,NULL,NULL,291),('196207271985032001','Maria Ramar, S.Sos',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,292),('196207271985032016','Maria Ramar',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,293),('196211131198031102','Mika Mirano, SE',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,294),('196211131988031012','Mika Mirino',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,295),('196303191989031021','Ir. Abdul Rahman Wairoy, M.Ec.Dev',NULL,NULL,NULL,'20120101',NULL,NULL,NULL,NULL,NULL,NULL,296),('196305251996101001','Asahel Watem, S.Sos',NULL,NULL,NULL,'1201200308',NULL,NULL,NULL,NULL,NULL,NULL,297),('196307041995031001','Ir. Willem J. Watem',NULL,NULL,NULL,'20120102',NULL,NULL,NULL,NULL,NULL,NULL,298),('196408031985031014','Drs. Burhanuddin Thahir',NULL,NULL,NULL,'11911903',NULL,NULL,NULL,NULL,NULL,NULL,299),('196410171989031015','Drs. Mansyur Syahdan',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,300),('196411051994031011','Hanoch Hamadi',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,301),('196412311995031007','Yulius R. Kole',NULL,NULL,NULL,'12019999',NULL,NULL,NULL,NULL,NULL,NULL,302),('196508301988031009','H. Djajali, S.Sos',NULL,NULL,NULL,'11411401',NULL,NULL,NULL,NULL,NULL,NULL,303),('196510022005021003','Abraham Mambraku',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,304),('196511301996071001','Noak Komboy, SH',NULL,NULL,NULL,'12012004',NULL,NULL,NULL,NULL,NULL,NULL,305),('196511301996101001','Noack Komboy, SH',NULL,NULL,NULL,'12012004',NULL,NULL,NULL,NULL,NULL,NULL,306),('196603110995031003','Ir. Wahab Sangaji',NULL,NULL,NULL,'10810801',NULL,NULL,NULL,NULL,NULL,NULL,307),('196611211992031007','Drs. Saiful M. Sangadji, M.Ec.Dev',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,308),('196701171992011002','Drs. Yusuf Salim, M.Si',NULL,NULL,NULL,'1201200301',NULL,NULL,NULL,NULL,NULL,NULL,309),('196703022000121000','Zakeus Mirino, S.pd',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,310),('196705061996101003','Artemas Mambrisauw, S.Sos. M.Si',NULL,NULL,NULL,'10610601',NULL,NULL,NULL,NULL,NULL,NULL,311),('196708151990031003','Feliks Dimara, S.Pi',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,312),('196708271996101001','Yulianus Mambraku,SH',NULL,NULL,NULL,'11411401',NULL,NULL,NULL,NULL,NULL,NULL,313),('196711021995031001','Ambri Hamza',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,314),('196807131990031023','Jerry L. Wakman, S.IP',NULL,NULL,NULL,'1201200305',NULL,NULL,NULL,NULL,NULL,NULL,315),('196812311994031105','H. BABA, SE. MM',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,316),('197006081995031004','Yusdi N. Lamatengge, S.PI,M.Si',NULL,NULL,NULL,'20420401',NULL,NULL,NULL,NULL,NULL,NULL,317),('197009281990031002','Johan A.S. Mampioper',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,318),('197101142003121004','Yustina Yomaki',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,319),('197103292006051001','Mario Michael Manuputty',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,320),('197105042006051001','Malik Husen',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,321),('197107032002121006','Dr. Engelbert Wader',NULL,NULL,NULL,'10210201',NULL,NULL,NULL,NULL,NULL,NULL,322),('197110191997022001','Martha M. Sanadi',NULL,NULL,NULL,'10110101',NULL,NULL,NULL,NULL,NULL,NULL,323),('197203101995031001','Imanuel P. Urbinas, S.Pi, M.Si',NULL,NULL,NULL,'20520501',NULL,NULL,NULL,NULL,NULL,NULL,324),('197208092009041001','La Ode Darwin',NULL,NULL,NULL,'1201200308',NULL,NULL,NULL,NULL,NULL,NULL,325),('197208162006051003','Dadang Sudrajat',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,326),('197303022000122005','Marthina Solossa',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,327),('197305151994031008','Saymsuddin Samuel Nimanuho',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,328),('197306212005021006','Frans Mandobar',NULL,NULL,NULL,'1201200202',NULL,NULL,NULL,NULL,NULL,NULL,329),('197307162006052001','Sara Djamaludin',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,330),('197309162005021001','Petrus Rabu, S.FIL',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,331),('197311201998031009','Willem Mambrasar, S.Sos,MM',NULL,NULL,NULL,'11811801',NULL,NULL,NULL,NULL,NULL,NULL,332),('197402232002121005','Orideko I Burdam,S.IP.MM.M.Ec.Dev',NULL,NULL,NULL,'12012006',NULL,NULL,NULL,NULL,NULL,NULL,333),('197404161997121001','Renold Mluy',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,334),('19740615200111001','M. Samad Soltif',NULL,NULL,NULL,'20120101',NULL,NULL,NULL,NULL,NULL,NULL,335),('197411202003021005','Paul Mayor, ST',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,336),('19744161997121001','Reybold Mloey',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,337),('197501052002121010','Muh. Anthoni Nasution, ST,MT',NULL,NULL,NULL,'10310301',NULL,NULL,NULL,NULL,NULL,NULL,338),('197503152000122004','Fransiska Wanma, S.Hut, M.Ec.Dev',NULL,NULL,NULL,'12212201',NULL,NULL,NULL,NULL,NULL,NULL,339),('197504112006051001','Muhaimin Rumbewas',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,340),('197601052003122010','Herlina H. Mambrasar, SE',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,341),('197606232010041001','Yermias Burdam, S.Sos',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,342),('197606252010041001','Soleman Soor',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,343),('197607282010042001','Mersilisa Hiborang',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,344),('197609102006051001','Stanly FM Sauyai',NULL,NULL,NULL,'11811801',NULL,NULL,NULL,NULL,NULL,NULL,345),('197610012003121009','Willy Kolamsusu',NULL,NULL,NULL,'88888888',NULL,NULL,NULL,NULL,NULL,NULL,346),('197611042006051001','Sandino',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,347),('197612072005021006','Adi Sucipto, ST',NULL,NULL,NULL,'1201200309',NULL,NULL,NULL,NULL,NULL,NULL,348),('197702072011042002','Dominggas Mambrasar, S.Sos',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,349),('197706142011041001','Stenly Wanma',NULL,NULL,NULL,'1201200309',NULL,NULL,NULL,NULL,NULL,NULL,350),('197707172005021009','Yustinus Sokoy',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,351),('197710142006052001','Riena Y.Y. Nikijuluw, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,352),('197711222011042011','Titik Noviati Sudarna, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,353),('197804112003121007','Iskandar Hamid Urbinas',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,354),('197805082000121003','Frans Mamraku',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,355),('197805262006052003','Merry Lensy S.Burwos, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,356),('197805262009091001','Yane Umpes, S.Sos',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,357),('197806052005021006','Marinus Mambrasar',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,358),('197806212005021005','Mesak Urbasa',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,359),('197806282006051002','Ismat HI. Muhd Nur',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,360),('197807242003121007','Frans Kelasin, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,361),('197809062005021002','Daud Mambrasar, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,362),('197809072005021006','Yan Dominggus Ruatakurey',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,363),('197810102005022012','Yane Wanma',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,364),('197812062011042002','Inneke Tin L. Morin',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,365),('197812152002122009','Tabita Demuh',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,366),('197907042003121007','Fahri Arfan',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,367),('197907042003121009','Fahri Arfan',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,368),('197908012006051001','Ferry Syors Yappen',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,369),('197908302005061001','Frengky Mamoribo',NULL,NULL,NULL,'12012008',NULL,NULL,NULL,NULL,NULL,NULL,370),('197910202003121007','Mohliyat Mayalibit, SH',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,371),('198002022006051001','Bodale Wajo, SH',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,372),('198002222005022006','Nurhayati',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,373),('198004162005021006','Dicky Sosipater, SE',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,374),('198005182009041003','Nasarudin Rumakat',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,375),('198007162006051001','Hudger Nifu',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,376),('198008082011041004','Irianto Silalahi, SE',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,377),('198010232008122012','Sarifa Arfan, SH',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,378),('198104042010011035','Deky Mambraku',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,379),('198107292006051001','Jhon Bastian Mirino',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,380),('198111052006052001','Ruaida Dimara, SE',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,381),('198201032011041001','Ahas Maray, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,382),('198202122009041005','Latif Wailata',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,383),('19820510201104001','Meily N Unmehopa, S. IP',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,384),('198205102011042001','Meily Nelly Unmehopa, S.IP',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,385),('198206162010042001','Esterlina F.Iwanggin, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,386),('198207102006051001','Yulian Urbata',NULL,NULL,NULL,'1201200305',NULL,NULL,NULL,NULL,NULL,NULL,387),('19820727200909001','Abraham Yosep Rumbewas, SH',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,388),('198207272009091001','Abraham Yosep Rumbewas, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,389),('198209172007012010','Adolfina Mambrasar',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,390),('198210212009041005','Latif Wailata',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,391),('198210212009092001','Latif Wailata',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,392),('198212012009092001','Desire Sanadi',NULL,NULL,NULL,'1201200306',NULL,NULL,NULL,NULL,NULL,NULL,393),('198301162006051001','Reiner Mokay',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,394),('198303222005021003','Lahasani Sakaiganan',NULL,NULL,NULL,'1201200202',NULL,NULL,NULL,NULL,NULL,NULL,395),('198304092010042001','Ani Muliani Margaretha Abidin, A.Md',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,396),('198305072005021004','Amos Rumere',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,397),('198308172003122002','Evi Dey',NULL,NULL,NULL,'1201200307',NULL,NULL,NULL,NULL,NULL,NULL,398),('198309162009041006','Jonisius Rumbewas',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,399),('198310172009091001','Muhammad Ali Bahale, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,400),('198310232010041001','Rifail Jongel Umpes, S.IP',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,401),('198310302011042001','Lusiana Pakarrang, A.Md',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,402),('198312112011041001','Barce Zakeus Lapon, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,403),('198406032005021002','Polce Muradji',NULL,NULL,NULL,'12012006',NULL,NULL,NULL,NULL,NULL,NULL,404),('198408312009091001','La Anwar',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,405),('198409272009092001','Grace Tadia Sauyai, AMd',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,406),('198410242009092001','Feronika Imelda Matatar',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,407),('198501101200412002','Nyoman Sari Buana, S.STP',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,408),('198504132010042001','Masad Alhamid, S.IP',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,409),('198504242010042001','Yusrisal Mayor',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,410),('198508042010012029','Fahria Kome',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,411),('198509082011041002','George Victor Richard Mnsen, SH',NULL,NULL,NULL,'1201200308',NULL,NULL,NULL,NULL,NULL,NULL,412),('198509182006051001','Rispa Poni Mamoaribo',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,413),('198609032004121001','Harun Mantafi, S.STP',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,414),('198610092005021001','Korneles Saleo A.Md',NULL,NULL,NULL,'1201200310',NULL,NULL,NULL,NULL,NULL,NULL,415),('198709172011042001','Kezia Lidya C. Rumbekwan, A.Md',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,416),('198808072010011007','Kamaruddin Arfan',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,417),('380032923','Drs. Darius Marauw',NULL,NULL,NULL,'1201200303',NULL,NULL,NULL,NULL,NULL,NULL,418),('62110237','Loudewick Rieupassa',NULL,NULL,NULL,'11911902',NULL,NULL,NULL,NULL,NULL,NULL,419),('640015665','Ir. Husen Duwila, MM',NULL,NULL,NULL,'20220201',NULL,NULL,NULL,NULL,NULL,NULL,420),('640024140','Asahel Watem, S.Sos',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,421),('640031037','Benyamin Lapon',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,422),('640036952','Herlina Yepasedanya',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,423),('940030879','R. Herman Tipinbdu, SH',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,424),('990002708','Lahasani Sakaiganan',NULL,NULL,NULL,'1201200202',NULL,NULL,NULL,NULL,NULL,NULL,425),('990003594','Nurhayati',NULL,NULL,NULL,'1201200304',NULL,NULL,NULL,NULL,NULL,NULL,426),('990005898','Octovianus Rumere',NULL,NULL,NULL,'1201200311',NULL,NULL,NULL,NULL,NULL,NULL,427),('990007745','Andrias Wilfrits Mayor',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,428),('990007833','Yosep Karubaba',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,429),('990007837','Joram Korwa',NULL,NULL,NULL,'12012003',NULL,NULL,NULL,NULL,NULL,NULL,430),('990007863','Marthinus D. Baransano',NULL,NULL,NULL,'1201200201',NULL,NULL,NULL,NULL,NULL,NULL,431);

/*Table structure for table `m_penandatangan` */

DROP TABLE IF EXISTS `m_penandatangan`;

CREATE TABLE `m_penandatangan` (
  `id_penandatangan` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `rincian` text,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_penandatangan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `m_penandatangan` */

insert  into `m_penandatangan`(`id_penandatangan`,`nip`,`nama`,`rincian`,`status`) values (4,'198909252001021002','Etrio Widodo','An.BupatI\nSekertaris Daerah\nUb\nKepala Bagian Umum',1),(5,'198805222009011002','Jakaria Yahya','An.Kepala Dinas Pendidikan\nKabupaten Raja Ampat',1);

/*Table structure for table `m_satker` */

DROP TABLE IF EXISTS `m_satker`;

CREATE TABLE `m_satker` (
  `id_satker` int(11) NOT NULL AUTO_INCREMENT,
  `kode_satker` varchar(11) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nama_atasan` varchar(100) DEFAULT NULL,
  `jabatan_atasan` varchar(100) DEFAULT NULL,
  `pangkat_atasan` varchar(100) DEFAULT NULL,
  `nip_atasan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_satker`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/*Data for the table `m_satker` */

insert  into `m_satker`(`id_satker`,`kode_satker`,`nama`,`nama_atasan`,`jabatan_atasan`,`pangkat_atasan`,`nip_atasan`) values (1,'10110101','Dinas Pendidikan  Kabupaten Raja Ampat','Martha M. Sanadi','Kepala Dinas Pendidikan Kabupaten Raja Ampat','Pembina (IV/a)','197110191997022001'),(2,'10210201','Dinas Kesehatan Kabupaten Raja Ampat','dr. Tommy Yong','Kepala Dinas','Pembina TK.I (IV/b)','195705201989111001'),(3,'10310301','Dinas Pekerjaan Umum Kabupaten Raja Ampat','Muh. Anthoni Nasution, ST','Kepala Dinas','Penata TK.I (III/d)','197501052002121010'),(4,'10411301','Dinas Kesejahtraan Sosial Kabupaten Raja Ampat','Marthen Umalan, SE. M.Ec.Dev','Kepala Dinas Kesejahtraan Soisal Kabupaten Raja Ampat','Pembina Tk. I ( IV/b )','195401061983031011'),(5,'10610601','Badan Perencanaan Pembangunan Daerah Kabupaten Raja Ampat','Artemas Mamrisauw, S.Sos','Kepala Bappeda','Pembina TK. I (IV/b)','196705061996101003'),(6,'10710701','Dinas Perhubungan Kabupaten Raja Ampat','Ir. J Becky Rahawarin, MM','Kepala Dinas','Pembina Utama Muda ( IV/c)','196102231989031003'),(7,'10810801','Kantor Lingkungan Hidup Kabupaten Raja Ampat','Ir. Wahab Sangaji','Kepala Kantor','Pembina TK.I (IV/b)','196603111995031003'),(8,'11011001','Dinas Kependudukan dan Catatan Sipil Kabupaten Raja Ampat','Semuel Monim, BA','Kepala Dinas Kependudukan dan Catatan Sipil Kabupaten Raja','Pembina (IV/a)','195509131983031001'),(9,'11111101','Kantor Pemberdayaan Perempuan dan Keluarga Berencana','Ratna Bondahara, S.Sos','Kepala Kantor','Pembina (IV/a)','195607171992012002'),(10,'11411401','Dinas Tenaga Kerja dan Transmigrasi Kabupaten Raja Ampat','Yulianus Mambraku, SH','Kepala Dinas','Pembina (IV/a)','000000000000000000'),(11,'11811801','Dinas Pemuda dan Olah Raga Kabupaten Raja Ampat','Wellem Mambrasar, S.Sos. MM','Kepala Dinas','Pembina TK. I (IV/b)','197311201998031009'),(12,'11911901','Badan Kesatuan Bangsa, Politik dan Perlindungan Masyarakat Kabupaten Raja Ampat','Nurdin Saka, S.Sos','Kepala Badan','Pembina TK. I (IV/b)','000000000000000000'),(13,'11911902','Kantor SatPol PP & Linmas Kabupaten Raja Ampat','Loudewick Rieupassa','Kepala Kantor Satpol PP & Linmas Kabupaten Raja Ampat','Penata (III/c)','62110237'),(14,'11911903','Badan Penanggulangan Bencana Daerah Kabupaten Raja Ampat','Alfaris Mambraku, SE,Pd, M.Si','Kepala Badan','Pembina TK. I (IV/b)','130764096'),(15,'1201200201','Kepala Daerah Kabupaten Raja Ampat','Drs. Marcus Wanma, M.Si','Kepala Daerah Kabupaten Raja Ampat','','0'),(16,'1201200202','Wakil Kepala Daerah Kabupaten Raja Ampat','Drs. Inda Arfan, M.Ec.Dev','Wakil Kepala Daerah Kabupaten Raja Ampat','','0'),(17,'12012003','Sekretariat Daerah Kabupaten Raja Ampat','Drs. Ferdinand Dimara, M.Si','Sekretaris Daerah Kabupaten Raja Ampat','Pembina Utama Muda (IV/c)','195712121983031031'),(18,'1201200301','AsistenTata Pemerintahan Kabupaten Raja Ampat','Drs. Yusuf Salim, M.Si','Asisten Tata Pemerintahan','Pembina TK.I (IV/b)','196701171992111002'),(19,'1201200302','Staf Ahli Bupati Bidang Pembangunan Kabupaten Raja Ampat','Abdul Samad Wajo, S.Sos','Staf Ahli Bupati Bidang Pembangunan Kabupaten Raja Ampat','Pembina TK. I ( IV/b )','120115504'),(20,'1201200303','Sekretariat Daerah (Bagian Administrasi Umum)','Samuel Belseran, S.Sos, M.Ec Dev','Asisten Administrasi Umum','Pembina Tk.I (IV/b)','195604191979031009'),(21,'1201200304','Sekretariat Daerah (Bagian Umum)','Maria Ramar, S.Sos','Kepala Bagian Umum','Penata TK.I (III/d)','196207271985032001'),(22,'1201200305','Sekretariat Daerah (Bagian Perlengkapan)','Jerry L. Wakman, S.IP','Kepala Bagian Perlengkapan','Penata TK.I (III/d)','196807131990031023'),(23,'1201200306','Sekretariat Daerah (Bagian Humas)','Petrus Rabu, S.Fil','Kepala Bagian Humas','Penata TK.I (III/d)','197309162005021001'),(24,'1201200307','Bagian Hukum Setda Kabupaten Raja Ampat','Mohliyat Mayalibit, SH','Kepala Bagian Hukum Kabupaten Raja Ampat','Penata TK.I (III/d)','197910202003121007'),(25,'1201200308','Bagian Ortala Setda Kabupaten Raja Ampat','Asahel Watem, S.Sos','Kepala Bagian Ortala Setda Kabupaten Raja Ampat','Pembina (IV/a)','196305251996101001'),(26,'1201200309','Sekretariat Daerah (Bagian Penyusunan Program)','Steven Numberi, S.Sos, M.Ec Dev','Kepala Bagian Penyusunan Program','Pembina Tk.I (IV/b)','196009181986031015'),(27,'1201200310','Sekretariat Daerah (Bagian Pemerintahan)','Drs. I Nyoman Jaya','Staf Ahli Bupati Bidang Pemerintahan','Pembina Utama Muda ( IV/c )','195412311986011002'),(28,'1201200311','Staf Ahli Bupati Bidang Ekonomi & Keuangan Kabupaten Raja Ampat','H. BABA, SE. MM','taf Ahli Bupati Bidang Ekonomi & Keuangan  Kabupaten Raja Am','Pembina TK. I ( IV/b )','196812311994031105'),(29,'12012004','Sekretariat DPRD','Noack Komboy, SH','Sekretaris Dewan','Pembina (IV/a)','196511301996101001'),(30,'12012005','Dinas Pendapatan Daerah','Drs. Yan Mambrasar','Kepala Dinas','Pembina Utama Muda (IV/c)','196001191987031010'),(31,'12012006','Badan Pengelolaan Keuangan dan Aset Daerah Kab. Raja Ampat','Orideko Burdam, S.IP. Mec.Dev','Kepala BPKAD Kabupaten Raja Ampat','Pembina (IV/a)','197402232002121005'),(32,'12012007','Inspektorat Kabupaten Raja Ampat','Heri Suwasito','Kasie Inspektorat Kabupaten Raja Ampat','Pembina  (IV/a)','196110131987031008'),(33,'12012008','Badan Kepegawaian Daerah dan Diklat Kabupaten Raja Ampat','Samgar Sosir, S.Sos. MH','Kepala BKD & Diklat Kabupaten Raja Ampat','Pembina TK.I (IV/b)','196102121985031001'),(34,'12019999','LPPD Kabupeten Raja Ampat','-','-','-','0'),(35,'12212201','Badan Pemberdayaan Masyarakat Kampung','Fransiska Wanma, S.Hut.M.Ec.Dev','Kepala Badan','Pembina (IV/a)','197401352000122004'),(36,'12612601','Kantor Perpustakaan,&Arsip Kabupaten Raja Ampat','Andrias Kabes','Kepala Kantor Perpustakaan & Arsip Kabupaten Raja Ampat','Penata TK. I(III/d)','050044050'),(37,'20120101','Dinas Pertanian dan Peternakan','Ir. Abdul Rahman Wairoy, M.Ec.Dev','Kepala Dinas','Pembina Utama Muda (IV/c)','196303191989031021'),(38,'20120102','Dinas Perkebunan Kabupeten Raja Ampat','Ir. Wellem J. Watem','Kepala Dinas','Pembina TK. I (IV/b)','196307041995031001'),(39,'20220201','Dinas Kehutanan Kabupaten Raja Ampat','Ir. Husen Duwila, MM','Kepala Dinas','Pembina TK. I (IV/b)','640015665'),(40,'20320301','Dinas Pertambangan dan Energi Kabupaten Raja Ampat','Drs. Muh. Idrus, BE','Kepala Dinas Pertambangan & Energi Kabupaten Raja Amapat','Pembina TK.I (IV/b)','195812311985031371'),(41,'20420401','Dinas Kebudayaan dan Pariwisata','Yusdi N. Lamatenggo, S.Pi.M.Si','Kepala Dinas','Pembina (IV/a)','640023008'),(42,'20520501','Dinas Kelautan dan Perikanan Kabupaten Raja Ampat','Mauel P. Urbinas, S.PI.M.Si','Kepala Dinas','Pembina (IV/a)','197203101995031001'),(43,'20720701','Dinas Perindagkop & UKM Kabupaten Raja Ampat','-','-','-','0'),(44,'21202095','Dinas Kesejahtraan Sosial Kabupaten Raja Ampat','Marthen Umalan, SE, M. Ec. Dev','Kepala Dinas Kesejahtraan Sosial Kabupaten Raja Ampat','Pembina Tk. I ( IV/b )','195401061983031011'),(45,'88888888','Dinas Binamarga Kabupaten Raja Ampat','-','-','-','0'),(46,'99999999','LPTQ Kabupaten Raja Ampat','-','-','-','0');

/*Table structure for table `main_konfig` */

DROP TABLE IF EXISTS `main_konfig`;

CREATE TABLE `main_konfig` (
  `id_konfig` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id_konfig`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `main_konfig` */

insert  into `main_konfig`(`id_konfig`,`nama`,`value`) values (1,'Loggo',''),(2,'nama aplikasi','Sistem Infromasi Perjalanan Dinas'),(3,'Tanggal Project',''),(4,'Klien','-'),(5,'Product By','dr.pc'),(6,'Jenis Login','1'),(8,'footer','Sistem Infromasi Perjalanan Dinas'),(7,'record log','1000');

/*Table structure for table `main_level` */

DROP TABLE IF EXISTS `main_level`;

CREATE TABLE `main_level` (
  `id_level` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(25) NOT NULL,
  `direct` text,
  `ket` text,
  PRIMARY KEY (`id_level`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `main_level` */

insert  into `main_level`(`id_level`,`nama`,`direct`,`ket`) values (1,'root','root','Untuk Super Admin'),(11,'admin','dashboard','Untuk Administrator');

/*Table structure for table `main_log` */

DROP TABLE IF EXISTS `main_log`;

CREATE TABLE `main_log` (
  `id_log` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) DEFAULT NULL,
  `nama_user` varchar(56) DEFAULT NULL,
  `table_updated` varchar(25) DEFAULT NULL,
  `aksi` text,
  `tgl` datetime DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `main_log` */

insert  into `main_log`(`id_log`,`id_admin`,`nama_user`,`table_updated`,`aksi`,`tgl`) values (1,2,'Bagus AA','admin','Upload photo','2017-04-25 19:37:14'),(2,33,'Administrator','admin','update data profile','2018-02-12 17:14:44'),(3,33,'Administrator','admin','update data profile','2018-02-12 17:14:47'),(4,33,'Administrator','admin','Upload photo','2018-02-12 17:14:49'),(5,33,'Administrator','admin','Upload photo','2018-02-12 17:14:57'),(6,33,'Administrator','admin','Upload photo','2018-02-12 18:10:44'),(7,33,'Administrator','admin','Upload photo','2018-02-14 10:52:47'),(8,33,'Administrator','admin','update data profile','2018-02-14 10:53:54');

/*Table structure for table `main_menu` */

DROP TABLE IF EXISTS `main_menu`;

CREATE TABLE `main_menu` (
  `id_menu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) DEFAULT NULL,
  `level` enum('1','2') DEFAULT NULL,
  `id_main` int(11) DEFAULT '0',
  `icon` varchar(25) DEFAULT NULL,
  `hak_akses` int(11) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `main_menu` */

insert  into `main_menu`(`id_menu`,`nama`,`level`,`id_main`,`icon`,`hak_akses`,`link`) values (1,'Konfigurasi','1',0,'fa fa-cog',1,'root/konfig'),(2,'User Group','1',0,'fa fa-users',1,'root/manajemen'),(3,'Data User','1',0,'fa fa-user',1,'root/data_user'),(5,'Pegawai','2',4,'fa fa-users',11,'pegawai'),(4,'Master','1',1,'fa fa-book',11,''),(6,'SKPD','2',4,'',11,'skpd'),(7,'Jenis Perjalanan','2',4,'',11,'jns_perjalanan'),(26,'Penanda Tangan','2',4,'',11,'penandatangan'),(25,'Transaksi','1',1,'fa fa-book',11,''),(20,'Surat Tugas','2',25,'fa fa-file',11,'surtu'),(24,'SPPD','2',25,'fa fa-file',11,'sppd');

/*Table structure for table `sppd` */

DROP TABLE IF EXISTS `sppd`;

CREATE TABLE `sppd` (
  `id_sppd` int(11) NOT NULL AUTO_INCREMENT,
  `id_surtu` int(11) DEFAULT NULL,
  `no_sppd` varchar(100) DEFAULT NULL,
  `tgl_sppd` date DEFAULT NULL,
  `jenis_perjalanan` int(11) DEFAULT NULL,
  `pejabat_perintah` varchar(100) DEFAULT NULL,
  `ket_lain` varchar(255) DEFAULT NULL,
  `tgl_berangkat` date DEFAULT NULL,
  `tgl_kembali` date DEFAULT NULL,
  PRIMARY KEY (`id_sppd`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `sppd` */

insert  into `sppd`(`id_sppd`,`id_surtu`,`no_sppd`,`tgl_sppd`,`jenis_perjalanan`,`pejabat_perintah`,`ket_lain`,`tgl_berangkat`,`tgl_kembali`) values (6,3,'noSp2d/009/03;001/2016-6666','2018-02-22',1,'Kepala Badan Pengembangan Hakim','',NULL,NULL);

/*Table structure for table `surtu` */

DROP TABLE IF EXISTS `surtu`;

CREATE TABLE `surtu` (
  `id_surtu` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(100) DEFAULT NULL,
  `tgl_surat` date DEFAULT NULL,
  `id_skpd` int(11) DEFAULT NULL,
  `dasar` varchar(255) DEFAULT NULL,
  `nip_tugas` varchar(20) DEFAULT NULL,
  `keperluan` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `ke` varchar(100) DEFAULT NULL,
  `alat_transport` varchar(100) DEFAULT NULL,
  `waktu` varchar(10) DEFAULT NULL,
  `beban` varchar(100) DEFAULT NULL,
  `tembusan` text,
  `dikeluarkan` varchar(100) DEFAULT NULL,
  `skpd` varchar(100) DEFAULT NULL,
  `pangkat_tugas` varchar(100) DEFAULT NULL,
  `jabatan_tugas` varchar(100) DEFAULT NULL,
  `nama_tugas` varchar(100) DEFAULT NULL,
  `gol_tugas` varchar(100) DEFAULT NULL,
  `pengikut` text,
  `nip_penandatangan` varchar(20) DEFAULT NULL,
  `nama_penandatangan` varchar(100) DEFAULT NULL,
  `rincian_penandatangan` text,
  `detil_waktu` varchar(100) DEFAULT NULL,
  `kop` tinyint(1) DEFAULT NULL,
  `status_tugas` tinyint(1) DEFAULT NULL,
  `status_penandatangan` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_surtu`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `surtu` */

insert  into `surtu`(`id_surtu`,`nomor`,`tgl_surat`,`id_skpd`,`dasar`,`nip_tugas`,`keperluan`,`tujuan`,`ke`,`alat_transport`,`waktu`,`beban`,`tembusan`,`dikeluarkan`,`skpd`,`pangkat_tugas`,`jabatan_tugas`,`nama_tugas`,`gol_tugas`,`pengikut`,`nip_penandatangan`,`nama_penandatangan`,`rincian_penandatangan`,`detil_waktu`,`kop`,`status_tugas`,`status_penandatangan`) values (3,'003/0050/00/2016','2018-02-22',5,'Surat Perintah Bupati lagi','198909252001021002','Agenda Dinas 3','Minahasa 4','Bali 6','Angkutan Umum Laut','2','Sekretariat Daerah Indonesia','google,muun,terdasut,agung','Minahasa Yes','Badan Perencanaan Pembangunan Daerah Kabupaten Raja Ampat','Pembina','Sekertaris Daerah','Etrio Widodo','IV/a','less,test,testpengikut,testhallo','198909252001021002','Etrio Widodo','An.BupatI<br />\nSekertaris Daerah<br />\nUb<br />\nKepala Bagian Umum','(02) Dua Hari Kerja',NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
